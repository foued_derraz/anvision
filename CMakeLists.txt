cmake_minimum_required(VERSION 2.8)
PROJECT(IVS_VICKY)

MESSAGE("------------------------------------")
set(CMAKE_AUTOMOC ON)
SET(CMAKE_INCLUDE_CURRENT_DIR ON)


include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()


set(CMAKE_BUILD_TYPE "Release" CACHE STRING  "Choose the type of build, options are: Debug ReleaseRelWithDebInfo MinSizeRel." FORCE)



set(
    CMAKE_LIBRARY_OUTPUT_DIRECTORY
    ${CMAKE_BINARY_DIR}/bin/lib
    )

set(
    CMAKE_RUNTIME_OUTPUT_DIRECTORY
    ${CMAKE_BINARY_DIR}/bin
    )

FIND_PACKAGE(Qt5 COMPONENTS Widgets Core OpenGL)
MESSAGE(STATUS "Found QtWidget-Version ${Qt4Widgets_VERSION}")
MESSAGE(STATUS "QtWidget Include: ${Qt4Widgets_INCLUDE_DIRS}")
MESSAGE(STATUS "Found QtCore-Version ${Qt4Core_VERSION}")
MESSAGE(STATUS "QtCore Include: ${Qt4Core_INCLUDE_DIRS}")

MESSAGE("------------------------------------")
FIND_PACKAGE(OpenCV 3.0 REQUIRED)
MESSAGE(STATUS "Found OpenCV-Version ${OpenCV_VERSION}")
MESSAGE(STATUS "OpenCV Include: ${OpenCV_INCLUDE_DIRS}")

MESSAGE("------------------------------------")
FIND_PACKAGE(Boost 1.47 COMPONENTS filesystem system thread program_options REQUIRED)
#MESSAGE(STATUS "Found Boost-Version ${Boost_VERSION}")
MESSAGE(STATUS "Boost Include: ${Boost_INCLUDE_DIR}")
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})

MESSAGE("------------------------------------")
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
    MESSAGE("CCACHE is used for faster compilation")
endif(CCACHE_FOUND)

MESSAGE("------------------------------------")

find_package(CURL)
IF(CURL_FOUND)
  MESSAGE(STATUS "Curl libraries found at: ${CURL_LIBRARIES}")
  MESSAGE(STATUS "Curl includes found at: ${CURL_INCLUDE_DIRS}")
else()
  MESSAGE(WARNING "WARNING : Could not find cURL on your system --> some component wont be compiled")
ENDIF(CURL_FOUND)

MESSAGE("------------------------------------")

find_package(dlib)
IF(dlib_FOUND)
  MESSAGE(STATUS "dlib version : ${dlib_VERSION}")
  MESSAGE(STATUS "dlib libraries found at: ${dlib_LIBRARIES}")
  MESSAGE(STATUS "dlib includes found at: ${dlib_INCLUDE_DIRS}")
else()
  MESSAGE(WARNING "WARNING : dlib not found.")
ENDIF(dlib_FOUND)

MESSAGE("------------------------------------")

OPTION(USE_ALPHA_COMPONENTS "activate alpha version component for compilation")



include_directories(lib)
INCLUDE_DIRECTORIES(src/ivs/view/qtStyle)

add_subdirectory(src/ivs/model)
add_subdirectory(src/ivs/helper)
add_subdirectory(src/ivs/view)
add_subdirectory(src/ivs/controller)