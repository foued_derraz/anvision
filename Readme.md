# VICKY: Video Integrated Camera frameworK sYstem 
# this framework is designed in respect with IEC 62304 norm

#### To launch Application, please use the following Command line 
```
./IVS_VICKY 
  --help                 generate Help and tutorial message
  --just-init            First launch without any component
  -F [ --json-file ] arg json file use by application
```
Other possible use
```
./IVS_VICKY --help
```
- To list all command.
```
./IVS_VICKY -F "../vicky/json/add.json" 
```
- To load and execute the project in file *add.json*
```
./IVS_VICKY -F "../vicky/json/test.json" --just-init
```
- To just load the project *test.json*

#### Please use Cmake command line to configure the framework
- push "Run CMake..." button
- as "Argument" :	
Debug mode:
```
-DCMAKE_BUILD_TYPE=Debug
```
for Release mode:
```
-DCMAKE_BUILD_TYPE=Release
```
- push "Run CMake" button