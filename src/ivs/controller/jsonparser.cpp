#include "jsonparser.h"
#include "maincontroller.h"

using namespace rapidjson;
using namespace std;

namespace ivs
{

JSonParser::JSonParser()
{
}

void JSonParser::loadFile(char* filename)
{
    FILE * pFile = fopen (filename , "r");
    //if(pFile == NULL) throw Exception("cannot open file: "+filename);
    char readBuffer[65536];
    FileReadStream is(pFile, readBuffer, sizeof(readBuffer));
    m_document = new Document();

    if(m_document->ParseStream(is).HasParseError())
    {
        FILE * pFile = fopen (filename , "r");
        char readBuffer[65536];
        FileReadStream is(pFile, readBuffer, sizeof(readBuffer));
        char c = is.Peek();
        unsigned int nbLine = 0;
        unsigned int offset = 0;
        while( offset != m_document->GetErrorOffset())
        {
            offset++;
            c = is.Take();
            if(c=='\n')
                nbLine++;
        }
        fprintf(stderr, "\nError(line %u): %s\n",
                (unsigned)nbLine,
                GetParseError_En(m_document->GetParseError()));
        m_parsingSuccess = false;
    }
    else
    {
        m_parsingSuccess = true;
    }

}

module::Module* JSonParser::parse()
{
    if(m_parsingSuccess)
    {
        if(m_document->HasMember("42"))
        {
            throw ivs::Exception("JSONParser => The meaning of life, the universe, and everything is not a \"Graph\" ");

        }

        if(m_document->HasMember("Graph") && m_document->MemberCount()==1)
        {
            module::Module* globalGraph = new module::Module();
            globalGraph->init();
            parse(&(*m_document)["Graph"],globalGraph);
            return globalGraph;
        }
        else
        {
            throw ivs::Exception("JSONParser => the first element of graph application is not  \"Graph\" ");
        }
    }
    return 0;
}

void JSonParser::parse(Value* doc,module::Module* graph)
{
    std::vector<std::pair<std::string,component::BaseObject* > > * links = MainController::Instance()->getLinks();

    for (Value::ConstMemberIterator it = doc->MemberBegin(); it != doc->MemberEnd(); ++it)
    {
        if (!strcmp( "components",it->name.GetString()))
        {
            Value* componentsList =  (Value*) &it->value;
            for (Value::ConstMemberIterator itcomp = componentsList->MemberBegin(); itcomp != componentsList->MemberEnd(); ++itcomp)
            {
                ivs::component::BaseObject* component = ivs::factory::ComponentFactory::Instance().Create(itcomp->name.GetString());
                Value* attList = (Value*) &itcomp->value;
                for (Value::ConstMemberIterator itatt = attList->MemberBegin(); itatt != attList->MemberEnd(); ++itatt)
                {
                    std::string itAttName = itatt->name.GetString();

                    if(itatt->value.IsString())
                    {
                        std::string itAttValue = itatt->value.GetString();
                        if(itAttValue[0] != '@')
                        {
                            //std::cout << "att : " << itAttName << " value : " << itAttValue << std::endl;
                            component->getData(itAttName)->setData(itAttValue);
                            component->getData(itAttName)->setIsChanged(true);

                        }
                        else
                        {
                            links->push_back(std::pair<std::string,component::BaseObject*>(itAttName+itAttValue,component));
                            component->getData(itAttName)->setIsChanged(true);
                        }
                    }
                    else
                    {
                        Value* databufferObject = (Value*) &itatt->value;

                        if(!databufferObject->HasMember("type"))
                        {
                            throw ivs::Exception("JSONParser => data :"+itAttName + " , has no type defined");
                        }
                        else if (!databufferObject->HasMember("size"))
                        {
                            throw ivs::Exception("JSONParser => data :"+itAttName + " , has no size defined");
                        }
                        else
                        {
                            string bufferType = (*databufferObject)["type"].GetString();
                            boost::to_upper(bufferType);
                            int bufferSize = std::stoi((*databufferObject)["size"].GetString());


                            if( bufferType.compare( "LIFO") == 0)
                            {
                                component->getData(itAttName)->initialyse(data::LIFO_BUFFER,bufferSize);
                            }
                            else if ( bufferType.compare( "FIFO") == 0)
                            {
                                component->getData(itAttName)->initialyse(data::FIFO_BUFFER,bufferSize);
                            }
                            else if ( bufferType.compare( "CONSTANT") == 0)
                            {
                                component->getData(itAttName)->initialyse(data::CONSTANT_BUFFER,bufferSize);
                            }


                            if(databufferObject->HasMember("value"))
                            {
                                string bufferValue = (*databufferObject)["value"].GetString();
                                if(bufferValue[0] != '@')
                                {
                                    component->getData(itAttName)->setData(bufferValue);
                                    component->getData(itAttName)->setIsChanged(true);

                                }
                                else
                                {
                                    links->push_back(std::pair<std::string,component::BaseObject*>(itAttName+bufferValue,component));
                                    component->getData(itAttName)->setIsChanged(true);
                                }
                            }
                        }
                    }
                }

                component->init();
                graph->addComponent(component);
            }
        }
        else if (!strcmp("modules",it->name.GetString()))
        {
            Value* graphsList =  (Value*) &it->value;
            for (Value::ConstMemberIterator itgraph = graphsList->MemberBegin(); itgraph != graphsList->MemberEnd(); ++itgraph)
            {
                module::Module* lowGraph = new module::Module();

                graph->init();
                parse((Value*) &itgraph->value,lowGraph);
                graph->addModule(lowGraph);
            }
        }
        else
        {
            graph->getData(it->name.GetString())->setData(it->value.GetString());
        }
    }
}



module::Module *JSonParser::parse(char* filename)
{
    loadFile(filename);
    return parse();
}

}
