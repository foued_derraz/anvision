/**
 * \file jsonparser.h
 * \brief parser json use for generate application's components from json file.
 * \author Derraz F 
 * \version 0.1
  *\ All Rights Reserved
 *
 * Parser json use for generate application's components from json file.
 */

#ifndef JSONPARSER_H
#define JSONPARSER_H

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/error/en.h"
#include "boost/algorithm/string.hpp"
#include "../model/factory/componentfactory.h"
#include "../model/module/module.h"
#include "../helper/exception.h"
#include <iostream>
#include <exception>
#include <cstring>


namespace ivs
{

/**
 * @brief The JSonParser class use in VICKY to load components from json file
 */
class JSonParser
{
public:

    /**
     * @brief JSonParser constructor
     */
    JSonParser();

    /**
     * @brief parse a file load before in m_document with rapidJson library
     * @return
     */
    module::Module *parse();

    /**
     * @brief load and parse a file.
     * @param filename file use for parsing
     * @return
     */
    module::Module* parse(char* filename);

    /**
     * @brief recursive function use to parse document
     * @param doc document to parse
     * @param graph graph associate to document
     */
    void parse(rapidjson::Value* doc,module::Module* module);

    /**
     * @brief loadFile load a json file
     * @param filename file to load
     */
    void loadFile(char* filename);

private:
    rapidjson::Document* m_document;
    bool m_parsingSuccess;
};

}

#endif // JSONPARSER_H
