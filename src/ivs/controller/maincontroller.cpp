#include "maincontroller.h"
#include "mainloopcontroller.h"
#include <boost/program_options.hpp>
#include "../helper/log.h"



using namespace std;
namespace ivs
{
MainController* MainController::instance=0;



MainController::MainController() : m_JSonfileName((char *)"../vicky/json/default.json")
{
    helper::log("MainController : Creation");
}

MainController::~MainController()
{
    helper::log("MainController : Destruction");
}

void MainController::init()
{
    this->graphInit();
    this->graphLinksInit();
    this->graphBwdInit();
}


void MainController::setPath(std::string path)
{
    m_JSonfileName = (char *)path.c_str();
}

MainController* MainController::Instance()
{
    if (instance==0)
        instance = new MainController();
    return instance;
}

void MainController::graphInit()
{
    try
    {
        m_graph= m_jsonParser.parse(m_JSonfileName);
    }
    catch ( ivs::Exception const& e)
    {
        cerr << "Erreur: " << e.what() << endl;
    }
    //this->m_graphs.push_back(myGraph);
}

void MainController::graphBwdInit()
{
    ((module::Module*)m_graph)->bwdInitComponents();
    ((module::Module*)m_graph)->bwdInitModules();
    //for first graph: init G
    //while()

}

void MainController::loop(){

    //need to change that, it's uggly ;) !!!!
    /*while (true)
    {
        m_graph->doTask();
    }*/
    helper::log("test loop maincontroller");
    ((module::Module*)m_graph)->start();
    //((module::Module*)m_graph)->join();
}

void MainController::waitGraphEndLoop()
{
    ((module::Module*)m_graph)->join();
}

void MainController::graphLinksInit(){

    for( int i = 0;i< m_links.size();++i)
    {
        std::string link = m_links[i].first;
        std::string delimiter = "@";
        std::string nameData = link.substr(0, link.find(delimiter));
        link = link.substr( link.find(delimiter)+1);

        delimiter = "/";
        std::string graphName;
        module::Module * graph = 0;
        module::Module * lastGraph = 0;
        component::BaseObject * srcComp =0;
        while(link.length()!=0)
        {
            graphName = link.substr(0, link.find(delimiter));
            lastGraph = graph;
            if(graphName.length() != link.length())
            {
                link = link.substr(link.find(delimiter)+1);
                if(graph == 0)
                {
                    if(strcmp(m_graph->getName().c_str() , graphName.c_str())==0)
                    {
                        graph = (module::Module *)m_graph;
                    }
                }
                else
                {
                    graph = lastGraph->findModule(graphName);
                    if(graph == 0)
                    {
                        srcComp = lastGraph->findComponent(graphName);
                        if(srcComp == 0)
                            helper::log("warning : error link");
                    }
                }
            }
            else
            {
                link = "";
                if(srcComp == 0)
                {
                    //We use graph to find parameter
                    //il faut ici récupérer les datas du graph
                    m_links[i].second->getData(nameData)->initData(graph->getData(graphName)->getDataVoid(),(void*)srcComp);

                }
                else
                {
                    //we find parameter from data of component
                    //il faut ici récuperer les datas du component
                    m_links[i].second->getData(nameData)->initData(srcComp->getData(graphName)->getDataVoid(),(void*)srcComp);
                }
            }
        }
    }
}

vector<pair<string,component::BaseObject* > > * MainController::getLinks()
{
    return &m_links;
}


}

int main(int argc, char *argv[])
{
    namespace po = boost::program_options;




    //use boost for declaration of arguments
    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
            ("interface,I", "use interface")
            ("help", "produce help message")
            ("just-init", "do just init, not loop")
            ("json-file,F", po::value< std::string >(), "json file use by application")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("interface"))
    {
        if (vm.count("help")) {
            cout << desc << "\n";
            return 1;
        }

        /*char ** rArray = new char*[argc+1];
        for(int i=0; i < argc; i++) {
            int len = strlen(argv[i]);
            rArray[i] = new char[len];
            strcpy(rArray[i], argv[i]);
            std::cout << "testB" << std::endl;
        }
        rArray[argc] = NULL;*/

        //int argcCopy = argc;




        QApplication a(argc, argv);
        QFile f(":qtStyle/style.qss");
        if (!f.exists())
        {
            printf("Unable to set stylesheet, file not found\n");
        }
        else
        {
            f.open(QFile::ReadOnly | QFile::Text);
            QTextStream ts(&f);
            qApp->setStyleSheet(ts.readAll());
        }

        QLocale curLocale(QLocale("en_US"));
        QLocale::setDefault(curLocale);

        //MainWindow w;
        //w.show();

        ivs::MainController* mc = ivs::MainController::Instance();


        ivs::view::SBaseView::declareInstance<ivs::view::QtView>(new ivs::view::QtView());


        ivs::view::SBaseView::Instance()->startDisplay();

        ((ivs::view::QtView*)ivs::view::SBaseView::Instance())->show();

        if(vm.count("json-file"))
        {
            std::string path = vm["json-file"].as<std::string>();
            helper::log("path : " + path);
            mc->setPath(path);
        }
        else
        {
            helper::log("path : use default scene");
        }

        /*mc->init();

        if (!vm.count("just-init")) {
            mc->loop();
        }*/

        //mc->waitGraphEndLoop();
        a.exec();
        //return a.exec();
    }
    else
    {
        if (vm.count("help")) {
            cout << desc << "\n";
            return 1;
        }

        ivs::MainController* mc = ivs::MainController::Instance();


        ivs::view::SBaseView::declareInstance<ivs::view::OpencvView>(new ivs::view::OpencvView());

        ivs::view::SBaseView::Instance()->startDisplay();

        if(vm.count("json-file"))
        {
            std::string path = vm["json-file"].as<std::string>();
            helper::log("path : " + path);
            mc->setPath(path);
        }
        else
        {
            helper::log("path : use default scene");
        }

        mc->init();

        if (!vm.count("just-init")) {
            mc->loop();
        }
        mc->waitGraphEndLoop();
    }

    return 0;
}

