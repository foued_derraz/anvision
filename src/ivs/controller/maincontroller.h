/**
 * \file maincontroller.h
 * \brief main controller 
 * \author Derraz F 
 * \version 0.1
 * Main controller of VICKY application, this class is the main controller of our application.
 * She manage all data and components of this application
 */


#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H


#include <iostream>
#include <vector>
#include <stdio.h>
#include "../model/factory/componentfactory.h"
#include "../model/module/basemodule.h"
#include "baseloopcontroller.h"
#include "jsonparser.h"
#include <QApplication>
#include <QFile>
#include <QTextStream>
#include "../view/mainwindow.h"
#include "../view/baseView.h"
#include "../view/OpencvView.h"
#include "../view/QtView.h"


namespace ivs
{

/**
 * @brief The MainController class implement singleton
 */
class MainController
{
public:
    MainController();

    /**
     * @brief Instance this function give the instance of the MainController
     * @return instance of main controller
     */
    static MainController* Instance();

    /**
     * @brief graphInit  Init all components of application
     */
    void graphInit();

    /**
     * @brief graphBwdInit backward init all components of application
     */
    void graphBwdInit();

    /**
     * @brief graphLinksInit init links of all components
     */
    void graphLinksInit();

    /**
     * @brief loop call doTask and manage all components
     */
    void loop();

    void waitGraphEndLoop();

    /**
     * @brief init mainController
     * @param path: json file to load
     */
    void init();

    /**
     * @brief setPath register the Json's path
     * @param path Json's path
     */
    void setPath(std::string path);

    ~MainController();


    /**
      * @brief getLinks
      * @return a pointer of vector conatains a link associate to this component
      */
    std::vector<std::pair<std::string,component::BaseObject* > > *  getLinks();

private:

    static MainController* instance;

    JSonParser m_jsonParser;
    module::BaseModule* m_graph;
    factory::ComponentFactory* m_factory;
    std::vector<BaseLoopController> m_loopControllers;
    char * m_JSonfileName;

    std::vector<std::pair<std::string,component::BaseObject* > > m_links;
};

}

#endif // MAINCONTROLLER_H
