/**
 * \file mainloopcontroller.h
 * \brief Main Loop controller class.
 * \author Derraz F 
 * \version 0.1

 *
 * Main Loop controller class.
 */

#ifndef MAINLOOPCONTROLLER_H
#define MAINLOOPCONTROLLER_H

#include "baseloopcontroller.h"
namespace ivs
{

class MainLoopController : public BaseLoopController
{
public:
    MainLoopController();
};

}
#endif // MAINLOOPCONTROLLER_H
