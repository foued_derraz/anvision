#include "AdvancedTimer.h"

namespace helper
{

boost::mutex _access;
AdvancedTimer AdvancedTimer::m_instance=AdvancedTimer();


void AdvancedTimer::sleep(double a)
{
    usleep((unsigned int)(a*1000000.0));
}

ctime_t AdvancedTimer::getFastTime()
{
#if defined(__amd64__)
    unsigned a, d;
    __asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
    return (((ctime_t)d)<<32UL) | a;
#else
    ctime_t t;
    __asm__ volatile ("rdtsc" : "=A" (t) );
    return t;
#endif
}

ctime_t AdvancedTimer::getRefTime()
{
    struct timeval tv;
    gettimeofday(&tv,0);
    return ((ctime_t)tv.tv_sec)*1000000 + ((ctime_t)tv.tv_usec);
}

AdvancedTimer::AdvancedTimer()
{
    this->step = 0;
    this->startTime = (double) getFastTime();


    ctime_t tick1,tick2,tick3,tick4, time1,time2;
    ctime_t reffreq = (ctime_t) 1000000;

    tick1 = getFastTime();
    time1 = getRefTime();
    tick2 = getFastTime();
    sleep(0.1);
    tick3 = getFastTime();
    time2 = getRefTime();
    tick4 = getFastTime();
    double cpu_frequency_min =(tick3-tick2) * ((double)reffreq) / (time2 - time1);
    double cpu_frequency_max =(tick4-tick1) * ((double)reffreq) / (time2 - time1);
    log(" CPU Frequency in [" + std::to_string(((int)(cpu_frequency_min/1000000))*0.001) + " .. " + std::to_string(((ctime_t)(cpu_frequency_max/1000000))*0.001) + "] GHz");
    this->ticksPerSeconds = (ctime_t)((cpu_frequency_min + cpu_frequency_max)/2);
}

AdvancedTimer::~AdvancedTimer()
{
    this->timers.clear();
}

AdvancedTimer& AdvancedTimer::Instance()
{
    return m_instance;
}

void AdvancedTimer::startTimer(string task)
{
    boost::lock_guard<boost::mutex> lock(_access);
    if(this->timers.count(task)==1){
        if(this->timers[task].isStart)
        {
            {
                log("error : Timer already start");
            }
        }
        else
        {
            this->timers[task].isStart = true;
            this->timers[task].value = (double) getFastTime();
            this->step++;
            this->printTimer(task,this->timers[task].value);
        }
    }
    else
    {
        Timer timer;
        timer.isStart = true;
        timer.value = (double) getFastTime();
        this->timers.insert(std::pair<string,Timer> (task,timer));
        this->step++;
        this->printTimer(task,this->timers[task].value);
    }
}

void AdvancedTimer::stopTimer(string task)
{
    boost::lock_guard<boost::mutex> lock(_access);
    if(this->timers.count(task)==1){
        if(this->timers[task].isStart)
        {
            this->timers[task].isStart = false;
            //this->timers[task].value = (long) clock();
            double currentTime = (double)getFastTime();
            this->printTimer(task,currentTime,currentTime - this->timers[task].value);
            this->timers[task].value = currentTime;
            this->step--;
        }
        else
        {
            {
                log("error : Timer not started");
            }
        }
    }
    else
    {
        {
            log("error : Timer have never started");
        }
    }
}

double AdvancedTimer::convertTimeInMS(ctime_t time)
{
    return ((double)time/this->ticksPerSeconds * 1000);
}

void AdvancedTimer::printTimer(string task,double currentTime, double timerDiff)
{
    std::stringstream ss;
    ss << "* " << setw(15) << (currentTime - this->startTime)/this->ticksPerSeconds * 1000<< " ms";
    for (unsigned int i=0; i<step; i++) ss << "   ";
    ss << ">end " << task << " (" << timerDiff/this->ticksPerSeconds * 1000 << " ms)" << endl;
    log(ss.str(),color::GREEN,color::BLACK);
}

void AdvancedTimer::printTimer(string task,double currentTime)
{
    std::stringstream ss;
    ss << "* " << setw(15) << (currentTime - this->startTime)/this->ticksPerSeconds *1000<< " ms";
    for (unsigned int i=0; i<step; i++) ss << "   ";
    ss << ">begin " << task << endl;
    log(ss.str(),color::RED,color::BLACK);
}

}
