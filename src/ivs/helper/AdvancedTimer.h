#ifndef ADVANCEDTIMER_H
#define ADVANCEDTIMER_H

/**
  *
  */
#include "stdio.h"
#include "ctime"
#include "iostream"
#include "../helper/log.h"
#include <iomanip>
#include <string>
#include <map>
#include <unistd.h>
#include <sys/time.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/lock_guard.hpp>

using namespace std;

namespace helper
{

typedef struct{
    double value;
    bool isStart;
} Timer;

typedef unsigned long long ctime_t;

/**
 * @brief The AdvancedTimer class : Classe permettant d'afficher le temps en console et de faire des stats.
 * Cette classe implémente le design pattern Singleton
 *
 * \author Dervaux.F (IVS)
 * \version 0.1
 * \date 20/11/2015
 *
 * Classe permettant d'afficher le temps en console et de faire des stats.
 * Cette classe implémente le design pattern Singleton.
 * Cette classe permet d'afficher le temps processeur convertit en ms.
 */
class AdvancedTimer
{
public:
    static AdvancedTimer& Instance();
    /**
     * @brief AdvancedTimer constructeur
     */
    AdvancedTimer();

    /*AdvancedTimer(const AdvancedTimer&) =delete;
    AdvancedTimer& operator=(const AdvancedTimer&) =delete;*/

    /**
      * @brief ~AdvancedTimer destructeur
      */
    ~AdvancedTimer();

    /**
     * @brief startTimer Permet de démarrer un timer lié à la chaine de caractère task
     * @param task chaine de caractère décrivant la tache réalisé
     */
    void startTimer(string task);

    /**
     * @brief startTimer Permet de stopper un timer lié à la chaine de caractère task
     * @param task chaine de caractère décrivant la tache réalisé
     */
    void stopTimer(string task);

    /**
     * @brief getFastTime
     * @return
     */
    ctime_t getFastTime();

    /**
     * @brief sleep the process stop "x" millisecondes
     * @param a the number of millisecondes
     */
    void sleep(double a);

    /**
     * @brief getRefTime Get the time of machine
     * @return
     */
    ctime_t getRefTime();

    /**
     * @brief convertTimeInMS Convert the time in millisecondes
     * @ param time the time to convert
     * @return the time in millisecondes
     */
    double convertTimeInMS(ctime_t time);

private:

    /**
     * @brief printTimer Permet d'afficher en console la description du temps
     * @param task
     * @param currentTime
     * @param timerDiff
     */
    void printTimer(string task,double currentTime, double timerDiff);

    /**
     * @brief printTimer Permet d'afficher en console la description du temps
     * @param task
     * @param currentTime
     */
    void printTimer(string task,double currentTime);

    static AdvancedTimer m_instance;
    std::map <string,Timer> timers;
    unsigned int step;
    double startTime;
    ctime_t ticksPerSeconds;

};

}

#endif // ADVANCEDTIMER_H
