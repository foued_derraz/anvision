#include "drawing.h"



namespace helper
{

shape::shape(int thickness,cv::Scalar color)
{
    this->thickness = thickness;
    this->color = color;
}

rectangle::rectangle(cv::Point2d center, cv::Point2d dimension, int thickness, cv::Scalar color) : shape(thickness,color)
{
    this->center = center;
    this->dimension = dimension;
}

void rectangle::draw(cv::Mat* frame)
{
    int width = frame->size().width;
    int height = frame->size().height;
    cv::rectangle(*frame,cv::Point2d(center.x*width,center.y*height),cv::Point2d((dimension.x+center.x)*width,(dimension.y+center.y)*height),color,thickness,8,0);
}

circle::circle(cv::Point2d center,double radius, int thickness, cv::Scalar color) : shape(thickness,color)
{
    this->center = center;
    this->radius = radius;
}

void circle::draw(cv::Mat *frame)
{
    int width = frame->size().width;
    int height = frame->size().height;
    cv::circle(*frame, cv::Point(center.x*width,center.y*height),radius*width,color,thickness,8,0);
}

line::line( cv::Point2d Point1, cv::Point2d Point2, int thickness, cv::Scalar color) : shape(thickness,color)
{
    this->Point1 = Point1;
    this->Point2 = Point2;
}

void line::draw(cv::Mat *frame)
{
    int width = frame->size().width;
    int height = frame->size().height;
    cv::line(*frame,cv::Point2d(Point1.x*width,Point1.y*height),cv::Point2d(Point2.x*width,Point2.y*height),color,thickness,8,0);
}

polygon::polygon(std::vector<cv::Point2d> listPoint, int thickness, cv::Scalar color) : shape(thickness,color)
{
    this->listPoint = listPoint;
}

void polygon::draw(cv::Mat *frame)
{
    int width = frame->size().width;
    int height = frame->size().height;


    for(int i=0;i<=listPoint.size()-1;i++)                      //  Display lines between all consecutives points and a last between last and first point
    {if(i!=listPoint.size()-1)
            cv::line(*frame,cv::Point2d(listPoint[i].x*width,listPoint[i].y*height),cv::Point2d(listPoint[i+1].x*width,listPoint[i+1].y*height),color,thickness,8,0);
        else cv::line(*frame,cv::Point2d(listPoint[i].x*width,listPoint[i].y*height),cv::Point2d(listPoint[0].x*width,listPoint[0].y*height),color,thickness,8,0);}
}

point::point( cv::Point2d Point, int thickness, cv::Scalar color) : shape(thickness,color)
{
    this->Point = Point;
}

void point::draw(cv::Mat *frame)
{
    int width = frame->size().width;
    int height = frame->size().height;
     cv::circle(*frame,cv::Point(Point.x*width,Point.y*height),0.2,color,thickness,8,0);
}



/*void draw(cv::Mat frame, string object) // frame, shape, thickness, list of points, radius (only for circle), color in scalar[3]
{


    int choix;
    double height=frameSptr.size().height;
    double width=frameSptr.size().width;

    if(object=="point")choix=0;
    if(object=="line")choix=1;
    if(object=="rectangle")choix=2;
    if(object=="circle")choix=3;
    if(object=="polygon")choix=4;





    switch(choix)
    {
    case 0:
        for(int i=0;i<listPoint.size();i++)                             //  Display small circle, so a point
            cv::circle(frameSptr,cv::Point(listPoint[i].x*width,listPoint[i].y*height),0.2,RGB,thickness,8,0);

        break;
    case 1:
        for(int i=0;i<listPoint.size()-1;i++)                       //  Display line between two consecutives points
            cv::line(frameSptr,cv::Point2d(listPoint[i].x*width,listPoint[i].y*height),cv::Point2d(listPoint[i+1].x*width,listPoint[i+1].y*height),RGB,thickness,8,0);

        break;

    case 2:
        for(int i=0;i<=listPoint.size()-2;i+=2)                     //  Display rectangle having two consecutives points as diagonal
            cv::rectangle(frameSptr,cv::Point2d(listPoint[i].x*width,listPoint[i].y*height),cv::Point2d(listPoint[i+1].x*width,listPoint[i+1].y*height),RGB,thickness,8,0);

        break;

    case 3:
        for(int i=0;i<=listPoint.size()-1;i++)                      // Display circle having for center the point, with user-choosen radius
            cv::circle(frameSptr,cv::Point2d(listPoint[i].x*width,listPoint[i].y*height),largeur*width,RGB,thickness,8,0);

        break;

    case 4:
        for(int i=0;i<=listPoint.size()-1;i++)                      //  Display lines between all consecutives points and a last between last and first point
        {if(i!=listPoint.size()-1)
                cv::line(frameSptr,cv::Point2d(listPoint[i].x*width,listPoint[i].y*height),cv::Point2d(listPoint[i+1].x*width,listPoint[i+1].y*height),RGB,thickness,8,0);
            else cv::line(frameSptr,cv::Point2d(listPoint[i].x*width,listPoint[i].y*height),cv::Point2d(listPoint[0].x*width,listPoint[0].y*height),RGB,thickness,8,0);}
        break;
    default:

        break;
    }




}*/


}

