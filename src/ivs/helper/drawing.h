/**
 * \file stringprocess.h
 * \brief helper function for string.
 * \author Derraz F 
 * \version 0.1
 *
 * helper function for string.
 */

#ifndef DRAWINGIVS_H
#define DRAWINGIVS_H




#include <opencv2/opencv.hpp>
#include "../helper/exception.h"

namespace helper
{

class shape
{
public:
    shape(int thickness,cv::Scalar color);
    virtual void draw(cv::Mat* frame)=0;
protected:
    int thickness;
    cv::Scalar color;
};

class circle : public shape
{
public :
    circle(cv::Point2d center, double radius,int thickness,cv::Scalar color);
    void draw(cv::Mat* frame);
private :
    cv::Point2d center;
    double radius;
};

class rectangle : public shape
{
public :
    rectangle(cv::Point2d center, cv::Point2d dimension,int thickness,cv::Scalar color);
    void draw(cv::Mat* frame);
private :
    cv::Point2d center;
    cv::Point2d dimension;
};

class line : public shape
{
public :
    line(cv::Point2d Point1, cv::Point2d Point2,int thickness,cv::Scalar color);
    void draw(cv::Mat* frame);
private :
    cv::Point2d Point1;
    cv::Point2d Point2;
};

class polygon: public shape
{
public :
    polygon(std::vector<cv::Point2d> listPoint,int thickness,cv::Scalar color);
    void draw(cv::Mat* frame);
private :
    std::vector<cv::Point2d> listPoint;

};

class point: public shape
{
public :
    point(cv::Point2d,int thickness,cv::Scalar color);
    void draw(cv::Mat* frame);
private :
    cv::Point2d Point;

};




}

#endif // DRAWINGIVS
