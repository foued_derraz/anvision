#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <iostream>

using namespace std;

namespace ivs
{

class Exception: public exception
{
public:
    Exception(string const& str="") throw()
         :m_str(str)
    {

    }

    virtual const char* what() const throw()
    {
        return m_str.c_str();
    }

    virtual ~Exception() throw()
    {

    }

private:
    string m_str;            //Error description
};

}

#endif // EXCEPTION_H
