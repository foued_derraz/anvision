#include "log.h"

namespace helper
{

std::map<boost::thread::id, std::ofstream*> fstreamsMap;

boost::mutex access;

void log(std::string str, color textColor, color forgroundColor)
{
#ifndef NDEBUG

	if (fstreamsMap.find(boost::this_thread::get_id()) != fstreamsMap.end())
    {
		*fstreamsMap[boost::this_thread::get_id()] << "\033[0;" << (type::TEXT + textColor) << ";" << (type::FOREGROUND + forgroundColor) << "m" << str << "\033[0m" << std::endl;
    }
    else
		std::cout << "\033[0;" << (type::TEXT + textColor) << ";" << (type::FOREGROUND + forgroundColor) << "m" << str << "\033[0m"  << std::endl;
#endif
}

void print(std::string str, color textColor, color forgroundColor)
{
	std::cout << "\033[0;" << (type::TEXT + textColor) << ";" << (type::FOREGROUND + forgroundColor) << "m" << str << "\033[0m"  << std::endl;
}


void createStream(std::string nameFile)
{
	boost::unique_lock<boost::mutex> lock(access);
	fstreamsMap.insert(std::pair<boost::thread::id, std::ofstream*>(boost::this_thread::get_id(), new std::ofstream(nameFile + ".log")));
}

}
