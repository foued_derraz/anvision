#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <map>
#include <boost/thread.hpp>
#include <fstream>

namespace helper
{

enum color { BLACK=0,
             RED=1,
             GREEN=2,
             YELLOW=3,
             BLUE=4,
             MAGENTA=5,
             CYAN=6,
             WHITE=7,
             BRIGHT_BLACK=60,
             BRIGHT_RED=61,
             BRIGHT_GREEN=62,
             BRIGHT_YELLOW=63,
             BRIGHT_BLUE=64,
             BRIGHT_MAGENTA=65,
             BRIGHT_CYAN=66,
             BRIGHT_WHITE=67
           };

enum type { TEXT = 30,
            FOREGROUND = 40
          };

void log(std::string,color textColor = color::BRIGHT_WHITE,color forgroundColor = color::BLACK);
void createStream(std::string nameFile);
void print(std::string,color textColor = color::BRIGHT_WHITE,color forgroundColor = color::BLACK);


}
#endif // LOG_H
