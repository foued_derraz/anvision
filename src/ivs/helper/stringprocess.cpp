#include "stringprocess.h"

#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <exception>


namespace helper
{



bool strToBool(std::string str){
    boost::algorithm::to_lower(str);
    if( str.compare("true")==0)
    {
        return true;
    }
    else if (str.compare("1")==0)
    {
        return true;
    }
    else if (str.compare("false")==0)
    {
        return false;
    }
    else if (str.compare("0")==0)
    {
        return false;
    }
    else
    {
        throw std::logic_error("this string is not a boolean");
    }
}


template<>
void strToMat<int>(std::string maChaine, cv::Mat &refMat)
{
    string delimiter(" ");

    maChaine = maChaine.substr(maChaine.find("[")+1);
    maChaine = maChaine.substr(0, maChaine.find("]"));

    int compteurCol(1), compteurColPrec(0), nbRows(0);
    bool ok(true);

    for(int j=0; j<maChaine.length(); ++j)
    {
        if(maChaine[j] == ' ')
            compteurCol++;
        else if(maChaine[j] == ';')
        {
            if(compteurColPrec==0)
                compteurColPrec=compteurCol;
            else
            {
                if(compteurColPrec!=compteurCol)
                {
                    ok=false;
                    break;
                }
            }
            compteurCol=0;
            nbRows++;
        }
    }
    nbRows++;

    if(ok)
    {
        int nbCols(compteurColPrec);

        refMat = cv::Mat::zeros(nbRows, nbCols, CV_32SC1);
        for(int i = 0; i < nbRows; ++i)
        {
            for(int j = 0; j < nbCols; ++j)
            {
                refMat.at<int>(i,j) = std::stoi(maChaine.substr(0,maChaine.find(delimiter)).c_str());
                maChaine = maChaine.substr(maChaine.find(delimiter)+1);
            }
        }
    }
    else
        throw ivs::Exception("this matrix'dimensions is false");


}

template<>
void strToMat<double>(std::string maChaine, cv::Mat &refMat)
{
    string delimiter(" ");

    maChaine = maChaine.substr(maChaine.find("[")+1);
    maChaine = maChaine.substr(0, maChaine.find("]"));

    int compteurCol(1), compteurColPrec(0), nbRows(0);
    bool ok(true);

    for(int j=0; j<maChaine.length(); ++j)
    {
        if(maChaine[j] == ' ')
            compteurCol++;
        else if(maChaine[j] == ';')
        {
            if(compteurColPrec==0)
                compteurColPrec=compteurCol;
            else
            {
                if(compteurColPrec!=compteurCol)
                {
                    ok=false;
                    break;
                }
            }
            compteurCol=0;
            nbRows++;
        }
    }
    nbRows++;

    if(ok)
    {
        int nbCols(compteurColPrec);

        refMat = cv::Mat::zeros(nbRows, nbCols, CV_64FC1);
        for(int i = 0; i < nbRows; ++i)
        {
            for(int j = 0; j < nbCols; ++j)
            {
                refMat.at<double>(i,j) = std::stod(maChaine.substr(0,maChaine.find(delimiter)).c_str());
                maChaine = maChaine.substr(maChaine.find(delimiter)+1);
            }
        }
    }
    else
        throw ivs::Exception("this matrix'dimensions is false");


}

template<>
void strToMat<float>(std::string maChaine, cv::Mat &refMat)
{
    string delimiter(" ");

    maChaine = maChaine.substr(maChaine.find("[")+1);
    maChaine = maChaine.substr(0, maChaine.find("]"));

    int compteurCol(1), compteurColPrec(0), nbRows(0);
    bool ok(true);

    for(int j=0; j<maChaine.length(); ++j)
    {
        if(maChaine[j] == ' ')
            compteurCol++;
        else if(maChaine[j] == ';')
        {
            if(compteurColPrec==0)
                compteurColPrec=compteurCol;
            else
            {
                if(compteurColPrec!=compteurCol)
                {
                    ok=false;
                    break;
                }
            }
            compteurCol=0;
            nbRows++;
        }
    }
    nbRows++;

    if(ok)
    {
        int nbCols(compteurColPrec);

        refMat = cv::Mat::zeros(nbRows, nbCols, CV_32FC1);
        for(int i = 0; i < nbRows; ++i)
        {
            for(int j = 0; j < nbCols; ++j)
            {
                refMat.at<float>(i,j) = std::stof(maChaine.substr(0,maChaine.find(delimiter)).c_str());
                maChaine = maChaine.substr(maChaine.find(delimiter)+1);
            }
        }
    }
    else
        throw ivs::Exception("this matrix'dimensions is false");
}


template <>
int stoType<int>(std::string str) { return std::stoi(str); }

template <>
double stoType<double>(std::string str) { return std::stod(str); }

template <>
float stoType<float>(std::string str) { return std::stof(str); }

template <>
std::string stoType<std::string>(std::string str) { return str; }

template <>
char stoType<char>(std::string str) { return str[0]; }

template <>
cv::Point2d stoType<cv::Point2d>(std::string str)
{
    str = str.substr((str.find("(")+1),str.find(")")+1);
    return cv::Point2d(std::stod(str.substr(0,str.find(" "))),std::stod(str.substr(str.find(" ")+1)));
}

template <>
cv::Point2f stoType<cv::Point2f>(std::string str)
{
    str = str.substr((str.find("(")+1),str.find(")")+1);
    return cv::Point2f(std::stod(str.substr(0,str.find(" "))),std::stof(str.substr(str.find(" ")+1)));
}

template <>
cv::Point2i stoType<cv::Point2i>(std::string str)
{
    str = str.substr((str.find("(")+1),str.find(")")+1);
    return cv::Point2i(std::stod(str.substr(0,str.find(" "))),std::stoi(str.substr(str.find(" ")+1)));
}





}
