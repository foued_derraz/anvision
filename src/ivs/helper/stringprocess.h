/**
 * \file stringprocess.h
 * \brief helper function for string.
 * \author Derraz F 
 * \version 0.1
 *
 * helper function for string.
 */

#ifndef STRINGPROCESS_H
#define STRINGPROCESS_H

#include <iostream>


#include <opencv2/opencv.hpp>
#include "../helper/exception.h"

namespace helper
{

/**
 * @brief strToBool convert std::string to bool
 * @return bool
 */
bool strToBool(std::string);

/**
 * @brief strToIntMat convert std::string to int Mat
 * @param maChaine string of Json "(int)[1 2; 3 4; 5 6;]" -- for initData => (cv::Mat)cv::Mat::zeros(2,2,CV_32F)
 * @param refMat reference of Matrix
 */
template<typename T>
void strToMat(std::string maChaine, cv::Mat &refMat);

//template <typename T>
void  strTolist(std::string str, std::vector<std::string> &listPoints);
//void  strTolist<std::vector<std::string>>(std::string str, std::vector<std::string> &listPoints);


template<typename Type>
Type stoType(std::string str);


/**
 * @brief strToVectorInt convert std::string to vector<int>
 * @param vecChaine string of Json "[1 2 3]"
 * @param refVec reference of Vector
 * @param nbElements number of elements in our vector
 */
template<typename T>
void strToVector(std::string vecChaine, std::vector<T> &refVec, int nbElements)
{
    std::string  delimiter(",");

    for(int i = 0; i < nbElements; ++i)
    {
        refVec[i]= stoType<T>(vecChaine.substr(0,vecChaine.find(delimiter)));
        vecChaine = vecChaine.substr(vecChaine.find(delimiter)+1);
    }
}



}

#endif // STRINGPROCESS_H
