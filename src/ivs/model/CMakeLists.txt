Set(IVS_VICKY_HEADER
	factory/basecomponentfactory.h
	factory/basecomponentmaker.h
	factory/basefactory.h
	factory/componentfactory.h
	factory/componentmaker.h
	data/basedata.h
	data/databuffer.h
	data/data.h
	data/datatest.h
	data/sptr.h
	component/baseobject.h
	module/basemodule.h
	module/module.h
	component/basecomponent.h
	component/addcomponent.h
	component/cameraacquisition.h
	component/graycomponent.h
	component/mycomponenttest.h
	component/printcamera.h
	component/recordvideocomponent.h
	component/backgroundsubstractorcomponent.h	
	component/drawopticalflowcomponent.h
	component/opticalflowcomponent.h
	component/mycomponent.h
	component/printint.h
	component/resizecomponent.h
	component/drawingComponent.h
	component/cropimage.h
	component/blurComponent.h
	component/directionalFlowEvaluation.h
	component/BGSMedianComponent.h
	component/copyFrame.h
	component/1D/conv1dfiltercomponent.h
	component/1D/median1dfiltercomponent.h
        component/drawinggraph.h
	component/BGS/gridBKG/gridBKG.hpp
	component/BGS/BGSgridBKGComponent.h
        component/countersystemcomponent.h
        component/BGS/BGS_FD.h
        component/Image_Processing/Scale_Image.h
        component/cameraExposureRegulationComponent.h
        component/Image_Processing/Morphology_Operators.h
        component/Image_Processing/Smoothing_Filters.h
        component/Image_Processing/Thresholding.h
        component/Image_Processing/Get_XColor.h
        component/Detection/Edges.h
        component/Detection/Find_Blobs.h
        component/Detection/Get_D.h
        #component/drawinggraph2.h
        component/Features/LBP.h
        component/Features/LBPH.h
        component/Tracking/Predict_New_Location.h
        component/Tracking/Assign_Detections.h
        component/Tracking/Update_Tracks.h
        component/Tracking/Delete_Lost_Tracks.h
        component/Tracking/Create_New_Tracks.h
        component/Tracking/Draw_Tracking_Results.h
       component/foregroundSubstractorComponent.h
        component/Tracking/Draw_Tracking_Results.h  
) 


Set(IVS_VICKY_SOURCES
	factory/basecomponentfactory.cpp
	factory/basecomponentmaker.cpp
	factory/basefactory.cpp
	factory/componentfactory.cpp
	factory/componentmaker.cpp
	data/basedata.cpp
	data/databuffer.cpp
	data/data.cpp
	data/datatest.cpp
	data/sptr.cpp	
	component/baseobject.cpp
	module/basemodule.cpp
	module/module.cpp
	component/basecomponent.cpp
	component/addcomponent.cpp
	component/cameraacquisition.cpp
	component/graycomponent.cpp
	component/mycomponenttest.cpp
	component/printcamera.cpp
	component/recordvideocomponent.cpp
	component/backgroundsubstractorcomponent.cpp
	component/drawopticalflowcomponent.cpp
	component/opticalflowcomponent.cpp
	component/mycomponent.cpp
	component/printint.cpp
	component/resizecomponent.cpp
	component/drawingComponent.cpp
	component/cropimage.cpp
	component/blurComponent.cpp
	component/directionalFlowEvaluation.cpp	
	component/BGSMedianComponent.cpp
	component/copyFrame.cpp
	component/1D/conv1dfiltercomponent.cpp
	component/1D/median1dfiltercomponent.cpp
	component/drawinggraph.cpp
	component/BGS/gridBKG/gridBKG.cpp
	component/BGS/BGSgridBKGComponent.cpp
        component/countersystemcomponent.cpp
        component/BGS/BGS_FD.cpp
        component/Image_Processing/Scale_Image.cpp
        component/cameraExposureRegulationComponent.cpp
        component/Image_Processing/Morphology_Operators.cpp
        component/Image_Processing/Smoothing_Filters.cpp
        component/Image_Processing/Thresholding.cpp
        component/Image_Processing/Get_XColor.cpp
        component/Detection/Edges.cpp
        component/Detection/Find_Blobs.cpp
        component/Detection/Get_D.cpp
        #component/drawinggraph2.cpp
        component/Features/LBP.cpp
        component/Features/LBPH.cpp
        component/Tracking/Predict_New_Location.cpp
        component/Tracking/Assign_Detections.cpp
        component/Tracking/Update_Tracks.cpp
        component/Tracking/Delete_Lost_Tracks.cpp
        component/Tracking/Create_New_Tracks.cpp
        component/Tracking/Draw_Tracking_Results.cpp
	component/foregroundSubstractorComponent.cpp
        component/Tracking/Draw_Tracking_Results.cpp
)

IF(CURL_FOUND)
MESSAGE("CURL_FOUND : add httpPostComponent")
Set(IVS_VICKY_HEADER ${IVS_VICKY_HEADER} component/httpPostComponent.h )
Set(IVS_VICKY_SOURCES ${IVS_VICKY_SOURCES} component/httpPostComponent.cpp )
ENDIF(CURL_FOUND)

IF(dlib_FOUND)
MESSAGE("dlib FOUND : add Tracker")
Set(IVS_VICKY_HEADER ${IVS_VICKY_HEADER}
        component/Tracking/Tracker.h
        component/Tracking/Tracker/Assignment.h
        component/Tracking/Tracker/Kalman.h
        component/Tracking/Tracker/Tracking.h)
Set(IVS_VICKY_SOURCES ${IVS_VICKY_SOURCES}
        component/Tracking/Tracker.cpp
        component/Tracking/Tracker/Assignment.cpp
        component/Tracking/Tracker/Kalman.cpp
        component/Tracking/Tracker/Tracking.cpp )
ENDIF(dlib_FOUND)



# PLACE HERE YOUR ONGOING DEVELOPPMENT : ACTIVATE THE USE_ALPHA_COMPONENTS in the main CMakeList : delete cache if needed
IF(USE_ALPHA_COMPONENTS)
Set(IVS_ALPHA_HEADER component/drawinggraph2.h)
Set(IVS_ALPHA_SOURCES component/drawinggraph2.cpp)
MESSAGE("Add Alpha Component :  ${IVS_ALPHA_SOURCES}") 
Set(IVS_VICKY_HEADER ${IVS_VICKY_HEADER} ${IVS_ALPHA_HEADER})
Set(IVS_VICKY_SOURCES ${IVS_VICKY_SOURCES} ${IVS_ALPHA_SOURCES})
ENDIF(USE_ALPHA_COMPONENTS)


ADD_LIBRARY(model SHARED ${IVS_VICKY_SOURCES} ${IVS_VICKY_HEADER} )


