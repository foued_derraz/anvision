#include "conv1dfiltercomponent.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(conv1DFilterComponent);

conv1DFilterComponent::conv1DFilterComponent(std::string str):BaseComponent(),
    m_newValue(initData("newValue","The new value enter",&m_newValue)),
    m_newValueOut(initData("newValueOut","The new value enter",&m_newValueOut)),
    m_kernel(initData("kernel","kernel used",{1.0,1.0,1.0},&m_kernel)),
    m_normalisation(initData("normalisation","normalisation or not?",false,&m_normalisation))
{
}

void conv1DFilterComponent::init()
{
}

void conv1DFilterComponent::bwdInit()
{
    data::SPtr<std::vector<double>> kernel = m_kernel.getData();

    for(int i=0; i<(*kernel).size();i++)
        m_listValue.push_back(0.);
}


void conv1DFilterComponent::doTask()
{
    //--------------get Data
    data::SPtr<double> newValue = m_newValue.getData();
    data::SPtr<std::vector<double>> kernel = m_kernel.getData();
    data::SPtr<bool> normalisation = m_normalisation.getData();

    data::SPtr<double> value = data::SPtr<double>(new double);


    m_listValue.push_back(*newValue);
    m_listValue.pop_front();


    *value = 0;
    for(int i=0; i<m_listValue.size();i++)
    {
        *value += m_listValue[i] * (*kernel)[i];
    }


    if(*normalisation)
    {
        double norm = 0;
        for(int i=0; i<m_listValue.size();i++)
            norm += (*kernel)[i];
        *value /= norm;
    }

    //-------------set Data
    m_newValueOut.setData(value);


}

}
}
