#ifndef CONV1DFILTERCOMPONENT_H
#define CONV1DFILTERCOMPONENT_H

#include <iostream>
#include "../basecomponent.h"
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{
namespace component
{

class conv1DFilterComponent : public BaseComponent
{
public:
    IVS_DECL(conv1DFilterComponent);
    conv1DFilterComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<double> m_newValue;
    data::Data<double> m_newValueOut;
    data::Data<std::vector<double>> m_kernel;
    data::Data<bool> m_normalisation;

    std::deque<double> m_listValue;

};

}
}

#endif // CONV1DFILTERCOMPONENT_H
