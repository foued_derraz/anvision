#include "median1dfiltercomponent.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(median1DFilterComponent);

median1DFilterComponent::median1DFilterComponent(std::string str):BaseComponent(),
  m_newValue(initData("newValue","The new value enter",&m_newValue)),
  m_sizeWindow(initData("sizeWindow","the window' size",5,&m_sizeWindow)),
  m_newValueOut(initData("newValueOut","The new value enter",&m_newValueOut))
{
}

void median1DFilterComponent::init()
{
}

void median1DFilterComponent::bwdInit()
{
    data::SPtr<int> sizeWindow = m_sizeWindow.getData();

    for(int i=0; i<(*sizeWindow);i++)
        m_listValue.push_back(0.);
}


void median1DFilterComponent::doTask()
{
    //--------------get Data
    data::SPtr<double> newValue = m_newValue.getData();
    data::SPtr<double> value = data::SPtr<double>(new double);

    m_listValue.push_back(*newValue);
    m_listValue.pop_front();

    std::deque<double> actualList;
    actualList = m_listValue;


    *value = 0;
    std::nth_element(actualList.begin(), actualList.begin() + actualList.size()/2, actualList.end());
    *value = actualList[actualList.size()/2];

    //-------------set Data
    m_newValueOut.setData(value);

}


}
}
