#ifndef MEDIAN1DFILTERCOMPONENT_H
#define MEDIAN1DFILTERCOMPONENT_H

#include <iostream>
#include "../basecomponent.h"
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{
namespace component
{

class median1DFilterComponent : public BaseComponent
{
public:
    IVS_DECL(median1DFilterComponent);
    median1DFilterComponent(std::string str);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<double> m_newValue;
    data::Data<int> m_sizeWindow;
    data::Data<double> m_newValueOut;

    std::deque<double> m_listValue;
};


}
}
#endif // MEDIAN1DFILTERCOMPONENT_H
