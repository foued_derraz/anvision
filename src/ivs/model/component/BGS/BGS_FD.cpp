#include "BGS_FD.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(BGS_FD);

BGS_FD::BGS_FD(std::string): BaseComponent(),

    m_frame(initData("frame","Frame IN", &m_frame)),
    m_foreground(initData("foreground","Forground",&m_foreground)),
    m_thresh1(initData("thresh1","Threshold 1", 15,&m_thresh1)),
    m_thresh2(initData("thresh2","Threshold 2", 25,&m_thresh2)),
    m_blur(initData("blur","Blur", 20,&m_blur))
{
    start = true;
}


void BGS_FD::init()
{

}

void BGS_FD::bwdInit()
{

}

void BGS_FD::doTask()
{ 
    data::SPtr<int> thresh1 = m_thresh1.getData();
    data::SPtr<int> thresh2 = m_thresh2.getData();
    data::SPtr<int> blur = m_blur.getData();
    data::SPtr<cv::Mat> frameSptr = m_frame.getData();

    cv::Mat foreground;
    cv::Mat frame = *frameSptr;
    normalize(frame, frame, 0, 255, cv::NORM_MINMAX);
    if (start) {
        start = false;
        prev = frame.clone();
    }

    cv::absdiff(prev, frame, foreground);
    cv::threshold(foreground, foreground, *thresh1, 255, cv::THRESH_BINARY);
    cv::blur(foreground, foreground, cv::Size(*blur,*blur));
    cv::threshold(foreground, *frameSptr, *thresh2, 255, cv::THRESH_BINARY);

    swap(frame,prev);
    m_foreground.setData(frameSptr);
}


}
}
