#ifndef BGSFD_H
#define BGSFD_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/opencv.hpp>

namespace ivs
{

namespace component
{

class BGS_FD : public BaseComponent
{
public:
    IVS_DECL(BGS_FD);
    BGS_FD(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    bool start;
    cv::Mat prev;
    data::Data<cv::Mat> m_frame;
    data::Data<cv::Mat> m_foreground;
    data::Data<int> m_thresh1;
    data::Data<int> m_thresh2;
    data::Data<int> m_blur;
};

}
}

#endif // BGSFD_H
