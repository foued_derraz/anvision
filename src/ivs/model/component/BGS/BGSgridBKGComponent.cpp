#include "BGSgridBKGComponent.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(BGSgridBKGComponent);

BGSgridBKGComponent::BGSgridBKGComponent(std::string str) :
				BaseComponent(),
				m_frameInColor(initData("m_frameInColor", "frame d'entrée BGR", &m_frameInColor)),
				m_frameForgroundMask(initData("m_frameForgroundMask", "mask de foreground", &m_frameForgroundMask)),
				m_foregroundThreshold(initData("m_foregroundThreshold", "seuil de foreground", 20, &m_foregroundThreshold))
{
	firstFrame = true;
	frameCounter = 0;

}

void BGSgridBKGComponent::init()
{
}

void BGSgridBKGComponent::bwdInit()
{
}

void BGSgridBKGComponent::doTask()
{
	//getData
	data::SPtr<cv::Mat> frameInColor = m_frameInColor.getData();
	data::SPtr<int> foregroundThreshold = m_foregroundThreshold.getData();
	data::SPtr<cv::Mat> foregroundMask = data::SPtr<cv::Mat>(new cv::Mat());

	if (firstFrame)
	{
		firstFrame = false;
		const int blockSize = 40;
		const int stride = 1;//blockSize/4;
		const int assimilationSpeed = 9;
		const int processDownscale = 2;
		const int downscale = processDownscale * stride;
		const int avarianceMultiplier = 2;
		const int athresholdOffset = 4;
		p_gridBKG = new gridBKG(blockSize, stride, assimilationSpeed, processDownscale, avarianceMultiplier, athresholdOffset);
		p_gridBKG->init(*frameInColor);
	}
	//imshow("bkgColor",bkgColor);

	if (frameCounter < 200)
	{
		p_gridBKG->update(*frameInColor, true, true, true);
	}
	else
	{
		if (frameCounter % 100 == 0)
			p_gridBKG->update(*frameInColor);
		else
			p_gridBKG->update(*frameInColor, false, false);
	}

	frameCounter++;

	unsigned long int pixelCounter = p_gridBKG->foregroundExtract(*frameInColor, *foregroundMask);
	//p_gridBKG->display(0);

	//set Data
	m_frameForgroundMask.setData(foregroundMask);
}

BGSgridBKGComponent::~BGSgridBKGComponent()
{
}

}
}
