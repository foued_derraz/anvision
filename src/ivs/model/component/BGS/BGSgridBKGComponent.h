/**
 *
 */
#ifndef BGSgridBKG
#define BGSgridBKG

#include "../basecomponent.h"
#include "../../factory/componentmaker.h"
#include "../../data/data.h"
#include "gridBKG/gridBKG.hpp"

namespace ivs
{

namespace component
{

/**
 * @brief The MyComponent class
 */
class BGSgridBKGComponent: public BaseComponent
{
public:
	IVS_DECL(BlurComponent);
	BGSgridBKGComponent(std::string);
	~BGSgridBKGComponent();
	void doTask();
	void init();
	void bwdInit();
private:
	data::Data<cv::Mat> m_frameInColor;
	data::Data<cv::Mat> m_frameForgroundMask;
	data::Data<int> m_foregroundThreshold;
	long unsigned int foregroundExtract(cv::Mat & frame, cv::Mat & bkgColor, int thresholdVal, cv::Mat & thres);
	void updatebkg_color(cv::Mat & bkg, cv::Mat & frame, cv::Mat & mask, bool useMask);
	bool firstFrame;
	unsigned int frameCounter;
	gridBKG * p_gridBKG;

};

}
}
#endif // BGSMEDIANCOMPONENT_H
