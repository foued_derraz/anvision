#include "gridBKG.hpp"

/// Constructors
gridBKG::gridBKG() :
		blockSize(4), stride(1), varianceMultiplier(2), thresholdOffset(10), assimilationSpeed(IVS_GRIDBKG_ASSIMILATION_SPEED), downscale(IVS_GRIDBKG_DOWNSCALE)
{
	colorAverage = NULL;
	variance = NULL;
	varianceIntegral = NULL;
	frameBkgDifferenceIntegral = NULL;
	frameBkgDiff = NULL;
	varianceComponentMax = NULL;
	isInitialized = false;
}
gridBKG::gridBKG(int ablockSize, int astride, int assimilSpeed, int processDownscale) :
		blockSize(ablockSize), stride(astride), varianceMultiplier(1), thresholdOffset(1), assimilationSpeed(assimilSpeed), downscale(processDownscale)
{
	colorAverage = NULL;
	variance = NULL;
	varianceIntegral = NULL;
	frameBkgDifferenceIntegral = NULL;
	frameBkgDiff = NULL;
	varianceComponentMax = NULL;
	isInitialized = false;
}

gridBKG::gridBKG(int ablockSize, int astride, int assimilSpeed, int processDownscale, int avarianceMultiplier, int athresholdOffset) :
		blockSize(ablockSize), stride(astride), varianceMultiplier(avarianceMultiplier), thresholdOffset(athresholdOffset), assimilationSpeed(assimilSpeed), downscale(processDownscale)
{
	colorAverage = NULL;
	variance = NULL;
	varianceIntegral = NULL;
	frameBkgDifferenceIntegral = NULL;
	frameBkgDiff = NULL;
	varianceComponentMax = NULL;
	isInitialized = false;
}

/// Desctructor.
gridBKG::~gridBKG()
{
	if (colorAverage != NULL)
		delete colorAverage;

	if (variance != NULL)
		delete variance;

	if (varianceIntegral != NULL)
		delete varianceIntegral;

	if (frameBkgDifferenceIntegral != NULL)
		delete frameBkgDifferenceIntegral;

	if (frameBkgDiff != NULL)
		delete frameBkgDiff;

	if (varianceComponentMax != NULL)
		delete varianceComponentMax;
}
//Point gridBKG::getBlockOffset()
//{
//
//        int dx =
//
//};

void gridBKG::init(Mat& firstFrame)
{
	if (isInitialized == false)
	{
		// bkg and variance size (1 channel)
		storageMatrixSize = Size(firstFrame.cols / downscale, firstFrame.rows / downscale);
		cout << "initialize with size " << storageMatrixSize << endl;
		uint32_t msize = storageMatrixSize.width * storageMatrixSize.height;
		colorAverage = new uint32_t[msize * 3];
		memset(colorAverage, 0, msize * 3 * sizeof(int));
		unsigned char* frameP;
		uint32_t* bkgP = colorAverage;

		const unsigned int nRows = firstFrame.rows;
		const unsigned int nCols = firstFrame.cols;

		// fill the bkg image
		for (unsigned int y = 0; y < nRows; y += downscale)
		{
			frameP = firstFrame.ptr<unsigned char>(y);
			for (unsigned int x = 0; x < nCols; x += downscale)
			{
				uint32_t f0 = *(frameP++);
				uint32_t f1 = *(frameP++);
				uint32_t f2 = *(frameP++);
				frameP += (downscale - 1) * 3;
				uint32_t& bkg0 = *(bkgP++);
				uint32_t& bkg1 = *(bkgP++);
				uint32_t& bkg2 = *(bkgP++);
				bkg0 = (f0 << IVS_GRIDBKG_BITSHIFT);
				bkg1 = (f1 << IVS_GRIDBKG_BITSHIFT);
				bkg2 = (f2 << IVS_GRIDBKG_BITSHIFT);
			}
		}

		// allocate variance and set to 0;
		variance = new uint32_t[msize * 3];
		memset(variance, 0, msize * 3 * sizeof(int));

		varianceComponentMax = new uint32_t[msize];

		// add 1 row and 1 col to the integral image so every blocks are ok on borders
		varianceIntegral = new uint32_t[(storageMatrixSize.width + 1) * (storageMatrixSize.height + 1)];
		frameBkgDifferenceIntegral = new uint32_t[(storageMatrixSize.width + 1) * (storageMatrixSize.height + 1)];

		// frame - bkg image
		frameBkgDiff = new uint32_t[msize];

		// find block image size depending on block size and stride
		// should be = ((size - blocksize) / stride) + 1

		unsigned int xs = 0;
		unsigned int ys = 0;
		int y = 0;
		for (y = 0; y+blockSize <= storageMatrixSize.height ; y += stride)
		{
			ys++;
		}
		int remainingY = storageMatrixSize.height-(y-stride+blockSize) ;

		int x = 0;
		for (x = 0; x + blockSize <= storageMatrixSize.width; x += stride)
		{
			xs++;
		}
		cout << "last x " << x << endl;
		int remainingX = storageMatrixSize.width-(x-stride+blockSize) ;


		cout << "stride " << stride <<endl;

		cout << " remaining " << remainingX << " " << remainingY << endl;

		foregroundMatrixSize = Size(xs, ys);

		blockOffsetX = remainingX / 2;
		blockOffsetY = remainingY / 2;

//        blockOffsetX = (storageMatrixSize.width - (((int)(storageMatrixSize.width / xs)) * xs)) / 4;
//               blockOffsetY = (storageMatrixSize.height - (((int)(storageMatrixSize.height / ys)) * ys)) / 4;

		cout << blockOffsetX << " " << blockOffsetY << endl;

		cout << "foregroundMatrixSize " << foregroundMatrixSize << endl;
		isInitialized = true;
	}
	else
	{
		cerr << " error : dual initialization" << endl;
		exit(-1);
	}
}

inline void gridBKG::fixedPointAverageUpdate(uint32_t& currentAverage, const uint32_t& accumulationSpeed, const uint32_t& updateValue)
{
	const uint32_t tmp = (currentAverage << accumulationSpeed) - (currentAverage) + updateValue;
	currentAverage = ((tmp + (1 << (accumulationSpeed - 1))) >> accumulationSpeed);
}

inline uint32_t colorMix(const uint32_t & c0, const uint32_t & c1, const uint32_t & c2)
{
	// return c0+c1+c2;
	return max(max(c0, c1), c2); // v0
}

//b g r
inline void colorTransform(uint32_t & c0, uint32_t & c1, uint32_t & c2)
{
	const uint32_t y = (c0 + (c1 << 1) + c2) >> 2;
	const uint32_t u = (c0 - y + 255) >> 1;
	const uint32_t v = (c2 - y + 255) >> 1;
	c0 = u;
	c1 = y >> 4;
	c2 = v;
}


//b g r
inline void colorTransform2(uint32_t & c0, uint32_t & c1, uint32_t & c2)
{
	const uint32_t sum = (c0 + c1 + c2+1) ;
	const uint32_t y = 255*c1 / (sum);
	const uint32_t u = 255*c0 / (sum);
	const uint32_t v = 255*c2 / (sum);

	c0 = u;
	c1 = y;
	c2 = v;
}

void gridBKG::update(Mat& frame, bool updateMean, bool updateVariance, bool fastAssimilation)
{
	unsigned char* frameP;
	uint32_t* bkgP = colorAverage;
	uint32_t* varianceP = variance;
	uint32_t* varianceComponentMaxP = varianceComponentMax;
	uint32_t* frameBkgDiffP = frameBkgDiff;

	const unsigned int nRows = frame.rows;
	const unsigned int nCols = frame.cols;

	for (unsigned int y = 0; y < nRows; y += downscale)
	{
		frameP = frame.ptr<unsigned char>(y);

		for (unsigned int x = 0; x < nCols; x += downscale)
		{
			uint32_t f0 = *(frameP++);  // b
			uint32_t f1 = *(frameP++);  // g
			uint32_t f2 = *(frameP++);  // r

			colorTransform2(f0, f1, f2);

			frameP += (downscale - 1) * 3;

			uint32_t& bkg0 = *(bkgP++);
			uint32_t& bkg1 = *(bkgP++);
			uint32_t& bkg2 = *(bkgP++);

			const uint32_t f0s = f0 << IVS_GRIDBKG_BITSHIFT;
			const uint32_t f1s = f1 << IVS_GRIDBKG_BITSHIFT;
			const uint32_t f2s = f2 << IVS_GRIDBKG_BITSHIFT;

			if (updateMean)
			{
				if (fastAssimilation)
				{
					fixedPointAverageUpdate(bkg0, 2, f0s);
					fixedPointAverageUpdate(bkg1, 2, f1s);
					fixedPointAverageUpdate(bkg2, 2, f2s);
				}
				else
				{
					fixedPointAverageUpdate(bkg0, assimilationSpeed, f0s);
					fixedPointAverageUpdate(bkg1, assimilationSpeed, f1s);
					fixedPointAverageUpdate(bkg2, assimilationSpeed, f2s);
				}

			}

			const uint32_t v0 = abs((int32_t) bkg0 - ((int32_t) f0s));
			const uint32_t v1 = abs((int32_t) bkg1 - ((int32_t) f1s));
			const uint32_t v2 = abs((int32_t) bkg2 - ((int32_t) f2s));

			*(frameBkgDiffP++) = colorMix(v0, v1, v2);
			//---------------------------------compute variance-------------------------------------------
			if (updateVariance)
			{
				uint32_t& var0 = *(varianceP++);
				uint32_t& var1 = *(varianceP++);
				uint32_t& var2 = *(varianceP++);

				if (fastAssimilation)
				{
					fixedPointAverageUpdate(var0, 1, v0);
					fixedPointAverageUpdate(var1, 1, v1);
					fixedPointAverageUpdate(var2, 1, v2);

				}
				else
				{
					fixedPointAverageUpdate(var0, assimilationSpeed, v0);
					fixedPointAverageUpdate(var1, assimilationSpeed, v1);
					fixedPointAverageUpdate(var2, assimilationSpeed, v2);
				}
				*(varianceComponentMaxP++) = colorMix(var0, var1, var2);

			}
		}
	}
}

void gridBKG::display(int choice)
{
	Mat bkg(storageMatrixSize.height, storageMatrixSize.width, CV_8UC3);
	uint32_t* bkgP = colorAverage;
	for (int y = 0; y < storageMatrixSize.height; y++)
	{
		for (int x = 0; x < storageMatrixSize.width; x++)
		{
			Vec3b tdata;
			tdata[0] = (unsigned char) ((*(bkgP++)) >> IVS_GRIDBKG_BITSHIFT);
			tdata[1] = (unsigned char) ((*(bkgP++)) >> IVS_GRIDBKG_BITSHIFT);
			tdata[2] = (unsigned char) ((*(bkgP++)) >> IVS_GRIDBKG_BITSHIFT);
			bkg.at<Vec3b>(y, x) = tdata;
		}
	}

	imshow("bkg", bkg);

	Mat var(storageMatrixSize.height, storageMatrixSize.width, CV_8UC3);
	uint32_t* varP = variance;
	for (int y = 0; y < storageMatrixSize.height; y++)
	{
		for (int x = 0; x < storageMatrixSize.width; x++)
		{
			Vec3b tdata;
			tdata[0] = (unsigned char) ((*(varP++)) >> IVS_GRIDBKG_BITSHIFT);
			tdata[1] = (unsigned char) ((*(varP++)) >> IVS_GRIDBKG_BITSHIFT);
			tdata[2] = (unsigned char) ((*(varP++)) >> IVS_GRIDBKG_BITSHIFT);
			var.at<Vec3b>(y, x) = tdata;
		}
	}
	//normalize(var, var, 0, 255, NORM_MINMAX);
	imshow("var", var);

	Mat dif(storageMatrixSize.height, storageMatrixSize.width, CV_8UC1);
	uint32_t* frameBkgDiffP = frameBkgDiff;
	for (int y = 0; y < storageMatrixSize.height; y++)
	{
		for (int x = 0; x < storageMatrixSize.width; x++)
		{
			dif.at<unsigned char>(y, x) = (unsigned char) ((*(frameBkgDiffP++)) >> IVS_GRIDBKG_BITSHIFT);
		}
	}
	//normalize(dif, dif, 0, 255, NORM_MINMAX);
	imshow("dif", dif);
}

void gridBKG::computeIntegralImages()
{

	//integral images are define as the sum of the area locate left and above of the point
	//
	//           |
	//           |
	//     S=sum |
	//  _________|
	//            S

	// value are shifted by IVS_GRIDBKG_BITSHIFT_2 to avoid overflow of the integral image
	// point dynamic is 255<<2 = 1024
	// The maximum value for UInt32 is 0xFFFFFFFF (or 4294967295 in decimal)
	// hence image up to (4294967295 / 1024) pixels are manageble : about 2000 x 2000

	// max(variance) integral
	uint32_t* p_variance = varianceComponentMax;
	uint32_t* p_variance_above = 0;
	uint32_t* p_variance_aboveleft = 0;
	uint32_t* p_varianceIntegral = varianceIntegral;
	uint32_t left = 0;

	///set first line to 0 since above is outside of the image
	memset(varianceIntegral, 0, sizeof(uint32_t) * (storageMatrixSize.width + 1));
	//
	p_variance_above = varianceIntegral;
	p_variance_aboveleft = varianceIntegral - 1;
	p_varianceIntegral += storageMatrixSize.width + 1;
	for (int y = 1; y < storageMatrixSize.height + 1; y++)
	{
		*(p_varianceIntegral++) = 0;  // first col is 0 since above left is outside of the images
		left = 0;
		p_variance_above++;
		p_variance_aboveleft++;
		for (int x = 1; x < storageMatrixSize.width + 1; x++)
		{
			const uint32_t newval = ((*(p_variance++)) >> IVS_GRIDBKG_BITSHIFT_2) + left + *(p_variance_above++) - *(p_variance_aboveleft++);
			*(p_varianceIntegral++) = newval;
			left = newval;
		}
	}

	// abs(frame-bkg) integral
	uint32_t* p_frameBkgDiffP = frameBkgDiff;
	uint32_t* p_frameBkgDifferenceIntegral = frameBkgDifferenceIntegral;
	uint32_t* p_frameBkgDiffP_above = 0;
	uint32_t* p_frameBkgDiffP_aboveleft = 0;

	///set first line to 0 since above is outside of the image
	memset(frameBkgDifferenceIntegral, 0, sizeof(uint32_t) * (storageMatrixSize.width + 1));
	//
	p_frameBkgDiffP_above = frameBkgDifferenceIntegral;
	p_frameBkgDiffP_aboveleft = frameBkgDifferenceIntegral - 1;
	p_frameBkgDifferenceIntegral += storageMatrixSize.width + 1;
	for (int y = 1; y < storageMatrixSize.height + 1; y++)
	{
		*(p_frameBkgDifferenceIntegral++) = 0;  // first col is 0 since above left is outside of the images
		left = 0;
		p_frameBkgDiffP_above++;
		p_frameBkgDiffP_aboveleft++;
		for (int x = 1; x < storageMatrixSize.width + 1; x++)
		{
			const uint32_t newval = ((*(p_frameBkgDiffP++)) >> IVS_GRIDBKG_BITSHIFT_2) + left + *(p_frameBkgDiffP_above++) - *(p_frameBkgDiffP_aboveleft++);
			*(p_frameBkgDifferenceIntegral++) = newval;
			left = newval;
		}
	}
}

/**
 * @brief compute the sum  of the block from its topLeft point
 * @param topLeft
 * @return
 */

inline uint32_t gridBKG::getAreaSumFromIntegral(Point topLeft, uint32_t* integralMatrix)
{
	//A---B
	//C---D
	//sum = D-B-C+A
	const uint32_t w = storageMatrixSize.width + 1;
	const uint32_t a = integralMatrix[topLeft.x + topLeft.y * w];
	const uint32_t b = integralMatrix[topLeft.x + blockSize + topLeft.y * w];
	const uint32_t c = integralMatrix[topLeft.x + (topLeft.y + blockSize) * w];
	const uint32_t d = integralMatrix[topLeft.x + blockSize + (topLeft.y + blockSize) * w];
	return d + a - b - c;
}

long unsigned int gridBKG::foregroundExtract(Mat& frame, Mat& foregroundMask)
{

	unsigned char* p_foregroundMask;
	foregroundMask = Mat::zeros(foregroundMatrixSize.height, foregroundMatrixSize.width, CV_8U);

	/// compute the 2 integral image
	computeIntegralImages();

	unsigned int imagePosX = 0;
	unsigned int imagePosY = blockOffsetY;

	// global mean of (frame - bkg) normalized for 1 block
	// use to smooth out sudden light change : average change is used has a floor threshold for foreground detection
	const uint32_t sumChange = frameBkgDifferenceIntegral[(storageMatrixSize.height + 1) * (storageMatrixSize.width + 1) - 1];
	const uint32_t divider = (storageMatrixSize.height * storageMatrixSize.width) / (blockSize * blockSize);
	const uint32_t meanChange = sumChange / divider;

	const uint32_t floorThreshold = (1 << (IVS_GRIDBKG_BITSHIFT - IVS_GRIDBKG_BITSHIFT_2)) * blockSize * blockSize * thresholdOffset;
	//cout << sumChange << endl;
	for (int y = 0; y < foregroundMatrixSize.height; y++)
	{
		imagePosX = blockOffsetX;
		p_foregroundMask = foregroundMask.ptr<unsigned char>(y);
		for (int x = 0; x < foregroundMatrixSize.width; x++)
		{
			uint32_t vs = getAreaSumFromIntegral(Point(imagePosX, imagePosY), varianceIntegral);
			uint32_t bfv = getAreaSumFromIntegral(Point(imagePosX, imagePosY), frameBkgDifferenceIntegral);


			//  bfv = frameBkgDiff[x+y*storageMatrixSize.width];
			//  vs = variance[x+y*storageMatrixSize.width];
			//  cout << bfv << " " << vs << endl;

			/// varianceOffset is multiplied by  1 << (IVS_GRIDBKG_BITSHIFT-IVS_GRIDBKG_BITSHIFT_2) to retrieve the proper dynamic
			/// varianceOffset is normalized on the bloc area
			//  if (bfv > (varianceMultiplier * vs) + meanChange + (1 << (IVS_GRIDBKG_BITSHIFT - IVS_GRIDBKG_BITSHIFT_2)) * blockSize * blockSize * thresholdOffset)
			if (bfv > (varianceMultiplier * vs) + meanChange + floorThreshold)
				*(p_foregroundMask++) = 255;
			else
				*(p_foregroundMask++) = 0;

			imagePosX += stride;
		}
		imagePosY += stride;
	}
	//exit(0);
	return 0;
}
