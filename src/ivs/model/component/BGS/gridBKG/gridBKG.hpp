#ifndef IVS_GRIDBKG
#define IVS_GRIDBKG

#include "opencv2/opencv.hpp"
#include "iostream"

using namespace cv;
using namespace std;

#define IVS_GRIDBKG_BITSHIFT 12
#define IVS_GRIDBKG_BITSHIFT_2 (IVS_GRIDBKG_BITSHIFT - 2)
#define IVS_GRIDBKG_DOWNSCALE 2
#define IVS_GRIDBKG_ASSIMILATION_SPEED 10  // higher is slower

class gridBKG
{

public:

    /// constructor
    gridBKG();
    gridBKG(int ablockSize, int astride, int assimilSpeed, int processDownscale , int avarianceMultiplier, int athresholdOffset);
    gridBKG(int blockSize, int stride, int assimilSpeed = IVS_GRIDBKG_ASSIMILATION_SPEED, int processDownscale = IVS_GRIDBKG_DOWNSCALE);
    /// Desctructor.
    ~gridBKG();

    /**
     * @brief initialize the inner buffer (malloc)
     * @param firstFrame : a frame to get the frame size
     */
    void init(Mat& firstFrame);

    /**
     * @brief upate the bkg : mean and variance
     * @param frame : current frame
     * @param updateMean : update mean when set to true
     * @param updateVariance : update variance when set to true
     */
    void update( Mat& frame, bool updateMean = true, bool updateVariance = true , bool fastAssimilation = false);

    /**
     * @brief compute the foreground given a frame
     * @param frame : current frame
     * @param foregroundMask : output binary mask
     * @return
     */
    long unsigned int foregroundExtract( Mat& frame, Mat& foregroundMask);

    /**
     * @brief display for debug
     * @param choice : not used for now
     */
    void display(int choice);

    /**
     * @brief return the stride
     * @return
     */
    unsigned int getStride()
    {
        return stride;
    };

    Point getBlockOffset()
    {
    };

   private:
    cv::Size storageMatrixSize;
    uint32_t* colorAverage;
    uint32_t* variance;
    uint32_t* frameBkgDiff;
    uint32_t* varianceComponentMax;
    uint32_t* varianceIntegral;
    uint32_t* frameBkgDifferenceIntegral;

    cv::Size foregroundMatrixSize;

    bool isInitialized;  // to check double init
    const int blockSize;  // block size
    const unsigned int stride;  // block stride
    const unsigned int varianceMultiplier;
    const unsigned int thresholdOffset;

    const unsigned int assimilationSpeed;  // assimilation speed (bitshift) used for mean and variance assimilation

    const unsigned int downscale;  // image stride when reading frame

    /**
     * @brief compute a fixed point cumulative average
     * @param currentAverage : average to be updated
     * @param accumulationSpeed : accumulation speed
     * @param updateValue : value used to update the average
     */
    inline void fixedPointAverageUpdate(uint32_t& currentAverage, const uint32_t& accumulationSpeed, const uint32_t& updateValue);

    /**
    * @brief compute the integral image of variance and (frame-bkg)
    */
    void computeIntegralImages();

    /**
     * @brief compute the sum of a given block in the integral image
     * @param topLeft : top left of the block
     * @param integralMatrix : integral where to compute the sum
     * @return
     */
    inline uint32_t getAreaSumFromIntegral(Point topLeft, uint32_t* integralMatrix);


    int blockOffsetX; // block offset in storageMatrix images
    int blockOffsetY;
};

#endif
