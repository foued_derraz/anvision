#include "BGSMedianComponent.h"

namespace ivs
{
	namespace component
	{

		IVS_REGISTER(BGSMedianComponent);

		BGSMedianComponent::BGSMedianComponent(std::string str) :
						BaseComponent(),
						m_frameInColor(initData("m_frameInColor", "frame d'entrée BGR", &m_frameInColor)),
						m_frameForgroundMask(initData("m_frameForgroundMask", "mask de foreground", &m_frameForgroundMask)),
						m_foregroundThreshold(initData("m_foregroundThreshold", "seuil de foreground", 20, &m_foregroundThreshold))
		{
			firstFrame = true;

		}

		void BGSMedianComponent::init()
		{
		}

		void BGSMedianComponent::bwdInit()
		{
		}

		void BGSMedianComponent::doTask()
		{
			//getData
			data::SPtr<cv::Mat> frameInColor = m_frameInColor.getData();
			data::SPtr<int> foregroundThreshold = m_foregroundThreshold.getData();
			data::SPtr<cv::Mat> foregroundMask = data::SPtr<cv::Mat>(new cv::Mat(frameInColor->rows,frameInColor->cols,CV_8U));

			if(firstFrame)
			{
				firstFrame = false;
				bkgColor = frameInColor->clone();
			}
			//imshow("bkgColor",bkgColor);
			unsigned long int pixelCounter = foregroundExtract(*frameInColor, bkgColor, *foregroundThreshold,*foregroundMask);
			updatebkg_color(bkgColor, *frameInColor, *foregroundMask, false);
//			//do staff
			//set Data
			m_frameForgroundMask.setData(foregroundMask);
		}

		BGSMedianComponent::~BGSMedianComponent()
		{
		}

		void BGSMedianComponent::updatebkg_color(cv::Mat & bkg, cv::Mat & frame, cv::Mat & mask, bool useMask)
		{
			unsigned char * pbkgval;
			unsigned char * pframeval;

			int channels = bkg.channels();

			int nRows = bkg.rows;
			int nCols = bkg.cols * channels;

			if (!useMask)
			{
				for (int y = 0; y < nRows; ++y)
				{
					//cout << y << endl;
					pbkgval = bkg.ptr<unsigned char>(y);
					pframeval = frame.ptr<unsigned char>(y);
					for (int x = 0; x < nCols; x += channels)
					{
						for (int c = 0; c < channels; ++c)
						{
							const int index = x + c;
							if (pframeval[index] > pbkgval[index])
								pbkgval[index]++;
							else
							{
								if (pframeval[index] < pbkgval[index])
									pbkgval[index]--;
							}
						}

					}
				}
			}
			else
			{
				unsigned char * pmask;
				for (int y = 0; y < nRows; ++y)
				{
					//cout << y << endl;
					pbkgval = bkg.ptr<unsigned char>(y);
					pframeval = frame.ptr<unsigned char>(y);
					pmask = mask.ptr<unsigned char>(y);
					int x2 = 0;
					for (int x = 0; x < nCols; x += channels, x2++)
					{
						if (pmask[x2] == 0)
						{
							for (int c = 0; c < channels; ++c)
							{
								const int index = x + c;
								if (pframeval[index] > pbkgval[index])
									pbkgval[index]++;
								else
								{
									if (pframeval[index] < pbkgval[index])
										pbkgval[index]--;
								}
							}
						}
					}
				}
			}
		}

		long unsigned int BGSMedianComponent::foregroundExtract(cv::Mat & frame, cv::Mat & bkgColor, int thresholdVal, cv::Mat & thres)
		{

			unsigned char * pbkgval;
			unsigned char * pframeval;
			unsigned char * pthres;

			long unsigned int pixelCounter = 0;

			const int channels = bkgColor.channels();

			int nRows = bkgColor.rows;
			int nCols = bkgColor.cols * channels;

			for (int y = 0; y < nRows; ++y)
			{
				//cout << y << endl;
				pbkgval = bkgColor.ptr<unsigned char>(y);
				pframeval = frame.ptr<unsigned char>(y);
				pthres = thres.ptr<unsigned char>(y);

				int x2 = 0;
				for (int x = 0; x < nCols; x += channels, x2++)
				{
					bool isForeground = false;
					for (int c = 0; c < channels; c++)
					{
						isForeground = isForeground || (abs(pbkgval[x + c] - pframeval[x + c]) > thresholdVal );
					}

					if (isForeground)
					{
						pthres[x2] = 255;
						pixelCounter++;
					}
					else
					{
						pthres[x2] = 0;
					}
				}
			}
			return pixelCounter;
		}

	}
}
