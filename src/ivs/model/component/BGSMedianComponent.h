/**
 *
 */
#ifndef BGSMEDIANCOMPONENT_H
#define BGSMEDIANCOMPONENT_H

#include "basecomponent.h"
#include "../factory/componentmaker.h"
#include "../data/data.h"

namespace ivs
{

namespace component
{

/**
 * @brief The MyComponent class
 */
class BGSMedianComponent: public BaseComponent
{
public:
	IVS_DECL(BlurComponent);
	BGSMedianComponent(std::string);
	~BGSMedianComponent();
	void doTask();
	void init();
	void bwdInit();
private:
	data::Data<cv::Mat> m_frameInColor;
	data::Data<cv::Mat> m_frameForgroundMask;
	data::Data<int> m_foregroundThreshold;
	long unsigned int foregroundExtract(cv::Mat & frame, cv::Mat & bkgColor, int thresholdVal, cv::Mat & thres);
	void updatebkg_color(cv::Mat & bkg, cv::Mat & frame, cv::Mat & mask, bool useMask);
	bool firstFrame;

	cv::Mat bkgColor;


};

}
}
#endif // BGSMEDIANCOMPONENT_H
