#include "Edges.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(Edges);


Edges::Edges(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_thresh1(initData("thresh1","thresh1",100.0,&m_thresh1)),
    m_thresh2(initData("thresh2","thresh2",255.0,&m_thresh2)),
    m_aperture(initData("aperture","aperture",3,&m_aperture)),
    m_gradient(initData("gradient","gradient",false,&m_gradient))
{
}

void Edges::init()
{

}

void Edges::bwdInit()
{

}

void Edges::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<double> thresh1Sptr = m_thresh1.getData();
    data::SPtr<double> thresh2Sptr = m_thresh2.getData();
    data::SPtr<int> apertureSptr = m_aperture.getData();
    data::SPtr<bool> gradientSptr = m_gradient.getData();


    Canny(*frameSptr, *frameSptr, *thresh1Sptr,*thresh2Sptr,*apertureSptr,*gradientSptr);

    m_frameOut.setData(frameSptr);

}

}
}
