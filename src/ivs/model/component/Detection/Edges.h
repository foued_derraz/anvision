#ifndef EDGES_H
#define EDGES_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class Edges : public BaseComponent
{
public:
    IVS_DECL(Edges);
    Edges(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<double> m_thresh1;
    data::Data<double> m_thresh2;
    data::Data<int> m_aperture;
    data::Data<bool> m_gradient;
};

}
}

#endif // EDGES_H
