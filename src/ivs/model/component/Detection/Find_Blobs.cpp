#include "Find_Blobs.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(FindBlobs);


FindBlobs::FindBlobs(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_ids(initData("ids","ids",&m_ids)),
    m_centers(initData("centers","centers",&m_centers)),
    m_dis(initData("dis","dis",&m_dis)),
    m_mode(initData("mode","mode",0,&m_mode)),
    m_method(initData("method","method",0,&m_method)),
    m_offset(initData("offset","offset",Point(),&m_offset)),
    m_minarea(initData("minarea","minarea",0.0,&m_minarea)),
    m_maxarea(initData("maxarea","maxarea",1000000.0,&m_maxarea))
{
}

void FindBlobs::init()
{

}

void FindBlobs::bwdInit()
{

}

void FindBlobs::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<vector<int>> idsSptr = new vector<int>;
    data::SPtr<vector<Point2f>> centersSptr = new vector<Point2f>;
    data::SPtr<vector<Point2f>> disSptr = new vector<Point2f>;
    data::SPtr<int> modeSptr = m_mode.getData();
    data::SPtr<int> methodSptr = m_method.getData();
    data::SPtr<Point> offsetSptr = m_offset.getData();
    data::SPtr<double> minareaSptr = m_minarea.getData();
    data::SPtr<double> maxareaSptr = m_maxarea.getData();

    vector<int> ids;
    vector<Point2f> centers;
    vector<Point2f> dis;
    Rect box;
    vector<vector<Point> > Contours;
    vector<Vec4i> hierarchy;
    findContours(*frameSptr, Contours, hierarchy, *modeSptr, *methodSptr, *offsetSptr);
        for (int i=0; i < Contours.size(); i++){
            if (contourArea(Mat(Contours[i])) > *minareaSptr & contourArea(Mat(Contours[i])) < *maxareaSptr) {
                ids.push_back(i);
                box = boundingRect(Contours[i]);
                centers.push_back((box.br() + box.tl())/2);
                dis.push_back(Point2f(box.x,box.y));
            }
        }
    *idsSptr = ids;
    *centersSptr = centers;
    *disSptr = dis;

    m_ids.setData(idsSptr);
    m_frameOut.setData(frameSptr);
    m_centers.setData(centersSptr);
    m_dis.setData(disSptr);

}

}
}
