#ifndef FINDBLOBS_H
#define FINDBLOBS_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class FindBlobs : public BaseComponent
{
public:
    IVS_DECL(FindBlobs);
    FindBlobs(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<vector<int>> m_ids;
    data::Data<vector<cv::Point2f>> m_centers;
    data::Data<vector<cv::Point2f>> m_dis;
    data::Data<int> m_mode;
    data::Data<int> m_method;
    data::Data<cv::Point> m_offset;
    data::Data<double> m_minarea;
    data::Data<double> m_maxarea;

};

}
}

#endif // FINDBLOBS_H
