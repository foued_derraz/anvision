#include "LBP.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(LBP);


LBP::LBP(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_neighbors(initData("neighbors","neighbors",8,&m_neighbors)),
    m_radius(initData("radius","radius", 1,&m_radius))
{
}


void LBP::init()
{

}

void LBP::bwdInit()
{

}


void LBP::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<int> neighborsSptr = m_neighbors.getData();
    data::SPtr<int> radiusSptr = m_radius.getData();

    Mat frame = *frameSptr;
    Mat lbp = Mat::zeros(frame.rows-(2**radiusSptr), frame.cols-(2**radiusSptr), CV_8UC1);
    for(unsigned int n=0 ; n < *neighborsSptr; n++) {
        float x = (float)*radiusSptr * cos(2.0*M_PI*n/(*neighborsSptr));
        float y = (float)*radiusSptr * -sin(2.0*M_PI*n/(*neighborsSptr));
        int fx = (int) floor(x);
        int fy = (int) floor(y);
        int cx = (int) ceil(x);
        int cy = (int) ceil(y);
        float ty = y - fy;
        float tx = x - fx;
        float w1 = (1 - tx) * (1 - ty);
        float w2 =      tx  * (1 - ty);
        float w3 = (1 - tx) *      ty;
        float w4 =      tx  *      ty;
        for(int i=*radiusSptr; i < frame.rows-*radiusSptr;i++) {
            const unsigned char* row = frame.ptr<unsigned char>(i);
            for(int j=*radiusSptr;j < frame.cols-*radiusSptr;j++) {
                float t = w1*frame.at<unsigned char>(i+fy,j+fx) + w2*frame.at<unsigned char>(i+fy,j+cx) + w3*frame.at<unsigned char>(i+cy,j+fx) + w4*frame.at<unsigned char>(i+cy,j+cx);
                lbp.at<unsigned char>(i-*radiusSptr,j-*radiusSptr) += ((t > row[j]) && (abs(t-row[j]) > std::numeric_limits<float>::epsilon())) << n;
            }

        }

    }

    *frameSptr = lbp;

    m_frameOut.setData(frameSptr);

}

}
}
