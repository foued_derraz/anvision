#ifndef LBP_H
#define LBP_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class LBP : public BaseComponent
{
public:
    IVS_DECL(LBP);
    LBP(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<int> m_neighbors;
    data::Data<int> m_radius;
};

}
}

#endif // LBP_H
