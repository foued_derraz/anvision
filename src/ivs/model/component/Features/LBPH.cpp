#include "LBPH.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(LBPH);


LBPH::LBPH(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_hist(initData("hist","hist", &m_hist)),
    m_neighbors(initData("neighbors","neighbors",8,&m_neighbors)),
    m_block(initData("block","block",8,&m_block)),
    m_overlap(initData("overlap","overlap",0,&m_overlap))
{
}


void LBPH::init()
{

}

void LBPH::bwdInit()
{

}


void LBPH::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<int> neighborsSptr = m_neighbors.getData();
    data::SPtr<int> blockSptr = m_block.getData();
    data::SPtr<int> overlapSptr = m_overlap.getData();


    Mat frame = *frameSptr;
    int w = frame.cols;
    int h = frame.rows;
    std::vector<int> table;
    int numUniforms = 0;
    int numSlots = 1 << *neighborsSptr;
    int pattern = *neighborsSptr*(*neighborsSptr-1) + 2;
    for ( int i=0; i<numSlots; i++ )
    {
        int transitions = 0;
        for ( int j=0; j<*neighborsSptr-1; j++ )
            transitions += (bit(i,j) != bit(i,j+1));
        transitions += (bit(i,*neighborsSptr-1) != bit(i,0));
        if ( transitions <= 2 )
        {        table.push_back(numUniforms++);
        }
        else
        {
            table.push_back(pattern);
        }
    }

    for (int i=0 ; i < frame.rows;i++)
        for (int j=0;j< frame.cols;j++)
        {
            uchar val;
            val = frame.at<uchar>(i,j);
            frame.at<uchar>(i,j) = table[(int)val];
        }
/*
    Size window ((int) floor(frame.cols/(*blockSptr)), (int) floor(frame.rows / (*blockSptr)));
    vector<Mat> histograms;
    for(int x=0; x < w - window.width; x+=(window.width-*overlapSptr)) {
        for(int y=0; y < h-window.height; y+=(window.height-*overlapSptr)) {
            Mat cell = Mat(frame, Rect(x,y,window.width, window.height));
            histograms.push_back(histogram(cell, pattern+1));
        }
    }

    hist.create(1, histograms.size()*(pattern+1), CV_32SC1);
    for(int histIdx=0; histIdx < histograms.size(); histIdx++) {
        for(int valIdx = 0; valIdx < numPatterns; valIdx++) {
            int y = histIdx*(patterns+1)+valIdx;
            hist.at<int>(0,y) = histograms[histIdx].at<int>(valIdx);
        }
    }
*/

    *frameSptr = frame;

    m_frameOut.setData(frameSptr);

}

bool LBPH::bit(unsigned b, unsigned i)
{
    return ((b & (1 << i)) != 0);
}

}
}
