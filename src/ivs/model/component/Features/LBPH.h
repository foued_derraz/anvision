#ifndef LBPH_H
#define LBPH_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class LBPH : public BaseComponent
{
public:
    IVS_DECL(LBPH);
    LBPH(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
    bool bit(unsigned b, unsigned i);

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<cv::Mat> m_hist;
    data::Data<int> m_neighbors;
    data::Data<int> m_block;
    data::Data<int> m_overlap;
};

}
}

#endif // LBP_H
