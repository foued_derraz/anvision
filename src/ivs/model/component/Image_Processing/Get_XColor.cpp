#include "Get_XColor.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(GetXColor);


GetXColor::GetXColor(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_space(initData("space","Space", (string) "gray",&m_space)),
    m_color(initData("color","Color", (string) "gray",&m_color))
{
}

void GetXColor::init()
{

}

void GetXColor::bwdInit()
{

}

void GetXColor::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<string> spaceSptr = m_space.getData();
    data::SPtr<string> colorSptr = m_color.getData();

    if (*spaceSptr == "GRAY") {
        cv::cvtColor(*frameSptr, *frameSptr, COLOR_BGR2GRAY);
    }
    else if (*spaceSptr == "RGB") {
        if (*colorSptr == "R") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[2];
        }
        if (*colorSptr == "G") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[1];
        }
        if (*colorSptr == "B") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[0];
        }
    }
    else if (*spaceSptr == "HSV") {
        cv::cvtColor(*frameSptr,*frameSptr,CV_BGR2HSV);
        if (*colorSptr == "H") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[0];
        }
        if (*colorSptr == "S") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[1];
        }
        if (*colorSptr == "V") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[2];
        }
    }
    else if (*spaceSptr == "Lab") {
        cv::cvtColor(*frameSptr,*frameSptr,CV_BGR2Lab);
        if (*colorSptr == "L") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[0];
        }
        if (*colorSptr == "a") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[1];
        }
        if (*colorSptr == "b") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[2];
        }
    }
    else if (*spaceSptr == "Luv") {
        cv::cvtColor(*frameSptr,*frameSptr,CV_BGR2Luv);
        if (*colorSptr == "L") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[0];
        }
        if (*colorSptr == "u") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[1];
        }
        if (*colorSptr == "v") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[2];
        }
    }
    else if (*spaceSptr == "XYZ") {
        cv::cvtColor(*frameSptr,*frameSptr,CV_BGR2XYZ);
        if (*colorSptr == "X") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[0];
        }
        if (*colorSptr == "Y") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[1];
        }
        if (*colorSptr == "Z") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[2];
        }
    }
    else if (*spaceSptr == "YCrCb") {
        cv::cvtColor(*frameSptr,*frameSptr,CV_BGR2YCrCb);
        if (*colorSptr == "Y") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[0];
        }
        if (*colorSptr == "Cr") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[1];
        }
        if (*colorSptr == "Cb") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[2];
        }
    }
    else if (*spaceSptr == "YUV") {
        cv::cvtColor(*frameSptr,*frameSptr,CV_BGR2YUV);
        if (*colorSptr == "Y") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[0];
        }
        if (*colorSptr == "U") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[1];
        }
        if (*colorSptr == "V") {
            std::vector<cv::Mat> Channels(3);
            cv::split(*frameSptr, Channels);
            *frameSptr = Channels[2];
        }
    }
    else {
        // nothing
    }


    m_frameOut.setData(frameSptr);

}

}
}
