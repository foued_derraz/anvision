#ifndef GETXCOLOR_H
#define GETXCOLOR_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class GetXColor : public BaseComponent
{
public:
    IVS_DECL(GetXColor);
    GetXColor(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<string> m_space;
    data::Data<string> m_color;

};

}
}

#endif // GETXCOLOR_H
