#include "Morphology_Operators.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(MorphologyOperators);


MorphologyOperators::MorphologyOperators(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_operator(initData("operator","operator",0,&m_operator)),
    m_element(initData("element","element",0,&m_element)),
    m_size(initData("size","size",1,&m_size))
{
}

void MorphologyOperators::init()
{

}

void MorphologyOperators::bwdInit()
{

}

void MorphologyOperators::doTask()
{
    data::SPtr<Mat> frameSptr = m_frameIn.getData();
    data::SPtr<int> operatorSptr = m_operator.getData();
    data::SPtr<int> elementSptr = m_element.getData();
    data::SPtr<int> sizeSptr = m_size.getData();

    int type;
    if( *elementSptr == 0 ){ type = MORPH_RECT; }
    else if( *elementSptr == 1 ){ type = MORPH_CROSS; }
    else if( *elementSptr == 2) { type = MORPH_ELLIPSE; }

    Mat kernel = getStructuringElement( type, Size( 2**sizeSptr + 1, 2**sizeSptr+1 ), Point( *sizeSptr, *sizeSptr ) );


    if (*operatorSptr == 0)
        erode( *frameSptr, *frameSptr, kernel );
    else if (*operatorSptr == 1)
        dilate( *frameSptr, *frameSptr, kernel );
    else
        morphologyEx( *frameSptr, *frameSptr, *operatorSptr, kernel );

    m_frameOut.setData(frameSptr);

}

}
}
