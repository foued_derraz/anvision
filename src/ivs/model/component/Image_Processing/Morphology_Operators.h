#ifndef MORPHOLOGYOPERATORS_H
#define MORPHOLOGYOPERATORS_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class MorphologyOperators : public BaseComponent
{
public:
    IVS_DECL(MorphologyOperators);
    MorphologyOperators(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<int> m_operator;
    data::Data<int> m_element;
    data::Data<int> m_size;
};

}
}

#endif // MORPHOLOGYOPERATORS_H
