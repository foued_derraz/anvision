#include "Scale_Image.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(ScaleImage);


ScaleImage::ScaleImage(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_scale(initData("scale","Scale",1.0,&m_scale)),
    m_width(initData("width","Width",100,&m_width)),
    m_height(initData("height","height",100,&m_height))
{
}

void ScaleImage::init()
{

}

void ScaleImage::bwdInit()
{

}

void ScaleImage::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<double> scaleSptr = m_scale.getData();
    data::SPtr<int> widthSptr = m_width.getData();
    data::SPtr<int> heightSptr = m_height.getData();

    cv::Mat frame = *frameSptr;
    if (*scaleSptr > 0)
        cv::resize(*frameSptr,*frameSptr,cv::Size (frame.cols**scaleSptr,frame.rows**scaleSptr));
    else
        cv::resize(*frameSptr,*frameSptr,cv::Size (*widthSptr,*heightSptr));

    m_frameOut.setData(frameSptr);

}

}
}
