#ifndef SCALEIMAGE_H
#define SCALEIMAGE_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class ScaleImage : public BaseComponent
{
public:
    IVS_DECL(ScaleImage);
    ScaleImage(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<double> m_scale;
    data::Data<int> m_width;
    data::Data<int> m_height;
};

}
}

#endif // SCALEIMAGE_H
