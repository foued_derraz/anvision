#include "Smoothing_Filters.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(SmoothingFilters);


SmoothingFilters::SmoothingFilters(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_filter(initData("filter","filter",0,&m_filter)),
    m_size(initData("size","size",3,&m_size)),
    m_anchor(initData("anchor","anchor",3,&m_anchor)),
    m_d(initData("d","d",1,&m_d)),
    m_sigmaX(initData("sigmaX","sigmaX",1.0,&m_sigmaX)),
    m_sigmaY(initData("sigmaY","sigmaY",1.0,&m_sigmaY)),
    m_sigmaC(initData("sigmaC","sigmaC",1.0,&m_sigmaC)),
    m_sigmaS(initData("sigmaS","sigmaS",1.0,&m_sigmaS)),
    m_ddepth(initData("ddepth","ddepth",-1,&m_ddepth)),
    m_delta(initData("delta","delta",0.0,&m_sigmaC)),
    m_border(initData("border","border",0,&m_border))
{
}

void SmoothingFilters::init()
{

}

void SmoothingFilters::bwdInit()
{

}

void SmoothingFilters::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<int> filterSptr = m_filter.getData();
    data::SPtr<int> sizeSptr = m_size.getData();
    data::SPtr<int> anchorSptr = m_anchor.getData();
    data::SPtr<int> dSptr = m_d.getData();
    data::SPtr<double> sigmaXSptr = m_sigmaX.getData();
    data::SPtr<double> sigmaYSptr = m_sigmaY.getData();
    data::SPtr<double> sigmaCSptr = m_sigmaC.getData();
    data::SPtr<double> sigmaSSptr = m_sigmaS.getData();
    data::SPtr<int> ddepthSptr = m_ddepth.getData();
    data::SPtr<double> deltaSptr = m_delta.getData();
    data::SPtr<int> borderSptr = m_border.getData();


    if (*filterSptr == 0)
       blur(*frameSptr, *frameSptr, Size(*sizeSptr,*sizeSptr), Point(*anchorSptr,*anchorSptr), *borderSptr);
    else if (*filterSptr == 1)
       GaussianBlur(*frameSptr, *frameSptr, Size(*sizeSptr,*sizeSptr), *sigmaXSptr, *sigmaYSptr, *borderSptr);
    else if (*filterSptr == 2)
       medianBlur(*frameSptr, *frameSptr, *sizeSptr);
    else if (*filterSptr == 3)
       bilateralFilter(*frameSptr, *frameSptr, *dSptr, *sigmaCSptr, *sigmaSSptr, *borderSptr);
    else {
        Mat kernel = Mat::ones( *sizeSptr, *sizeSptr, CV_32F )/ (float)(*sizeSptr**sizeSptr);
        filter2D(*frameSptr, *frameSptr, *ddepthSptr , kernel, Point(*anchorSptr,*anchorSptr), *deltaSptr, *borderSptr);
    }

    m_frameOut.setData(frameSptr);

}

}
}
