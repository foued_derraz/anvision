#ifndef SMOOTHINGFILTERS_H
#define SMOOTHINGFILTERS_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class SmoothingFilters : public BaseComponent
{
public:
    IVS_DECL(SmoothingFilters);
    SmoothingFilters(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<int> m_filter;
    data::Data<int> m_size;
    data::Data<int> m_anchor;
    data::Data<int> m_d;
    data::Data<double> m_sigmaX;
    data::Data<double> m_sigmaY;
    data::Data<double> m_sigmaC;
    data::Data<double> m_sigmaS;
    data::Data<int> m_ddepth;
    data::Data<double> m_delta;
    data::Data<int> m_border;
};

}
}

#endif // SCALEIMAGE_H
