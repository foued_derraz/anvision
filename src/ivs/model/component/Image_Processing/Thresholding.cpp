#include "Thresholding.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(Thresholding);


Thresholding::Thresholding(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_thresh(initData("thresh","thresh",100.0,&m_thresh)),
    m_maxval(initData("maxval","maxval",255.0,&m_maxval)),
    m_type(initData("type","type",0,&m_type))
{
}

void Thresholding::init()
{

}

void Thresholding::bwdInit()
{

}

void Thresholding::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<double> threshSptr = m_thresh.getData();
    data::SPtr<double> maxvalSptr = m_maxval.getData();
    data::SPtr<int> typeSptr = m_type.getData();

    threshold(*frameSptr, *frameSptr, *threshSptr, *maxvalSptr, *typeSptr);

    m_frameOut.setData(frameSptr);

}

}
}
