#ifndef THRESHOLDING_H
#define THRESHOLDING_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class Thresholding : public BaseComponent
{
public:
    IVS_DECL(Thresholding);
    Thresholding(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<double> m_thresh;
    data::Data<double> m_maxval;
    data::Data<int> m_type;
};

}
}

#endif // THRESHOLDING_H
