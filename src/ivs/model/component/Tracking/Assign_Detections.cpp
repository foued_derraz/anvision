#include "Assign_Detections.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(AssignDetections);


AssignDetections::AssignDetections(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_ids(initData("ids","ids",vector<int> (),&m_ids)),
    m_centers(initData("centers","centers", vector<Point2f> (),&m_centers)),
    m_dis(initData("dis","dis", vector<Point2f> (), &m_dis))
{
}

void AssignDetections::init()
{

}

void AssignDetections::bwdInit()
{

}

void AssignDetections::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<vector<int>> idsSptr = m_ids.getData();
    data::SPtr<vector<Point2f>> centersSptr = m_centers.getData();
    data::SPtr<vector<Point2f>> disSptr = m_dis.getData();

    vector<int> ids = *idsSptr;
    vector<Point2f> centers = *centersSptr;
    vector<Point2f> dis = *disSptr;

    for (unsigned i = 0; i < centers.size(); i++) {
        Point2f pt = dis[i];
        Point2f center = centers[i];
        Point2f ptx = Point(pt.x+(abs(pt.x-center.x)*2),pt.y+(abs(pt.y-center.y)*2));
        int radius = abs(pt.x - center.x);
        circle	(*frameSptr,center, radius, Scalar(120),4,LINE_8,0 );
        rectangle(*frameSptr,Rect(pt,ptx),Scalar(120),4,LINE_8,0);
    }

    m_frameOut.setData(frameSptr);

}

}
}
