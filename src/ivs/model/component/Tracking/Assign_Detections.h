#ifndef ASSIGNDETECTIONS_H
#define ASSIGNDETECTIONS_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class AssignDetections : public BaseComponent
{
public:
    IVS_DECL(AssignDetections);
    AssignDetections(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<vector<int>> m_ids;
    data::Data<vector<cv::Point2f>> m_centers;
    data::Data<vector<cv::Point2f>> m_dis;
};

}
}

#endif // ASSIGNDETECTIONS_H
