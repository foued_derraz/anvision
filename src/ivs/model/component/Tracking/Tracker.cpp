#include "Tracker.h"


#include "../../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(Tracker);


Tracker::Tracker(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main frame", &m_frameOut)),
    m_ids(initData("ids","ids",vector<int> (),&m_ids)),
    m_centers(initData("centers","centers", vector<Point2f> (),&m_centers)),
    m_dis(initData("dis","dis", vector<Point2f> (), &m_dis)),
    m_distance_thresh(initData("distance_thresh","Distance thresh", 30.0, &m_distance_thresh)),
    m_maximum_skipping(initData("maximum_skipping","Maximum skipping", 10, &m_maximum_skipping)),
    m_maximum_trace(initData("maximum_trace","Maximum trace", 10, &m_maximum_trace)),
    m_dimensionality_state(initData("dimensionality_state","Dimensionality state", 4, &m_dimensionality_state)),
    m_dimensionality_measurment(initData("dimensionality_measurment","Dimensionality measurment", 2, &m_dimensionality_measurment)),
    m_dimensionality_control(initData("dimensionality_control","Dimensionality control", 0, &m_dimensionality_control)),
    m_delta_time(initData("delta_time","Delta time", 0.2, &m_delta_time)),
    m_process_noise(initData("process_noise","Process noise", 0.5, &m_process_noise)),
    m_type(initData("type","Type", 0, &m_type)),
    m_limits_x(initData("limits_x","Limits x", -5, &m_limits_x)),
    m_limits_y(initData("limits_y","Limits y", -5, &m_limits_y)),
    m_limits_w(initData("limits_w","Limits w", -10, &m_limits_w)),
    m_limits_h(initData("limits_h","Limits h", 400, &m_limits_h))

{
}

void Tracker::init()
{

}

void Tracker::bwdInit()
{
    data::SPtr<double> distance_threshSptr = m_distance_thresh.getData();
    data::SPtr<int> maximum_skippingSptr = m_maximum_skipping.getData();
    data::SPtr<int> maximum_traceSptr = m_maximum_trace.getData();
    data::SPtr<int> dimensionality_stateSptr = m_dimensionality_state.getData();
    data::SPtr<int> dimensionality_measurmentSptr = m_dimensionality_measurment.getData();
    data::SPtr<int> dimensionality_controlSptr = m_dimensionality_control.getData();
    data::SPtr<double> delta_timeSptr = m_delta_time.getData();
    data::SPtr<double> process_noiseSptr = m_process_noise.getData();
    data::SPtr<int> typeSptr = m_type.getData();
    data::SPtr<int> limits_xSptr = m_limits_x.getData();
    data::SPtr<int> limits_ySptr = m_limits_y.getData();
    data::SPtr<int> limits_wSptr = m_limits_w.getData();
    data::SPtr<int> limits_hSptr = m_limits_h.getData();

    Tracking.init(*dimensionality_stateSptr,*dimensionality_measurmentSptr,*dimensionality_controlSptr,(float)*delta_timeSptr,(float)*process_noiseSptr);
    Tracking.SetLimits(*limits_xSptr,*limits_ySptr,*limits_wSptr,*limits_hSptr,*distance_threshSptr,*maximum_skippingSptr,*maximum_traceSptr);

}

void Tracker::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<vector<int>> idsSptr = m_ids.getData();
    data::SPtr<vector<Point2f>> centersSptr = m_centers.getData();
    data::SPtr<vector<Point2f>> disSptr = m_dis.getData();

    vector<int> ids = *idsSptr;
    vector<Point2f> centers = *centersSptr;
    vector<Point2f> dis = *disSptr;

    vector<IVS::Blob> Detections;
    IVS::Blob blob;

    // D
    for (unsigned i = 0; i < centers.size(); i++) {
        Point2f pt = dis[i];
        Point2f center = centers[i];
        Point2f ptx = Point(pt.x+(abs(pt.x-center.x)*2),pt.y+(abs(pt.y-center.y)*2));
        int radius = abs(pt.x - center.x);

        blob.id = i;
        blob.area = 0;
        blob.center = centers[i];
        blob.roi = Rect(pt,ptx);

        //circle	(*frameSptr,center, radius, Scalar(120),4,LINE_8,0 );
        //rectangle(*frameSptr,Rect(pt,ptx),Scalar(120),4,LINE_8,0);

        Detections.push_back(blob);
    }

    // T
    std::cout << "Detections" << Detections.size() << std::endl;
    Tracking.Do(Detections, Tracks, Tracklets);
    std::cout << "Tracks" << Tracks.size() << std::endl;
    /*
    string num[] = {"0","1","2","3","4","5","6","7","8","9","10","11"};
    for (int i = 0; i < Tracklets.size(); i++) {
        if (!Tracklets[i].counted) {
            circle(*frameSptr,Tracklets[i].center,20,Scalar(0,255,0),4);
        }
        else {
            circle(*frameSptr,Tracklets[i].center,20,Scalar(255,0,0),4);
        }
        putText(*frameSptr, num[i], Tracklets[i].center, 1, 1, Scalar(0,255,0), 3);
        if(Tracklets[i].trace.size() > 1) {
            for(int j=0;j < Tracklets[i].trace.size()-1;j++) {
                line(*frameSptr,Tracklets[i].trace[j],Tracklets[i].trace[j+1],Scalar(0,255,0),2);
            }
        }
    }
    */

    Tracklets.clear();

    m_frameOut.setData(frameSptr);

}

}
}
