#ifndef TRACKER_H
#define TRACKER_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

// Tracker
#include "Tracker/Tracking.h"

namespace ivs
{

namespace component
{

class Tracker : public BaseComponent
{
public:
    IVS_DECL(Tracker);
    Tracker(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    IVS::Tracking Tracking;
    vector<IVS::Tracklet> Tracklets;
    vector<IVS::Track*> Tracks;
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<vector<int>> m_ids;
    data::Data<vector<cv::Point2f>> m_centers;
    data::Data<vector<cv::Point2f>> m_dis;
    data::Data<double> m_distance_thresh;
    data::Data<int> m_maximum_skipping;
    data::Data<int> m_maximum_trace;
    data::Data<int> m_dimensionality_state;
    data::Data<int> m_dimensionality_measurment;
    data::Data<int> m_dimensionality_control;
    data::Data<double> m_delta_time;
    data::Data<double> m_process_noise;
    data::Data<int> m_type;
    data::Data<int> m_limits_x;
    data::Data<int> m_limits_y;
    data::Data<int> m_limits_w;
    data::Data<int> m_limits_h;

};

}
}

#endif // TRACKER_H
