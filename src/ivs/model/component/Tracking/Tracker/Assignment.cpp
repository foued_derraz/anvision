#include "Assignment.h"

// Def
Assign::Assign() {

}

// Delete

Assign::~Assign()
{

}

// Solve
vector<int> Assign::Solve(dlib::matrix<int> cost,int L)
{
    vector<long> assignment = dlib::max_cost_assignment(cost);
    vector<int> assignments(L,-1);

    for(int i=0;i<assignment.size();i++) {
        if (assignment.size() <= assignments.size())
            assignments[i] = assignment[i];
        else
            assignments[i] = -1;
    }
    return assignments;
}
