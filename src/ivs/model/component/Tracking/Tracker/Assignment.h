#ifndef AssignmentSolver
#define AssignmentSolver

#include <vector>
#include "opencv2/opencv.hpp"
#include "dlib/matrix/matrix.h"
#include "dlib/optimization.h"
using namespace cv;
using namespace std;


class Assign
{
public:
    Assign();
    ~Assign();
    vector<int> Solve(dlib::matrix<int> cost, int L);
};


#endif
