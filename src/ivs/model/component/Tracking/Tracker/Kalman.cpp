#include "Kalman.h"

// Def
KFilter::KFilter() {

}

// Delete

KFilter::~KFilter()
{
    delete KalmanF;
}

// Initialize
void KFilter::init(Point _P, int _DIMENSIONALITY_STATE, int _DIMENSIONALITY_MEASUREMENT, int _DIMENSIONALITY_CONTROL, float _DELTA_TIME, float _PROCESS_NOISE, int _TYPE)
{
    KalmanF = new KalmanFilter(_DIMENSIONALITY_STATE,_DIMENSIONALITY_MEASUREMENT,_DIMENSIONALITY_CONTROL,_TYPE);
    KalmanF->transitionMatrix = (Mat_<float>(4, 4) <<  1,0,_DELTA_TIME,0,   0,1,0,_DELTA_TIME,   0,0,1,0,   0,0,0,1);
    KalmanF->statePre.at<float>(0) = _P.x;
    KalmanF->statePre.at<float>(1) = _P.y;
    KalmanF->statePre.at<float>(2) = 0;
    KalmanF->statePre.at<float>(3) = 0;
    KalmanF->statePost.at<float>(0) = _P.x;
    KalmanF->statePost.at<float>(1) = _P.y;
    setIdentity(KalmanF->measurementMatrix);
    KalmanF->processNoiseCov=(Mat_<float>(4, 4) << pow(_DELTA_TIME,4.0)/4.0	,0 ,pow(_DELTA_TIME,3.0)/2.0,0,
        0,pow(_DELTA_TIME,4.0)/4.0	,0 ,pow(_DELTA_TIME,3.0)/2.0,
        pow(_DELTA_TIME,3.0)/2.0,0	,pow(_DELTA_TIME,2.0),0,
        0,pow(_DELTA_TIME,3.0)/2.0	,0,pow(_DELTA_TIME,2.0));
    KalmanF->processNoiseCov*= _PROCESS_NOISE;
    setIdentity(KalmanF->measurementNoiseCov, Scalar::all(0.1));
    setIdentity(KalmanF->errorCovPost, Scalar::all(.1));
}

// Predict
void KFilter::Predict() {
    Mat prediction = KalmanF->predict();
    this->PREV_P = Point(prediction.at<float>(0),prediction.at<float>(1));
}

// Correct
void KFilter::Correct(Point _P, bool _CORRECT, int _TYPE)
{
    Mat measurement(2,1,_TYPE);
    if(!_CORRECT)
    {
        measurement.at<float>(0) = this->PREV_P.x;
        measurement.at<float>(1) = this->PREV_P.y;
    }
    else
    {
        measurement.at<float>(0) = _P.x;
        measurement.at<float>(1) = _P.y;
    }

    Mat estimated = KalmanF->correct(measurement);
    this->PREV_P.x = estimated.at<float>(0);
    this->PREV_P.y = estimated.at<float>(1);
}

// Get point
Point KFilter::GetP() {
    return this->PREV_P;
}
