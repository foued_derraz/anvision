#ifndef INCLUDED_Kalman
#define INCLUDED_Kalman

#include "opencv2/opencv.hpp"
using namespace cv;

class KFilter
{
public:
    KFilter();
    ~KFilter();
    void init(Point _P, int _TRACKING_DIMENSIONALITY_STATE, int _TRACKING_DIMENSIONALITY_MEASUREMENT, int _TRACKING_DIMENSIONALITY_CONTROL, float _TRACKING_DELTA_TIME, float _TRACKING_PROCESS_NOISE, int _TRACKING_TYPE);
    void Predict();
    void Correct(Point _P, bool _CORRECT, int _TYPE);
    Point GetP();
    KalmanFilter* KalmanF;
private:
    Point PREV_P;

};

#endif
