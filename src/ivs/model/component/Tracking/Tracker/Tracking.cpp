#include "Tracking.h"

////////////////
/// Tracking ///
////////////////

IVS::Tracking::Tracking() {

}

IVS::Tracking::~Tracking() {

}

void IVS::Tracking::init(int _DIMENSIONALITY_STATE, int _DIMENSIONALITY_MEASUREMENT, int _DIMENSIONALITY_CONTROL, float _DELTA_TIME, float _PROCESS_NOISE) {
    this->DIMENSIONALITY_STATE = _DIMENSIONALITY_STATE;
    this->DIMENSIONALITY_MEASUREMENT = _DIMENSIONALITY_MEASUREMENT;
    this->DIMENSIONALITY_CONTROL = _DIMENSIONALITY_CONTROL;
    this->DELTA_TIME = _DELTA_TIME;
    this->PROCESS_NOISE = _PROCESS_NOISE;
    this->TYPE = CV_32F;

}

void IVS::Tracking::SetLimits(int _LIMITS_X, int _LIMITS_Y, int _LIMITS_W, int _LIMITS_H, int _DISTANCE_THRESH, int _MAXIMUM_SKIPPING, int _MAXIMUM_TRACE) {
    this->LIMITS_X = _LIMITS_X;
    this->LIMITS_Y = _LIMITS_Y;
    this->LIMITS_W = _LIMITS_W;
    this->LIMITS_H = _LIMITS_H;
    this->DISTANCE_THRESH = _DISTANCE_THRESH;
    this->MAXIMUM_SKIPPING = _MAXIMUM_SKIPPING;
    this->MAXIMUM_TRACE = _MAXIMUM_TRACE;
}

void IVS::Tracking::Do(vector<Blob>& Detections, vector<Track*> &Tracks, vector<Tracklet>& Tracklets) {
    vector<int> assignments;

    if (Detections.size() > 0)
    {

        Update(Detections, Tracks, assignments);

        for (int i = 0; i < Tracks.size(); i++)
        {
            if ( Tracks[i]->center.y >= this->LIMITS_H || Tracks[i]->center.y <= this->LIMITS_Y)
            {
                kill(Tracks,assignments,i);
                continue;
            }

            kill(Tracks,assignments);
        }
    }
    kill(Tracks,assignments);


    if (Detections.size() == 0 && Tracks.size() > 0) {

        for (int i = 0; i < Tracks.size(); i++)
        {
            Tracks[i]->skipped++;
            if (Tracks[i]->skipped > this->MAXIMUM_SKIPPING) {
                delete Tracks[i];
                Tracks.erase(Tracks.begin() + i);
            }


        }
    }


    if (Tracks.size() > 0) {
        for (int i = 0; i < Tracks.size(); i++)
        {
            Tracklet tracklet;
            tracklet.id = Tracks[i]->id;
            tracklet.roi = Tracks[i]->roi;
            tracklet.center = Tracks[i]->center;
            tracklet.trace = Tracks[i]->trace;
            tracklet.counted = Tracks[i]->counted;
            Tracklets.push_back(tracklet);
        }
    }

    Detections.clear();

}

void IVS::Tracking::Update(vector<Blob>& Detections, vector<Track*> &Tracks, vector<int>& assignments) {


    if(Tracks.size()==0)
    {
        for(int i=0;i<Detections.size();i++)
        {
            Track* track = new Track(Detections[i].center, this->DIMENSIONALITY_STATE, this->DIMENSIONALITY_MEASUREMENT, this->DIMENSIONALITY_CONTROL, this->DELTA_TIME, this->PROCESS_NOISE, this->TYPE);
            track->center = Detections[i].center;
            track->roi = Detections[i].roi;
            track->dead = false;
            track->bIn = false;
            track->bOut = false;
            track->counted = false;
            Tracks.push_back(track);
        }
    }


    int N = Tracks.size();
    int M = Detections.size();

    dlib::matrix<int> cost(N,M);
    for (int i=0;i<N;i++) {
        for (int j=0;j<M;j++) {
            Point diff=(Tracks[j]->prediction-Detections[i].center);
            cost(i,j) = sqrt(pow (diff.x,2) + pow (diff.y,2));
        }
    }

    Assign Assignment;
    if (cost.size() > 0)
        assignments = Assignment.Solve(cost,N);

    /*
    cout << " Cost " << endl;
    vector< vector<double> > cost(N,vector<double>(M));
       for(int i=0;i<Tracks.size();i++)
       {
           for(int j=0;j<Detections.size();j++)
           {
               Point diff=(Tracks[i]->prediction-Detections[j].center);
               cost[i][j] = sqrt(pow(diff.x,2) + pow(diff.y,2));
               cout << cost[i][j] << " " ;
           }
           cout << endl;
       }

    AssignmentProblemSolver APS;
    APS.Solve(cost,assignments,AssignmentProblemSolver::optimal);

*/

    vector<int> not_assigned;

    for(int i=0;i<assignments.size();i++)
    {
        if(assignments[i]!=-1)
        {
            if(cost(i,assignments[i]) > this->DISTANCE_THRESH)
            {
                assignments[i] = -1;
                not_assigned.push_back(i);
            }
        }
        else
        {
            Tracks[i]->skipped++;
        }
    }

    kill(Tracks,assignments);

    vector<int> not_assigned_detections;
    vector<int>::iterator it;
    for(int i=0;i<Detections.size();i++)
    {
        it=find(assignments.begin(), assignments.end(), i);
        if(it==assignments.end())
        {
            not_assigned_detections.push_back(i);
        }
    }



    if(not_assigned_detections.size() != 0)
    {
        for(int i=0;i<not_assigned_detections.size();i++)
        {
            Track* track = new Track(Detections[not_assigned_detections[i]].center, this->DIMENSIONALITY_STATE, this->DIMENSIONALITY_MEASUREMENT, this->DIMENSIONALITY_CONTROL, this->DELTA_TIME, this->PROCESS_NOISE, this->TYPE);
            track->center = Detections[not_assigned_detections[i]].center;
            track->roi = Detections[not_assigned_detections[i]].roi;
            track->dead = false;
            track->bIn = false;
            track->bOut = false;
            track->counted = false;
            Tracks.push_back(track);
        }
    }

    for(int i=0;i<assignments.size();i++)
    {
        Tracks[i]->KalmanF->Predict();
        if(assignments[i]!=-1)
        {
            Tracks[i]->skipped = 0;
            Tracks[i]->KalmanF->Correct(Detections[assignments[i]].center, true, this->TYPE);
        }else
        {
            Tracks[i]->KalmanF->Correct(Point(0,0), false, this->TYPE);
        }

        Tracks[i]->prediction = Tracks[i]->KalmanF->GetP();

        if(Tracks[i]->trace.size() > this->MAXIMUM_TRACE)
        {
            Tracks[i]->trace.erase(Tracks[i]->trace.begin(),Tracks[i]->trace.end() - this->MAXIMUM_TRACE);
        }

        Tracks[i]->center = Tracks[i]->prediction;
        Tracks[i]->trace.push_back(Tracks[i]->prediction);
    }

}


void IVS::Tracking::kill(vector<Track*>& Tracks, vector<int>& assignments)
{
    for (int i = 0; i<Tracks.size(); i++)
    {
        if (Tracks[i]->skipped > this->MAXIMUM_SKIPPING)
        {
            delete Tracks[i];
            Tracks.erase(Tracks.begin() + i);
            assignments.erase(assignments.begin() + i);
            i--;
        }
    }
}

void IVS::Tracking::kill(vector<Track*>& Tracks, vector<int>& assignments, int i)
{
    delete Tracks[i];
    Tracks.erase(Tracks.begin() + i);
    assignments.erase(assignments.begin() + i);
}

int nextID = 0;

IVS::Track::Track(Point _P, int _DIMENSIONALITY_STATE, int _DIMENSIONALITY_MEASUREMENT, int _DIMENSIONALITY_CONTROL, float _DELTA_TIME, float _PROCESS_NOISE, int _TYPE) {
    id = nextID;
    nextID++;
    roi = Rect();
    center = Point();
    prediction = _P;
    skipped = 0;
    dead = false;
    KalmanF = new KFilter();
    KalmanF->init(_P, _DIMENSIONALITY_STATE, _DIMENSIONALITY_MEASUREMENT, _DIMENSIONALITY_CONTROL, _DELTA_TIME, _PROCESS_NOISE, _TYPE);
}

IVS::Track::~Track()
{
    delete KalmanF;
}
