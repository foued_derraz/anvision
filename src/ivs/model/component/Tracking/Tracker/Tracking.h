#pragma once
#ifndef _Tracker
#define _Tracker

#include "opencv2/opencv.hpp"
#include "Kalman.h"
#include "Assignment.h"
#include "HungarianAlg.h"

////////////////
/// Tracking ///
////////////////

using namespace cv;

namespace IVS {

struct Blob
{
    int id;
    Point center;
    double area;
    Rect roi;
};

struct Tracklet {
    int id;
    Rect roi;
    Point center;
    vector<Point> trace;
    bool counted;
};

class Track
{
public:
    int id;
    Rect roi;
    Point center;
    Point prediction;
    int skipped;
    vector<Point> trace;
    bool dead;
    bool counted;
    bool bIn, bOut;
    KFilter* KalmanF;
    Track(Point _P, int _DIMENSIONALITY_STATE, int _DIMENSIONALITY_MEASUREMENT, int _DIMENSIONALITY_CONTROL, float _DELTA_TIME, float _PROCESS_NOISE, int _TYPE);
    ~Track();
};

class Tracking
{
public:
    Tracking();
    ~Tracking();
    void init(int _DIMENSIONALITY_STATE, int _DIMENSIONALITY_MEASUREMENT, int _DIMENSIONALITY_CONTROL, float _DELTA_TIME, float _PROCESS_NOISE);
    void SetLimits(int _LIMITS_X, int _LIMITS_Y, int _LIMITS_W, int _LIMITS_H, int _DISTANCE_THRESH , int _MAXIMUM_SKIPPING, int _MAXIMUM_TRACE);
    void Do(vector<Blob>& Detections, vector<Track*>& Tracks, vector<Tracklet>& Tracklets);
    void Update(vector<Blob>& Detections, vector<Track*>& Tracks, vector<int>& assignments);
    void kill(vector<Track*>& Tracks, vector<int>& assignments);
    void kill(vector<Track*>& Tracks, vector<int>& assignments, int i);

    int LIMITS_X, LIMITS_Y, LIMITS_W, LIMITS_H, DISTANCE_THRESH , MAXIMUM_SKIPPING, MAXIMUM_TRACE;
    int DIMENSIONALITY_STATE, DIMENSIONALITY_MEASUREMENT, DIMENSIONALITY_CONTROL;
    float DELTA_TIME, PROCESS_NOISE;
    int TYPE;
};

}

#endif
