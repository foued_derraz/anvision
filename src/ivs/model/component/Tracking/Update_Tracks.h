#ifndef UPDATETRACKS_H
#define UPDATETRACKS_H

#include "../basecomponent.h"
#include <iostream>
#include "../../factory/componentmaker.h"
#include "../../data/data.h"

namespace ivs
{

namespace component
{

class UpdateTracks : public BaseComponent
{
public:
    IVS_DECL(UpdateTracks);
    UpdateTracks(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<vector<int>> m_ids;
    data::Data<vector<cv::Point2f>> m_centers;
    data::Data<vector<cv::Point2f>> m_dis;
};

}
}

#endif // UPDATETRACKS_H
