#include "addcomponent.h"
#include "../factory/componentmaker.h"


namespace ivs
{
namespace component
{

IVS_REGISTER(AddComponent);

AddComponent::AddComponent(std::string str):
    BaseComponent(),
    m_first(initData("first","first operand",1,&m_first)),
    m_second(initData("second","second operand",1,&m_second)),
    m_result(initData("result","result",0,&m_result))
{
}

void AddComponent::init()
{
    log("AddComponent : init " + this->getName());
}

void AddComponent::bwdInit()
{
    log("AddComponent : bwdInit" + this->getName() + " myInt: ");
}

void AddComponent::doTask()
{
    log("AddComponent : " + std::to_string(*m_first.getData()) + " + " + std::to_string(*m_second.getData()));
    *m_result.getData() = *m_first.getData() + *m_second.getData();
}

}
}
