#ifndef ADDCOMPONENT_H
#define ADDCOMPONENT_H

#include "basecomponent.h"

namespace ivs
{
namespace component
{


class AddComponent : public BaseComponent
{
public:
    IVS_DECL(AddComponent);
    AddComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<int> m_first;
    data::Data<int> m_second;
    data::Data<int> m_result;
    AddComponent();
};


}
}
#endif // ADDCOMPONENT_H
