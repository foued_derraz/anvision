#include "backgroundsubstractorcomponent.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(backgroundSubstractorComponent);

backgroundSubstractorComponent::backgroundSubstractorComponent(std::string): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","the background",&m_frameOut)),
    m_history(initData("history","history of background",500, &m_history)),
    m_threshold(initData("threshold","threshold of background",32, &m_threshold)),
    m_detectShadows(initData("detectShadows","detect shadow or not?",false, &m_detectShadows))
{
}


void backgroundSubstractorComponent::init()
{

}

void backgroundSubstractorComponent::bwdInit()
{

    data::SPtr<int> historySptr = m_history.getData();
    data::SPtr<int> thresholdSptr = m_threshold.getData();
    data::SPtr<bool> detectShadowsSptr = m_detectShadows.getData();

    m_substractionBackGroundMOG = cv::createBackgroundSubtractorMOG2(*historySptr,*thresholdSptr,*detectShadowsSptr);
}

void backgroundSubstractorComponent::doTask()
{
    //getData and create Data
    data::SPtr<cv::Mat> frameInSptr = m_frameIn.getData();

    //do staff
    m_substractionBackGroundMOG->apply(*frameInSptr, *frameInSptr);


    //set Data
    m_frameOut.setData(frameInSptr);

}


}
}
