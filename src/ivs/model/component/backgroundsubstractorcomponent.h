#ifndef BACKGROUNDSUBSTRACTORCOMPONENT_H
#define BACKGROUNDSUBSTRACTORCOMPONENT_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/opencv.hpp>

namespace ivs
{

namespace component
{

class backgroundSubstractorComponent : public BaseComponent
{
public:
    IVS_DECL(backgroundSubstractorComponent);
    backgroundSubstractorComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    cv::Ptr<cv::BackgroundSubtractorMOG2> m_substractionBackGroundMOG;
    data::Data<cv::Mat> m_frameOut;
    data::Data<cv::Mat> m_frameIn;
    data::Data<int> m_history;
    data::Data<int> m_threshold;
    data::Data<bool> m_detectShadows;
};

}
}

#endif // BACKGROUNDSUBSTRACTORCOMPONENT_H
