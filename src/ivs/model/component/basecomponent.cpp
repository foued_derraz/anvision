#include "basecomponent.h"

namespace ivs
{
	namespace component
	{

		boost::mutex _accessLog;

		BaseComponent::BaseComponent() :
						m_name(initData("name", "Name of component", (std::string) "baseComponent", &m_name)),
                        m_verbose(initData("verbose", "verbose or not verbose? That is the question", false, &m_verbose)),
                        data(initData("data", "dataTest", { dataTest("tztq") }, &data))
		{

		}

		void BaseComponent::log(std::string str, helper::color textColor, helper::color forgroundColor)
		{
            if ((*m_verbose.getData()))
			{
				helper::log(str, textColor, forgroundColor);
			}
		}

		void BaseComponent::print(std::string str, helper::color textColor, helper::color forgroundColor)
		{
			helper::print((string) (this->getName()) + " : " + str, textColor, forgroundColor);
		}

	}
}
