/**
 * \file basecomponent.h
 * \brief abstract class basecomponent for components.

 *
 * abstract class basecomponent for components.
 */

#ifndef BASECOMPONENT_H
#define BASECOMPONENT_H

#include "baseobject.h"
#include "../data/data.h"
#include <fstream>
#include <boost/thread/mutex.hpp>
#include <boost/thread/lock_guard.hpp>
#include "../../helper/log.h"
#include "../data/datatest.h"

namespace ivs
{
namespace component
{


/**
 * @brief The BaseComponent class
 */
class BaseComponent : public BaseObject
{
public:
    IVS_DECL(BaseComponent)

    /**
     * @brief BaseComponent
     */
    BaseComponent();

    /**
     * @brief getName
     * @return name of the component
     */
    std::string getName() {return *m_name.getData(false,false);}

    void log(std::string, helper::color textColor = helper::color::BRIGHT_WHITE, helper::color forgroundColor = helper::color::BLACK);
    void print(std::string, helper::color textColor = helper::color::BRIGHT_WHITE, helper::color forgroundColor = helper::color::BLACK);


    /*const data::Data<std::string> initData(char* name,char* desc)
    {
        data::Data<std::string>* data = new data::Data<std::string>();
        data->initData(name,desc,(BaseObject*)this);
        this->addData(name,data);
        return *data;
    }*/
private:
    /**
     * @brief m_name data represent name of the component.
     */
    data::Data<std::string> m_name;
    data::Data<bool> m_verbose;
    data::Data<std::vector<dataTest> > data;
    std::ofstream* outStream = NULL;
};

}
}
#endif // BASECOMPONENT_H
