#ifndef BASEOBJECT_H
#define BASEOBJECT_H

#include <iostream>
#include <map>
#include "../data/basedata.h"
#include "../../helper/exception.h"
#include "../data/databuffer.h"

#define IVS_REGISTER(T)\
static factory::ComponentMaker<T> maker(#T);

#define IVS_DECL(T)                                                                                     \
template<class TData>                                                                                   \
data::Data<TData> initData(const char* name,const char* desc,TData defaultData,data::Data<TData>* data, int bufferType = data::CONSTANT_BUFFER , int bufferSize = 1 )              \
{                                                                                                       \
    *data = data::Data<TData>(bufferType,bufferSize);                                                                        \
    data->initData(name,defaultData,desc,(BaseObject*)this);                                            \
    this->addData(name,data);                                                                           \
    return *data;                                                                                       \
}                                                                                                       \
template<class TData>                                                                                   \
data::Data<TData> initData(const char* name,const char*desc,data::Data<TData>* data, int bufferType = data::LIFO_BUFFER , int bufferSize = 1)                                \
{\
    *data = data::Data<TData>(bufferType,bufferSize);                                                                        \
    data->initData(name,desc,(BaseObject*)this);                                                        \
    this->addData(name,data); \
    return *data;                                                                                       \
}\
template<class TData>                                                                                   \
const data::Data<TData> initLink(const char* name,const char*desc,data::BaseDataBuffer<TData>* link,data::Data<TData>* data)              \
{                                                                                                       \
    *data = data::Data<TData>(link);                                                                        \
    data->initData(name,*link,desc,(BaseObject*)this);                                                  \
    this->addData(name,data);                                                                           \
    return *data;                                                                                       \
}


namespace ivs
{

namespace component
{

class BaseObject
{
public:
    //more virtual function here
    virtual std::string getName()  = 0;
    virtual void doTask()  = 0;
    virtual void init()  = 0;
    virtual void bwdInit()  = 0;

    void addData(std::string name, data::BaseData* data){ dataList.insert(std::pair<std::string,data::BaseData*>(name,data));}
    void addData(data::BaseData* data){ addData(data->getName(),data);}

    bool hasData(std::string name){return dataList.find(name)!=dataList.end();}
    data::BaseData* getData(std::string name){
        if (!hasData(name))
            throw ivs::Exception("Component : " + getName() +" => there is no data " + name);
        return dataList[name];
    }

protected:
    std::map<std::string,data::BaseData*> dataList;

};


}

}
#endif // BASEOBJECT_H
