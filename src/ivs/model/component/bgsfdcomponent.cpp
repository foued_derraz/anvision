#include "bgsstaticcomponent.h"






using namespace std;

namespace ivs
{
namespace component
{

IVS_REGISTER(BGSStaticComponent);

    BGSStaticComponent::BGSStaticComponent(std::string): BaseComponent(),
    m_Prev(initData("Prev","Previous Frame", &m_Prev)),
    m_Next(initData("frameOut","Next Frame",&m_Next)),
	m_Forground(initData("frameIn","Forground", &m_Forground)),
    m_Thresh1(initData("Thresh1","Binary Threshold 1", 15,&m_Thresh1)),
	m_Thresh2(initData("Thresh2","Binary Threshold 2", 35,&m_Thresh2)),
	m_Blur(initData("Blur","Filter Blur 2D",std::vector<int>(),&m_Blur))
{
}


void BGSStaticComponent::init()
{

}

void BGSStaticComponent::bwdInit()
{
    data::SPtr<int> Thresh1Sptr = m_Thresh1.getData();
    data::SPtr<int> Thresh2Sptr = m_Thresh2.getData();
    data::SPtr<cv::Size> BlurSptr = m_Blur.getData();
}

void BGSStaticComponent::doTask()
{
    //getData and create Data
    data::SPtr<cv::Mat> PrevSptr = m_Prev.getData();
	data::SPtr<cv::Mat> NextSptr = m_Next.getData();
	
	//do staff
	cv::absdiff(PrevSptr, NextSptr, PrevSptr);
    cv::threshold(PrevSptr, PrevSptr, Thresh1Sptr, 255, THRESH_BINARY);
    cv::blur(PrevSptr, PrevSptr, Thresh2Sptr);
    cv::threshold(PrevSptr, PrevSptr, BlurSptr, 255, THRESH_BINARY);
	
    //set Data
    m_Forground.setData(PrevSptr);

}


}
}
