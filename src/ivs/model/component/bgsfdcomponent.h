#ifndef BGSSTATICCOMPONENT_H
#define BGSSTATICCOMPONENT_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/opencv.hpp>

namespace ivs
{

namespace component
{

class BGSStaticComponent : public BaseComponent
{
public:
    IVS_DECL(BGSStaticComponent);
    BGSStaticComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<cv::Mat> m_Prev;
    data::Data<cv::Mat> m_Next;
    data::Data<cv::Mat> m_Forground;
    data::Data<int> m_Thresh1;
    data::Data<int> m_Thresh2;
	data::Data<cv::Size> m_Blur;
};

}
}

#endif // BGSSTATICCOMPONENT_H