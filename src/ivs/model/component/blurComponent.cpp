#include "blurComponent.h"


namespace ivs
{
namespace component
{

IVS_REGISTER(BlurComponent);


BlurComponent::BlurComponent(std::string str): BaseComponent(),
    m_frameIn (initData("frameIn","frame d'entrée",&m_frameIn)),
    m_frameOut(initData("frameOut","blur frame en sortie",&m_frameOut)),
    m_sizeX(initData("sizeX","size kernel x",3,&m_sizeX)),
    m_sizeY(initData("sizeY","size kernel y",3,&m_sizeY))
{
}

void BlurComponent::init()
{
}

void BlurComponent::bwdInit()
{
}

void BlurComponent::doTask()
{
    //getData
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<int> sizeX = m_sizeX.getData();
    data::SPtr<int> sizeY = m_sizeY.getData();

    //do staff
    cv::blur(*frameSptr,*frameSptr,cv::Size(*sizeX,*sizeY));
    //cv::cvtColor(*frameSptr, *frameSptr, COLOR_BGR2GRAY);

    //set Data
    m_frameOut.setData(frameSptr);
}

}
}
