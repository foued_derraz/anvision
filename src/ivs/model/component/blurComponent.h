/**
  *
  */
#ifndef BLURCOMPONENT_H
#define BLURCOMPONENT_H

#include "basecomponent.h"
#include "../factory/componentmaker.h"
#include "../data/data.h"

namespace ivs
{

namespace component
{

/**
 * @brief The MyComponent class
 */
class BlurComponent : public BaseComponent{
public:
    IVS_DECL(BlurComponent);
    BlurComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<int> m_sizeX;
    data::Data<int> m_sizeY;
};

}
}
#endif // BLURCOMPONENT_H
