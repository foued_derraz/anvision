#include "cameraExposureRegulationComponent.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(cameraExposureRegulationComponent);

cameraExposureRegulationComponent::cameraExposureRegulationComponent(std::string str) :
				BaseComponent(),
				m_frameInColor(initData("m_frameInColor", "frame d'entrée BGR", &m_frameInColor)),
				m_in_luminostiyTarget(initData("m_in_luminostiyTarget", "seuil de foreground", 100, &m_in_luminostiyTarget)),
				m_in_cameraDevice(initData("m_in_cameraDevice", "camera device", 0, &m_in_cameraDevice))

{
	firstFrame = true;

}

void cameraExposureRegulationComponent::init()
{
}

void cameraExposureRegulationComponent::bwdInit()
{
}

void cameraExposureRegulationComponent::doTask()
{
	//getData
	data::SPtr<cv::Mat> frameInColor = m_frameInColor.getData();
	data::SPtr<int> in_luminostiyTarget = m_in_luminostiyTarget.getData();
	data::SPtr<int> in_cameraDevice = m_in_cameraDevice.getData();

	if (firstFrame)
	{
		if (*in_cameraDevice == 0)
		{
			commandBase = "v4l2-ctl -d /dev/video0 ";
		}
		else
		{
			commandBase = "v4l2-ctl -d /dev/video1 ";
		}
		cout << "working on camera " << *in_cameraDevice << endl;
		string command1 = commandBase + "-c exposure_auto=1"; // set to manual exposure
		system(command1.c_str());
		sleep(1);
		command1 = commandBase + "-c exposure_absolute=100";
		system(command1.c_str());
		sleep(1);
		currentExposure = 100;
		previous_error = 0;
		integral = 0;
		//return;
	}

	unsigned char * p_img;
	const int channels = (*frameInColor).channels();
	const int nRows = (*frameInColor).rows;
	const int nCols = (*frameInColor).cols * channels;

	const int stride = channels * 16;
	uint64 counter = 0;
	double sum = 0;
	for (int y = 0; y < nRows; y+=stride)
	{
		//cout << y << endl;
		p_img = (*frameInColor).ptr<unsigned char>(y) + 1; // first G of the line in bgr opencv format
		for (int x = 0; x < nCols; x += stride)
		{
			sum += *p_img;
			p_img += stride;
			counter++;
		}
	}
	double out_luminostiy = sum / counter;

	cout << "mesure = " << out_luminostiy << endl;

	//Ziegler–Nichols method
	const double Tu = 5; // oscillation period in Ziegler–Nichols : around 5,
	const double Ku = 0.4; // proportionnal gain causing oscillation : around 0.4, reduce to avoid oscillation
	const double Kp = Ku * 0.60;
	const double Ki = 2 * Kp / Tu;
	const double Kd = Kp * Tu / 8.0;

	double error = *in_luminostiyTarget - out_luminostiy;

	cout << "error = " << error << endl;

	integral = integral + error;
	double derivative = (error - previous_error);
	currentExposure = max(0.0, Kp * error + Ki * integral + Kd * derivative + 100);
	previous_error = error;

	cout << "currentExposure = " << currentExposure << endl;

	char newSetting[20];

	if (!firstFrame)
	{

		sprintf(newSetting, "%d", (int) currentExposure);
		string v4l2s;
		if (*in_cameraDevice == 0)
			v4l2s = "v4l2-ctl -d /dev/video0 -c exposure_absolute=" + string(newSetting);
		else
			v4l2s = "v4l2-ctl -d /dev/video1 -c exposure_absolute=" + string(newSetting);

		cout << v4l2s << endl;
		system(v4l2s.c_str());
	}
	firstFrame = false;


	sleep(1);

}

cameraExposureRegulationComponent::~cameraExposureRegulationComponent()
{
}

}
}
