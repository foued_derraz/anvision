/**
 *
 */
#ifndef LUMINOSITYMEASURECOMPONENT_H
#define LUMINOSITYMEASURECOMPONENT_H

#include "basecomponent.h"
#include "../factory/componentmaker.h"
#include "../data/data.h"

namespace ivs
{

namespace component
{

/**
 * @brief The MyComponent class
 */
class cameraExposureRegulationComponent: public BaseComponent
{
public:
	IVS_DECL(BlurComponent);
	cameraExposureRegulationComponent(std::string);
	~cameraExposureRegulationComponent();
	void doTask();
	void init();
	void bwdInit();
private:
	data::Data<cv::Mat> m_frameInColor;
	data::Data<int> m_in_luminostiyTarget;
	data::Data<int> m_in_cameraDevice;
	bool firstFrame;
	double currentExposure;

	double previous_error;
	double integral;
	string commandBase;


};

}
}
#endif // BGSMEDIANCOMPONENT_H
