#include "cameraacquisition.h"


namespace ivs
{
namespace component
{

using namespace cv;

IVS_REGISTER(CameraAcquisition);

CameraAcquisition::CameraAcquisition(std::string str):BaseComponent(),
    m_frame(initData("frame","Mat main_frame",&m_frame)),
    m_Capture(initData("Capture", "Camera Capture",(std::string)"0",&m_Capture)),
    m_fps(initData("fps","number of frame for camera",30.,&m_fps)),
    m_Height(initData("Height","Height of frame",0,&m_Height)),
    m_Width(initData("Width","width of frame",0,&m_Width)),
    m_CV_FOURCC(initData("fourCC","CV_FOURCC of frame",std::vector<char>(), &m_CV_FOURCC)),
    m_CV_CONVERT(initData("CV_CONVERT","CV_CONVERT of frame",false, &m_CV_CONVERT)),
    m_FrameCount(initData("FrameCount","CV_FrameCount of Frame",0, &m_FrameCount)),
    m_BufferSize (initData("BUFFERSIZE", "BufferSize of camera",0, &m_BufferSize)),
    m_POS_MSEC (initData("POS_MSEC", "POS_MSEC of frame",0,&m_POS_MSEC)),
    m_AVI_RATIO(initData("AVI_RATIO", "AVI_RATIO of frame",0,&m_AVI_RATIO)),
    m_FORMAT(initData("FORMAT", "PROP_FORMAT of frame",0,&m_FORMAT)),
    m_MODE(initData("MODE","PROP_MODE of frame",0,&m_MODE)),
    m_BRIGHTNESS(initData("BRIGHTNESS","PROP_BRIGHTNESS of frame",0,&m_BRIGHTNESS )),
    m_CONTRAST(initData("CONTRAST","PROP_CONTRAST of frame",0,&m_CONTRAST )),
    m_SATURATION(initData("SATURATION","PROP_SATURATION of frame",0,&m_SATURATION)),
    m_HUE(initData("HUE", "PROP_HUE of frame",0,&m_HUE)),
    m_GAIN(initData("GAIN","PROP_GAIN of frame",0, &m_GAIN)),
    m_EXPOSURE(initData("EXPOSURE","PROP_EXPOSURE of frame",0,&m_EXPOSURE )),
    m_RECTIFICATION(initData("RECTIFICATION","PROP_RECTIFICATION of frame",0,&m_RECTIFICATION)),
    m_FOCUS(initData("FOCUS","CAP_PROP_FOCUS of frame",15,&m_FOCUS)),
    m_AVFOUNDATION(initData("AVFOUNDATION","CAP_AVFOUNDATION of camera framework for iOS",1200,&m_AVFOUNDATION)),
    m_UNICAP(initData("UNICAP","CAP_UNICAP of camera Unicap drivers",600,&m_UNICAP)),
    m_simulateCamera(initData("simulateCamera","use to simulate camera with video",false,&m_simulateCamera)),
    m_isVideo(initData("isVideo","bool",false,&m_isVideo))
{
}

//initialisation of videocapture


void CameraAcquisition::init()
{

}

void CameraAcquisition::bwdInit()
{

    if(*m_isVideo.getData())
        m_cap = cv::VideoCapture(*m_Capture.getData());
    else
        m_cap = cv::VideoCapture(stoi(*m_Capture.getData()));

    if( !m_cap.isOpened() )
    {
        throw ivs::Exception("Camera cannot be open "+*m_Capture.getData());
    }

    if(m_fps.IsChanged())
        m_cap.set(CV_CAP_PROP_FPS, *m_fps.getData());
    else
        *m_fps.getData() = m_cap.get(CV_CAP_PROP_FPS);

    if (m_CV_FOURCC.IsChanged())
        m_cap.set(CV_CAP_PROP_FOURCC ,CV_FOURCC((*m_CV_FOURCC.getData())[0],(*m_CV_FOURCC.getData())[1],(*m_CV_FOURCC.getData())[2],(*m_CV_FOURCC.getData())[3]));
    else
        m_cap.set(CV_CAP_PROP_FOURCC ,CV_FOURCC('Y', 'U', 'Y', 'V'));

    if(m_FrameCount.IsChanged())
        m_cap.set(CV_CAP_PROP_FRAME_COUNT, *m_FrameCount.getData());
    else
        *m_FrameCount.getData() = m_cap.get(CV_CAP_PROP_FRAME_COUNT);

    if(m_Height.IsChanged())
        m_cap.set(CV_CAP_PROP_FRAME_HEIGHT, *m_Height.getData());
    else
        *m_Height.getData() = m_cap.get(CV_CAP_PROP_FRAME_HEIGHT);

    if(m_Width.IsChanged())
        m_cap.set(CV_CAP_PROP_FRAME_WIDTH, *m_Width.getData());
    else
        *m_Width.getData() = m_cap.get(CV_CAP_PROP_FRAME_WIDTH);

    if(m_CV_CONVERT.IsChanged())
        m_cap.set(CV_CAP_PROP_CONVERT_RGB, *m_CV_CONVERT.getData());
    else
        *m_CV_CONVERT.getData() = m_cap.get(CV_CAP_PROP_CONVERT_RGB);

    if(m_BufferSize.IsChanged())
        m_cap.set(CV_CAP_PROP_BUFFERSIZE, *m_BufferSize.getData());
    else
        *m_BufferSize.getData() = m_cap.get(CV_CAP_PROP_BUFFERSIZE);

    if(m_POS_MSEC.IsChanged())
        m_cap.set(CV_CAP_PROP_POS_MSEC, *m_POS_MSEC.getData());
    else
        *m_POS_MSEC.getData() = m_cap.get(CV_CAP_PROP_POS_MSEC);

    if(m_AVI_RATIO.IsChanged())
        m_cap.set(CAP_PROP_POS_AVI_RATIO, *m_AVI_RATIO.getData());
    else
        *m_AVI_RATIO.getData() = m_cap.get(CAP_PROP_POS_AVI_RATIO);

    if(m_FORMAT.IsChanged())
        m_cap.set(CAP_PROP_FORMAT, *m_FORMAT.getData());
    else
        *m_FORMAT.getData() = m_cap.get(CAP_PROP_FORMAT);

    if(m_MODE.IsChanged())
        m_cap.set(CAP_PROP_MODE, *m_MODE.getData());
    else
        *m_MODE.getData() = m_cap.get(CAP_PROP_MODE);

    if(m_BRIGHTNESS.IsChanged())
        m_cap.set(CAP_PROP_BRIGHTNESS, *m_BRIGHTNESS.getData());
    else
        *m_BRIGHTNESS.getData() = m_cap.get(CAP_PROP_BRIGHTNESS);

    if(m_CONTRAST.IsChanged())
        m_cap.set(CAP_PROP_CONTRAST, *m_CONTRAST.getData());
    else
        *m_CONTRAST.getData() = m_cap.get(CAP_PROP_CONTRAST);

    if(m_SATURATION.IsChanged())
        m_cap.set(CAP_PROP_SATURATION, *m_SATURATION.getData());
    else
        *m_SATURATION.getData() = m_cap.get(CAP_PROP_SATURATION);

    if(m_SATURATION.IsChanged())
        m_cap.set(CAP_PROP_SATURATION, *m_SATURATION.getData());
    else
        *m_SATURATION.getData() = m_cap.get(CAP_PROP_SATURATION);

    if(m_HUE.IsChanged())
        m_cap.set(CAP_PROP_HUE, *m_HUE.getData());
    else
        *m_HUE.getData() = m_cap.get(CAP_PROP_HUE);

    if(m_GAIN.IsChanged())
        m_cap.set(CAP_PROP_GAIN, *m_GAIN.getData());
    else
        *m_GAIN.getData() = m_cap.get(CAP_PROP_GAIN);

    if(m_EXPOSURE.IsChanged())
        m_cap.set(CAP_PROP_EXPOSURE, *m_EXPOSURE.getData());
    else
        *m_EXPOSURE.getData() = m_cap.get(CAP_PROP_EXPOSURE);

    if(m_RECTIFICATION.IsChanged())
        m_cap.set(CAP_PROP_RECTIFICATION, *m_RECTIFICATION.getData());
    else
        *m_RECTIFICATION.getData() = m_cap.get(CAP_PROP_RECTIFICATION);

    if(m_FOCUS.IsChanged())
        m_cap.set(CAP_PROP_FOCUS, *m_FOCUS.getData());
    else
        *m_FOCUS.getData() = m_cap.get(CAP_PROP_FOCUS);

    if(m_AVFOUNDATION.IsChanged())
        m_cap.set(CAP_AVFOUNDATION, *m_AVFOUNDATION.getData());
    else
        *m_AVFOUNDATION.getData() = m_cap.get(CAP_AVFOUNDATION);

    if(m_UNICAP.IsChanged())
        m_cap.set(CAP_UNICAP, *m_UNICAP.getData());
    else
        *m_UNICAP.getData() = m_cap.get(CAP_UNICAP);


    data::SPtr<bool> simulateCamera = m_simulateCamera.getData();
    if(*simulateCamera)
    {

        prevFrame = data::SPtr<cv::Mat>(new cv::Mat);
        m_cap >> *prevFrame;
        while ((*prevFrame).empty()) {
            m_cap >> *prevFrame;
        }
        std::cout << "test12: " << (*prevFrame).cols << std::endl;

    }
}

void CameraAcquisition::doTask()
{
    //create Data and get data
    data::SPtr<cv::Mat> sptr(new cv::Mat());
    data::SPtr<bool> simulateCamera = m_simulateCamera.getData();

    if( *simulateCamera )
    {
        data::SPtr<double> fps = m_fps.getData();
        //boost::this_thread::sleep_for(boost::chrono::milliseconds((int)(1000. / *fps)));
        m_cap >> *sptr;

        //std::cout << "test12: " << (*sptr).cols << " " << (*prevFrame).cols << std::endl;
        if((*sptr).cols == 0)
        {
            *sptr = cv::Mat::zeros(800,600,CV_8UC3);
        }
        else
        {
            //compute difference between two frames (test)
            cv::Mat diff = *sptr - *prevFrame;
            cv::cvtColor(diff,diff,CV_BGR2GRAY);
            cv::Scalar  sum = cv::sum(diff);

            while(sum[0] <= 0.3*(sptr->cols*sptr->rows))
            {
                m_cap >> *sptr;
                diff = *sptr - *prevFrame;
                cv::cvtColor(diff,diff,CV_BGR2GRAY);
                sum = cv::sum(diff);
            }


             //std::cout << "test : " << sum << std::endl;
            //set all data

            data::SPtr<cv::Mat> prev (new cv::Mat() );
            sptr->copyTo(*prev);
            prevFrame = prev;
        }



        m_frame.setData(sptr);

    }
    else
    {
        m_cap >> *sptr;
        if((*sptr).cols == 0)
        {
            *sptr = cv::Mat::zeros(200,200,CV_8UC3);
        }

        //set all data
        m_frame.setData(sptr);
    }

}

}
}
