#ifndef CAMERAACQUISITION_H
#define CAMERAACQUISITION_H

#include "basecomponent.h"
#include "opencv2/opencv.hpp"
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "../../helper/exception.h"
#include "opencv2/highgui/highgui.hpp"

namespace ivs
{
namespace component
{


class CameraAcquisition: public BaseComponent
{
public:

    IVS_DECL(CameraAcquisition);
    /**
     * @brief CameraAcquisition: constructor of camera acquisition
     */
    CameraAcquisition();

    /**
     * constructor of CameraAcquisition with the name of the camera acquisition in the json
     */
    CameraAcquisition(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<cv::Mat> m_frame;
    data::Data<double> m_fps;
    data::Data<int> m_Height;
    data::Data<int> m_Width;
    data::Data<std::vector<char>>m_CV_FOURCC;
    data::Data<bool> m_CV_CONVERT;
    data::Data<int> m_FrameCount;
    data::Data<std::string> m_Capture;
    data::Data<int> m_BufferSize;
    data::Data<int> m_POS_MSEC;
    data::Data<int> m_AVI_RATIO;
    data::Data<int> m_FORMAT;
    data::Data<int> m_MODE;
    data::Data<int> m_BRIGHTNESS;
    data::Data<int> m_CONTRAST;
    data::Data<int> m_SATURATION;
    data::Data<int> m_HUE;
    data::Data<int> m_GAIN;
    data::Data<int> m_EXPOSURE;
    data::Data<int> m_RECTIFICATION;
    data::Data<int> m_FOCUS;
    data::Data<int> m_AVFOUNDATION;
    data::Data<int> m_UNICAP;
    data::Data<bool> m_simulateCamera;
    data::Data<bool> m_isVideo;




    cv::VideoCapture m_cap;

    data::SPtr<cv::Mat> prevFrame;

};

bool matIsEqual(const cv::Mat mat1, const cv::Mat mat2);
}

}
#endif // CAMERAACQUISITION_H
