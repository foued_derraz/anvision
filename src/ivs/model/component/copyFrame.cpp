#include "copyFrame.h"
#include "../factory/componentmaker.h"


namespace ivs
{
namespace component
{

using namespace cv;

IVS_REGISTER(CopyFrame);


CopyFrame::CopyFrame(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut1(initData("frameOut1", "Mat frame",&m_frameOut1)),
    m_frameOut2(initData("frameOut2", "Mat copy frame",&m_frameOut2))
{
}

void CopyFrame::init()
{

}

void CopyFrame::bwdInit()
{

}

void CopyFrame::doTask()
{



    //getData
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<cv::Mat> frameCopy = data::SPtr<cv::Mat>(new cv::Mat());



    //do staff
    frameSptr->copyTo(*frameCopy);


    //set Data
    m_frameOut1.setData(frameSptr);
    m_frameOut2.setData(frameCopy);
}

}
}
