#ifndef COPYFRAME_H
#define COPYFRAME_H
#include "basecomponent.h"
#include "opencv2/opencv.hpp"
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "../../helper/exception.h"

namespace ivs
{
namespace component
{

class CopyFrame : public BaseComponent
{
public:

    IVS_DECL(CopyFrame);
    CopyFrame();
    CopyFrame(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:

   data::Data<cv::Mat> m_frameIn;
   data::Data<cv::Mat> m_frameOut1;
   data::Data<cv::Mat> m_frameOut2;


};


}
}

#endif // COPYFRAME_H
