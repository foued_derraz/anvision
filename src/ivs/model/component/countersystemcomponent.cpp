#include "countersystemcomponent.h"



namespace ivs
{

namespace component
{

IVS_REGISTER(counterSystemComponent);

counterSystemComponent::counterSystemComponent(std::string str):BaseComponent(),
    m_newValue(initData("newValue","The new value enter",&m_newValue)),
    m_newValueOut(initData("newValueOut","The new value enter",&m_newValueOut)),
    m_compteur(initData("compteur","flow evaluation counter",&m_compteur)),
    m_threshold(initData("threshold","lowess thershold for amplitude between maxima and minima",20.0,&m_threshold)),
    m_nbAreaActive(initData("nbAreaActive","number of area active",&m_nbAreaActive))
{
}


void counterSystemComponent::init()
{

}

void counterSystemComponent::bwdInit()
{
    for(int i=0; i < 2 ; i++)
        m_listValue.push_back(0.);

    m_myCounter = 0;
    m_sup = 0;
    m_inf = 0;
    m_frameEnCours = 0;
    m_areaAtZero = 0;
}

void counterSystemComponent::doTask()
{
    data::SPtr<double> newValue = m_newValue.getData();
    data::SPtr<int> compteur = data::SPtr<int>(new int);
    data::SPtr<double> threshold = m_threshold.getData();
    data::SPtr<int> nbAreaActive = m_nbAreaActive.getData();


    m_listValue.push_front(*newValue);
    m_listValue.pop_back();

    m_frameEnCours++;

    bool verb(false);

    //double seuil = 20.0;


    if( m_listValue[0]<0 &&  m_listValue[1]>0)
        m_areaAtZero = *nbAreaActive;


    if( m_listValue[0]>0)
    {
        if(m_listValue[1]>0)
        {
            if(m_listValue[0]>m_sup)
            {
                m_sup = m_listValue[0];
            }
        }
        else
        {
            //compter
            double amplitude = (m_sup-m_inf);
            if(amplitude>(*threshold))
            {
                if(m_areaAtZero>=2)
                {
                    std::cout << "sup " << m_sup << "   inf " << m_inf << "   amplitude " << amplitude << std::endl;
                    std::cout << "nb zone " << m_areaAtZero << std::endl;
                    m_myCounter++;
                    verb=true;
                }
                if(m_areaAtZero>=6)
                    m_myCounter++;
                if(m_areaAtZero>=8)
                    m_myCounter++;
                //if(amplitude>350)
                    //m_myCounter++;
            }
            m_sup=0;
            m_inf=0;
        }
    }
    else
    {
        if(m_listValue[1]<0)
        {
            if(m_listValue[0]<m_inf)
            {
                m_inf = m_listValue[0];
            }
        }
        else
        {
            m_inf=m_listValue[0];
        }
    }


    if(verb)
    {
        std::cout << "compteur " << m_myCounter << std::endl;
    }

    *compteur = m_myCounter;

    m_compteur.setData(compteur);
    m_newValueOut.setData(newValue);

}

}
}
