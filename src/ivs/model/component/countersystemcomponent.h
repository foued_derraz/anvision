#ifndef COUNTERSYSTEMCOMPONENT_H
#define COUNTERSYSTEMCOMPONENT_H


#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include <deque>


namespace ivs
{

namespace component
{


class counterSystemComponent : public BaseComponent
{
public:
    IVS_DECL(counterSystemComponent);
    counterSystemComponent(string str);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<int> m_compteur;
    int m_myCounter;
    data::Data<double> m_newValue;
    data::Data<double> m_newValueOut;
    data::Data<double> m_threshold;
    data::Data<int> m_nbAreaActive;


    std::deque<double> m_listValue;
    double m_sup;
    double m_inf;
    int m_frameEnCours;
    int m_areaAtZero;
};

}
}

#endif // COUNTERSYSTEMCOMPONENT_H
