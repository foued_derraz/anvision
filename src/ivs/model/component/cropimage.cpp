#include "cropimage.h"
#include <iostream>


//#include "../factory/componentmaker.h"




namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(CropImage);


CropImage::CropImage(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_dimension(initData("dimension","premier point",cv::Point2i(1.0,1.0),&m_dimension)),
    m_origin(initData("origin","second point",cv::Point2i(0.0,0.0),&m_origin)),
    m_Ndimension(initData("Ndimension","premier point",cv::Point2d(1.0,1.0),&m_Ndimension)),
    m_Norigin(initData("Norigin","second point",cv::Point2d(0.0,0.0),&m_Norigin))
{

}

void CropImage::init()
{
    //std::cout << "MyComponent : init " <<  this->getName() << std::endl;
}

void CropImage::bwdInit()
{   

    if(m_dimension.IsChanged()) NormedMode = false;

    data::SPtr<cv::Point2d> tmpdim = m_Ndimension.getData();
    std::cout << "size dim " << tmpdim->x <<", " << tmpdim->y <<  std::endl;
}

void CropImage::doTask()
{

    data::SPtr<cv::Mat> frameSptr= m_frameIn.getData();

    cv::Point2d size= cv::Point2d(frameSptr->cols,frameSptr->rows);


    if(this->NormedMode)
    {
        data::SPtr<cv::Point2d> dim = data::SPtr<cv::Point2d>(new cv::Point2d(0,0));
        data::SPtr<cv::Point2d> ori = data::SPtr<cv::Point2d>(new cv::Point2d(0,0));
        data::SPtr<cv::Point2d> tmpdim = m_Ndimension.getData();
        dim->x =  tmpdim->x * size.x;
        dim->y = tmpdim->y * size.y;
        data::SPtr<cv::Point2d> tmpOri = m_Norigin.getData();
        ori->x =  tmpOri->x * size.x;
        ori->y =  tmpOri->y * size.y;

        if(dim->x == 0 || dim->y == 0)
        {
            throw Exception("Error :  Ndimension or Norigin equal to zero ");
        }

        else if(ori->x + dim->x > size.x || ori->y + dim->y > size.y)
        {
            throw Exception("Error :  Ndimension or Norigin too big ");
        }

        Rect region_of_interest = Rect(ori->x,ori->y,dim->x,dim->y);
        data::SPtr<cv::Mat> roi= data::SPtr<cv::Mat>(new cv::Mat((*frameSptr),(region_of_interest)));

        m_frameOut.setData(roi);
    }
    else
    {
        data::SPtr<cv::Point2i> dim=m_dimension.getData();
        data::SPtr<cv::Point2i> ori=m_origin.getData();

        if(dim->x == 0 || dim->y == 0)
        {
            throw Exception("Error :  dimension or origin equal to zero ");
        }
        else if((ori->x + dim->x) > size.x || (ori->y + dim->y) > size.y)
        {
            throw Exception("Error :  dimension or origin too big ");
        }

        Rect region_of_interest = Rect(ori->x,ori->y,dim->x,dim->y);
        data::SPtr<cv::Mat> roi= data::SPtr<cv::Mat>(new cv::Mat((*frameSptr),(region_of_interest)));

        m_frameOut.setData(roi);
    }

}





}
}


