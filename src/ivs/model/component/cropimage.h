#ifndef CROPIMAGE_H
#define CROPIMAGE_H
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "basecomponent.h"
#include "opencv2/core.hpp"


namespace ivs
{

namespace component
{


class CropImage: public BaseComponent
{
public:
    IVS_DECL(CropImage);
    CropImage(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<cv::Point2i> m_origin;
    data::Data<cv::Point2i> m_dimension;
    data::Data<cv::Point2d> m_Norigin;
    data::Data<cv::Point2d> m_Ndimension;

    bool NormedMode = true;

};

}
}

#endif // CROPIMAGE_H
