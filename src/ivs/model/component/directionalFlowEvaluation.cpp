#include "directionalFlowEvaluation.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(DirectionalFlowEvaluation);

DirectionalFlowEvaluation::DirectionalFlowEvaluation(std::string str): BaseComponent(),
    m_uflow(initData("Uflow","exit data of optical flow",&m_uflow)),
    m_angleWanted(initData("angleWanted","angle wanted",-1.0,&m_angleWanted)),
    m_leftSide(initData("leftSide","value in left direction",&m_leftSide)),
    m_rightSide(initData("rightSide","value in right direction",&m_rightSide)),
    m_upSide(initData("upSide","value in up direction",&m_upSide)),
    m_downSide(initData("downSide","value in down direction",&m_downSide)),
    m_newValue(initData("newValue","The new value enter",&m_newValue)),
    m_vecSumFlowOut(initData("vecSumFlowOut","Vector of Sum of Flow",&m_vecSumFlowOut)),
    m_nbArea(initData("nbArea","number of area",10,&m_nbArea)),
    m_nbAreaActive(initData("nbAreaActive","number of area active",&m_nbAreaActive))
{

}

void DirectionalFlowEvaluation::init()
{

}

void DirectionalFlowEvaluation::bwdInit()
{

}

void DirectionalFlowEvaluation::doTask()
{
    //---------------get Data
    data::SPtr<cv::Mat> UFlowSptr = m_uflow.getData();
    data::SPtr<int> nbArea = m_nbArea.getData();

    data::SPtr<double> newValue = data::SPtr<double>(new double);

    data::SPtr<int> nbAreaActive = data::SPtr<int>(new int);
    *nbAreaActive = 0;

    if(m_angleWanted.IsChanged())
    {
        data::SPtr<double> angleWanted = m_angleWanted.getData();

        cv::Mat xy[2];
        cv::split(*UFlowSptr,xy);

        cv::Mat magnitude, angle;
        cv::cartToPolar(xy[0], xy[1], magnitude, angle, true);

        double k = 0.005;
        double sum = 0;
        double x = 0;
        double center = *angleWanted; //90;

        data::SPtr<std::vector<double>> vecSumFlowOut = data::SPtr<vector<double>>(new std::vector<double>);
        //std::cout << "size vecSumFlowOut" << (*vecSumFlowOut).size() << std::endl;

        for(int i=0; i<magnitude.cols;i++)
            (*vecSumFlowOut).push_back(0.);

        for(int i=0; i<magnitude.cols;i++)
            (*vecSumFlowOut)[i]=0.;

        //Calcule pour chaque colonne : résultat mis dans le vecteur vecSumFlowOut
/*
        for ( unsigned int i = 0 ; i < magnitude.rows ; i++)
        {
            float* row_ptr_angle = angle.ptr<float>(i);
            float* row_ptr_magnitude = magnitude.ptr<float>(i);

            for(unsigned int j = 0; j < magnitude.cols; j++)
            {
                x= (mod((180 + row_ptr_angle[j] - center ),360))-180;
                x= exp( -k*x*x );
                (*vecSumFlowOut)[j] +=  row_ptr_magnitude[j] * x;
            }
        }*/


        //Calcul pour la totalité de la fenêtre
/*        for ( unsigned int i = 0 ; i < magnitude.rows ; i++)
        {
            float* row_ptr_angle = angle.ptr<float>(i);
            float* row_ptr_magnitude = magnitude.ptr<float>(i);

            for(unsigned int j = 0; j < magnitude.cols; j++)
            {
                x= (mod((180 + row_ptr_angle[j] - center ),360))-180;
                x= exp( -k*x*x );
                sum +=  row_ptr_magnitude[j] * x;
            }
        }
*/

        double sum2 = 0.;

        for(unsigned int lz = 0; lz < magnitude.cols; lz+=magnitude.cols/(*nbArea))
        {
            sum2 = 0.;
            for ( unsigned int i = 0 ; i < magnitude.rows ; i++)
            {
                float* row_ptr_angle = angle.ptr<float>(i);
                float* row_ptr_magnitude = magnitude.ptr<float>(i);

                for(unsigned int j = lz; j < (lz+(magnitude.cols/(*nbArea))); j++)
                {
                    x= (mod((180 + row_ptr_angle[j] - center ),360))-180;
                    x= exp( -k*x*x );
                    sum +=  row_ptr_magnitude[j] * x;
                    sum2 += row_ptr_magnitude[j] * x;
                }
            }
            if(sum2>1)
                (*nbAreaActive)++;
        }

//        std::cout << "(*nbAreaActive)  " << (*nbAreaActive) << std::endl;


        *newValue = sum;
        m_newValue.setData(newValue);
        m_vecSumFlowOut.setData(vecSumFlowOut);
        m_nbAreaActive.setData(nbAreaActive);
    }
    else
    {
        data::SPtr<double> leftSide = data::SPtr<double>(new double);
        data::SPtr<double> rightSide = data::SPtr<double>(new double);
        data::SPtr<double> downSide = data::SPtr<double>(new double);
        data::SPtr<double> upSide = data::SPtr<double>(new double);

        cv::Mat uv[2];
        cv::split(*UFlowSptr,uv);

        double sumLeft = 0;
        double sumRight = 0;
        double sumUp = 0;
        double sumDown = 0;

        for ( unsigned int i = 0 ; i < uv[0].rows ; i++)
        {
            float* row_ptr_x = uv[0].ptr<float>(i);
            float* row_ptr_y = uv[1].ptr<float>(i);


            for(unsigned int j = 0; j < uv[0].cols; j++)
            {
                if(row_ptr_x[j] < 0)
                    sumLeft += row_ptr_x[j];

                if(row_ptr_x[j] > 0)
                    sumRight += row_ptr_x[j];

                if(row_ptr_y[j] > 0)
                    sumDown += row_ptr_y[j];

                if(row_ptr_y[j] < 0)
                    sumUp += row_ptr_y[j];
            }
        }

        *leftSide = sumLeft;
        *rightSide = sumRight;
        *downSide = sumDown;
        *upSide = sumUp;


        m_leftSide.setData(leftSide);
        m_rightSide.setData(rightSide);
        m_downSide.setData(downSide);
        m_upSide.setData(upSide);
    }

}

double mod(double x, double N)
{
    return fmod(((x < 0) ? (fmod(x,N) + N) : x) , N);
}


}
}
