/**
  *
  */
#ifndef DIRECTIONALFLOWEVALUATION_H
#define DIRECTIONALFLOWEVALUATION_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "../data/datatest.h"
#include <deque>


namespace ivs
{

namespace component
{

/**
 * @brief The MyComponent class
 */
class DirectionalFlowEvaluation : public BaseComponent{
public:
    IVS_DECL(DirectionalFlowEvaluation);
    DirectionalFlowEvaluation(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_uflow;
    data::Data<double> m_angleWanted;
    data::Data<double> m_rightSide;
    data::Data<double> m_leftSide;
    data::Data<double> m_downSide;
    data::Data<double> m_upSide;
    data::Data<double> m_newValue;
    data::Data<vector<double>> m_vecSumFlowOut;
    data::Data<int> m_nbArea;
    data::Data<int> m_nbAreaActive;

};


double mod(double x, double N);

}
}
#endif // DIRECTIONALFLOWEVALUATION_H
