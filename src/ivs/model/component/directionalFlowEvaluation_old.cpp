#include "directionalFlowEvaluation.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(DirectionalFlowEvaluation);

//static factory::ComponentMaker<MyComponent> maker("MyComponent");

DirectionalFlowEvaluation::DirectionalFlowEvaluation(std::string str): BaseComponent(),
    m_uflow(initData("Uflow","exit data of optical flow",&m_uflow)),
    graphOut(initData("graphOut","graph",&graphOut)),
    m_compteur(initData("flowEvaluationCounter","flow evaluation counter",0,&m_compteur)),
    m_sizeWindow(initData("sizeWindow","size of std::deque",100,&m_sizeWindow)),
    m_sizeThreshold(initData("sizeThreshold","size Threshold for method 3",25,&m_sizeThreshold)),
    m_thresholdMinMoy(initData("thresholdMinMoy","threshold min for mean",200,&m_thresholdMinMoy))
{
    data::SPtr<int> sizeWindow = m_sizeWindow.getData();
    //Initialisation of std::deque
    for(unsigned int i = 0;i<(*sizeWindow);i++)
    {
        values.push_back(0.0);
        counters.push_back(0);
        finalCounters.push_back(0);
    }

    max = 0;
    actualFrame = 0;

    //For method two
    m_compteur1=0;
    m_compteur2=0;
}

void DirectionalFlowEvaluation::init()
{

}

void DirectionalFlowEvaluation::bwdInit()
{

}

void DirectionalFlowEvaluation::doTask()
{
    //---------------get Data
    data::SPtr<cv::Mat> UFlowSptr = m_uflow.getData();
    data::SPtr<cv::Mat> graphSptr = data::SPtr<cv::Mat>(new cv::Mat());
    data::SPtr<int> compteur = m_compteur.getData();
    data::SPtr<int> sizeThreshold = m_sizeThreshold.getData();
    data::SPtr<int> thresholdMinMoy = m_thresholdMinMoy.getData();

    //--------------Compute Values of Direction Flow
    double sum = computeValuesDirectionFlow(UFlowSptr);
    values.push_back(sum);

    if(sum>max)
        max =sum;


    //----------------Signal processing
    deque<double> valuesMA3;
    double moy = 0;
    deque<double> valuesDiff;

    signalProcessing(&values, &valuesMA3, &valuesDiff, &moy, thresholdMinMoy);


    //-------------------Counter Methods
    counterMethodOne(&valuesDiff, &moy, compteur, &framesWithPeople, &actualFrame);
    //counterMethodTwo(&valuesDiff, &moy, compteur, &m_compteur1, &m_compteur2, &framesWithPeople, &actualFrame);
    //counterMethodThree(&valuesDiff, &moy, compteur, &counters, &finalCounters, &framesWithPeople, &actualFrame, sizeThreshold);

    log(std::to_string(*compteur));


    actualFrame++;

    values.pop_front();

    //-----------------Drawing
    *graphSptr = cv::Mat(255,1000,CV_8UC3);
    graphSptr->setTo(255);
    drawingLine(graphSptr, &valuesDiff, &moy, &max, &valuesMA3, &finalCounters);

    //----------------Set Data
    graphOut.setData(graphSptr);
    m_compteur.setData(compteur);

}

double mod(double x, double N)
{
    return fmod(((x < 0) ? (fmod(x,N) + N) : x) , N);
}

double computeValuesDirectionFlow(data::SPtr<cv::Mat> UFlowSptr)
{
    cv::Mat xy[2];
    cv::split(*UFlowSptr,xy);

    cv::Mat magnitude, angle;
    cv::cartToPolar(xy[0], xy[1], magnitude, angle, true);

    double k = 0.005;
    double sum = 0;
    double x = 0;
    double center = 90;
    for ( unsigned int i = 0 ; i < magnitude.rows ; i++)
    {

        float* row_ptr_angle = angle.ptr<float>(i);
        float* row_ptr_magnitude = magnitude.ptr<float>(i);
        for(unsigned int j = 0; j < magnitude.cols; j++)
        {

            x= (mod((180 + row_ptr_angle[j] - center ),360))-180;
            x= exp( -k*x*x );
            sum +=  row_ptr_magnitude[j] * x;

        }
    }

    return sum;

}

void signalProcessing(std::deque<double> *values, std::deque<double> *valuesMA3, std::deque<double> *valuesDiff, double *moy, data::SPtr<int> thresholdMinMoy)
{
    //________moy3
    for(int i=1; i<((*values).size()-1);i++)
    {
        (*valuesMA3).push_back(((*values)[i-1]+(*values)[i]+(*values)[i+1])/3);
    }

    //________moy100
    for(int i=0; i<((*values).size());i++)
    {
        (*moy)+=(*values)[i];
    }
    (*moy) = ((*moy)/(*values).size());
    if((*moy)<(*thresholdMinMoy))
        (*moy)=(*thresholdMinMoy);

    //_______________diff
    for(int i=0; i<(*valuesMA3).size();i++)
    {
        (*valuesDiff).push_back((*valuesMA3)[i]-(*moy));
    }
}

void counterMethodOne(std::deque<double> *valuesDiff, double *moy, data::SPtr<int> compteur, vector<int> *framesWithPeople, int *actualFrame, int position)
{
    if((*valuesDiff)[((*valuesDiff).size())-position] > (*moy) && (*valuesDiff)[((*valuesDiff).size())-position-1] < (*moy) && (*valuesDiff)[((*valuesDiff).size())-position-2] < (*moy) && (*valuesDiff)[((*valuesDiff).size())-position-3] < (*moy))
    {
        (*compteur)++;
        (*framesWithPeople).push_back((*actualFrame)-position);
        for(int i=0; i<(*framesWithPeople).size(); i++)
        {
            std::cout << (*framesWithPeople)[i] << "  ";
        }

        std::cout << std::endl;
    }
}

void counterMethodTwo(std::deque<double> *valuesDiff, double *moy, data::SPtr<int> compteur, int *compteur1, int *compteur2, vector<int> *framesWithPeople, int *actualFrame, int position)
{
    if((*valuesDiff)[((*valuesDiff).size())-position] > (*moy))
    {
        if((*valuesDiff)[((*valuesDiff).size())-position-1] < (*moy) && (*valuesDiff)[((*valuesDiff).size())-position-2] < (*moy) && (*valuesDiff)[((*valuesDiff).size())-position-3] < (*moy))
        {
            (*compteur1)++;
            (*framesWithPeople).push_back((*actualFrame)-position);
            for(int i=0; i<(*framesWithPeople).size(); i++)
            {
                std::cout << (*framesWithPeople)[i] << "  ";
            }

            std::cout << std::endl;
        }

        if((*valuesDiff)[((*valuesDiff).size())-position-1] < (*moy) && (*valuesDiff)[((*valuesDiff).size())-position-2] < (*moy))
        {
            (*compteur2)++;
            /*(*framesWithPeople).push_back((*actualFrame)-position);
            for(int i=0; i<(*framesWithPeople).size(); i++)
            {
                std::cout << (*framesWithPeople)[i] << "  ";
            }

            std::cout << std::endl;
            */
        }
    }

    *compteur= ceil(((*compteur1)+(*compteur2))/2);
}

void counterMethodThree(std::deque<double> *valuesDiff, double *moy, data::SPtr<int> compteur, std::deque<int> *counters, std::deque<int> *finalCounters, vector<int> *framesWithPeople, int *actualFrame, data::SPtr<int> seuil)
{
    //add one counters
    if((*valuesDiff)[(*valuesDiff).size()-1] > (*moy))
        (*counters).push_back(1);
    else
        (*counters).push_back(0);
    (*counters).pop_front();


    //update all counters
    for(int i=1;i< (*valuesDiff).size();i++)
    {
        if((*valuesDiff)[i]> (*moy))
            (*counters)[i]++;
    }

    (*finalCounters).push_back((*counters)[0]);
    (*finalCounters).pop_front();

    if( (*finalCounters)[0]>(*seuil) && (*finalCounters)[1]<(*seuil) && (*finalCounters)[2] < (*seuil) && (*finalCounters)[3] < (*seuil))
    {
        (*compteur)++;
        (*framesWithPeople).push_back((*actualFrame)-((*valuesDiff).size()*2));
        for(int i=0; i<(*framesWithPeople).size(); i++)
        {
            std::cout << (*framesWithPeople)[i] << "  ";
        }

        std::cout << std::endl;
    }

}

void drawingLine(data::SPtr<cv::Mat> graphSptr, std::deque<double> *valuesDiff, double *moy, double *max, std::deque<double> *valuesMA3, std::deque<int> *finalCounters)
{
    double coef = (*max)/255;

    /*for(unsigned int i=1;i<valuesMA11.size();i++)
    {
        //if(values[i-1]/12>255) values[i-1] = 255/12;
        cv::line(*graphSptr,cv::Point2d(1000-(i-1)*10,250-valuesMA11[i-1]/coef),cv::Point2d((1000-i*10),250-valuesMA11[i]/coef),cv::Scalar( 200, 200, 0 ));

    }*/

    cv::line(*graphSptr,cv::Point2d(0,255 - (*moy)/coef),cv::Point2d(1000,255 - (*moy)/coef),cv::Scalar( 0, 200, 0 ));

    for(unsigned int i=1;i<(*valuesMA3).size();i++)
    {
        cv::line(*graphSptr,cv::Point2d(1000-(i-1)*10,250-(*valuesMA3)[i-1]/coef),cv::Point2d((1000-i*10),250-(*valuesMA3)[i]/coef),cv::Scalar( 0, 200, 0 ));
    }

    for(unsigned int i=1;i<(*valuesDiff).size();i++)
    {
        cv::line(*graphSptr,cv::Point2d(1000-(i-1)*10,250-(*valuesDiff)[i-1]/coef),cv::Point2d((1000-i*10),250-(*valuesDiff)[i]/coef),cv::Scalar( 200, 0, 0 ));
    }

    for(unsigned int i=1;i<(*finalCounters).size();i++)
    {
        cv::line(*graphSptr,cv::Point2d(1000-(i-1)*10,255-(*finalCounters)[i-1]),cv::Point2d((1000-i*10),255-(*finalCounters)[i]),cv::Scalar( 255, 0, 255 ));
    }

}

}
}
