/**
  *
  */
#ifndef DIRECTIONALFLOWEVALUATION_H
#define DIRECTIONALFLOWEVALUATION_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "../data/datatest.h"
#include <deque>


namespace ivs
{

namespace component
{

/**
 * @brief The MyComponent class
 */
class DirectionalFlowEvaluation : public BaseComponent{
public:
    IVS_DECL(DirectionalFlowEvaluation);
    DirectionalFlowEvaluation(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    /*data::Data<cv::Mat>  m_uflow;
    data::Data<cv::Mat>  m_flowTheta;*/
    data::Data<cv::Mat> m_uflow;
    data::Data<cv::Mat> graphOut;
    deque<double> values;
    double max;
    data::Data<int> m_compteur;
    int m_compteur1;
    int m_compteur2;
    deque<int> counters;
    deque<int> finalCounters;
    vector<int> framesWithPeople;
    int actualFrame;
    data::Data<int> m_sizeWindow;
    data::Data<int> m_sizeThreshold;
    data::Data<int> m_thresholdMinMoy;

};


double mod(double x, double N);
double computeValuesDirectionFlow(data::SPtr<cv::Mat> UFlowSptr);
void signalProcessing(std::deque<double> *values, std::deque<double> *valuesMA3, std::deque<double> *valuesDiff, double *moy, data::SPtr<int> thresholdMinMoy);
void counterMethodOne(std::deque<double> *valuesDiff, double *moy, data::SPtr<int> compteur, vector<int> *framesWithPeople, int *actualFrame, int position = 25);
void counterMethodTwo(std::deque<double> *valuesDiff, double *moy, data::SPtr<int> compteur, int *compteur1, int *compteur2, vector<int> *framesWithPeople, int *actualFrame, int position = 25);
void counterMethodThree(std::deque<double> *valuesDiff, double *moy, data::SPtr<int> compteur, std::deque<int> *counters, std::deque<int> *finalCounters, vector<int> *framesWithPeople, int *actualFrame, data::SPtr<int> seuil);
void drawingLine(data::SPtr<cv::Mat> graphSptr, std::deque<double> *valuesDiff, double *moy, double *max, std::deque<double> *valuesMA3, std::deque<int> *finalCounters);

}
}
#endif // DIRECTIONALFLOWEVALUATION_H
