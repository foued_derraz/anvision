#include "drawingComponent.h"
#include <iostream>
//#include "databuffer.h"
#include "../data/datatest.h"
#include "../factory/componentmaker.h"

//#include "../factory/componentmaker.h"




namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(drawingComponent);


drawingComponent::drawingComponent(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut", "Mat draw_frame",&m_frameOut)),
    m_drawList(initData("drawList","Object to draw",std::vector<std::string> (),&m_drawList))

{

}

void drawingComponent::init()
{

}

void drawingComponent::bwdInit()
{

}

void drawingComponent::doTask()
{
    data::SPtr<cv::Mat> frame=m_frameIn.getData();
    //do staff


    data::SPtr<std::vector<std::string>> recup=m_drawList.getData();


    for(int i=0;i<(*recup).size();i++)
    {

        std::vector<std::string> comptri= strtoshape((*recup)[i]);

        if(comptri[0]=="point")
        {
            cv::Point2d Point=cv::Point2d(std::stod(comptri[1]),stod(comptri[2]));
            int thickness = std::stoi(comptri[3]);
            helper::point(Point,thickness,Scalar(getColor(comptri[comptri.size()-1])[2],getColor(comptri[comptri.size()-1])[1],getColor(comptri[comptri.size()-1])[0])).draw(&(*frame));
        }


        if(comptri[0]=="line")
        {
            cv::Point2d Point1=cv::Point2d(std::stod(comptri[1]),stod(comptri[2]));
            cv::Point2d Point2=cv::Point2d(std::stod(comptri[3]),stod(comptri[4]));
            int thickness = std::stoi(comptri[5]);
            helper::line(Point1,Point2,thickness,Scalar(getColor(comptri[comptri.size()-1])[2],getColor(comptri[comptri.size()-1])[1],getColor(comptri[comptri.size()-1])[0])).draw(&(*frame));
        }


        if(comptri[0]=="rectangle")
        {
            cv::Point2d origin=cv::Point2d(std::stod(comptri[1]),stod(comptri[2]));
            cv::Point2d dimension=cv::Point2d(std::stod(comptri[3]),stod(comptri[4]));
            int thickness = std::stoi(comptri[5]);
            helper::rectangle(origin,dimension,thickness,Scalar(getColor(comptri[comptri.size()-1])[2],getColor(comptri[comptri.size()-1])[1],getColor(comptri[comptri.size()-1])[0])).draw(&(*frame));
        }



        if(comptri[0]=="circle")
        {
            cv::Point2d center=cv::Point2d(std::stod(comptri[1]),std::stod(comptri[2]));
            double radius= std::stod(comptri[3]);
            int thickness=std::stoi(comptri[4]);
            helper::circle(center,radius,thickness,Scalar(getColor(comptri[comptri.size()-1])[2],getColor(comptri[comptri.size()-1])[1],getColor(comptri[comptri.size()-1])[0])).draw(&(*frame));
        }




        if(comptri[0]=="polygon")
        {
            std::vector<cv::Point2d> listPoint;
            for (int i=1;i<comptri.size()-2;i=i+2)   listPoint.push_back(cv::Point2d(std::stod(comptri[i]),std::stod(comptri[i+1])));
            int thickness=std::stoi(comptri[comptri.size()-2]);
            helper::polygon(listPoint,thickness,Scalar(getColor(comptri[comptri.size()-1])[2],getColor(comptri[comptri.size()-1])[1],getColor(comptri[comptri.size()-1])[0])).draw(&(*frame));
        }

    }


    m_frameOut.setData(frame);

    //set Data


}



std::vector<std::string> drawingComponent::strtoshape(std::string composant)
{

    std::string vecChaine=composant;

    vecChaine = vecChaine.substr(vecChaine.find("[")+1);
    vecChaine = vecChaine.substr(0,vecChaine.find("]"));
    std::string leBuffer=vecChaine;

    int nbElements = 0;

    std::vector<std::string> comptri;

    for(int i=0; i<leBuffer.size(); i++)
    {
        if(leBuffer[i]==':')
        {
            comptri.push_back(leBuffer.substr(0,i));
            leBuffer=leBuffer.substr(i+1,leBuffer.size());
            nbElements++;
            i=0;
        }
    }

    comptri.push_back(leBuffer);

    return comptri;
}


std::vector<int> drawingComponent::getColor(std::string color)
{
    std::string vecChaine=color;

    vecChaine = vecChaine.substr(vecChaine.find("{")+1);
    vecChaine = vecChaine.substr(0,vecChaine.find("}"));
    std::string leBuffer=vecChaine;

    int nbElements = 0;

    std::vector<int> RGBcolor;

    for(int i=0; i<leBuffer.size(); i++)
    {
        if(leBuffer[i]==';')
        {
            RGBcolor.push_back(std::stoi(leBuffer.substr(0,i)));
            leBuffer=leBuffer.substr(i+1,leBuffer.size());
            nbElements++;
            i=0;
        }
    }

    RGBcolor.push_back(std::stoi(leBuffer));

    return RGBcolor;


}






}
}


