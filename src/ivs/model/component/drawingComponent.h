#ifndef DRAWINGCOMPONENT_H
#define DRAWINGCOMPONENT_H
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "basecomponent.h"
#include "../../helper/drawing.h"

namespace ivs
{

namespace component
{


class drawingComponent: public BaseComponent
{
public:
    IVS_DECL(drawingComponent);
    drawingComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
    std::vector<std::string> strtoshape(string composant);
    void draw(std::string composant);
    std::vector<int> getColor(string color);

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<std::vector<std::string> > m_drawList;

};

}
}

#endif // DRAWINGCOMPONENT_H
