#include "drawinggraph.h"


namespace ivs
{

namespace component
{

IVS_REGISTER(drawingGraph);

drawingGraph::drawingGraph(std::string str):BaseComponent(),
    m_newValue(initData("newValue","The new value enter",&m_newValue)),
    m_sizeWindow(initData("sizeWindow","Window'size",101,&m_sizeWindow)),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut))
{
    for(int i=0; i < (*m_sizeWindow.getData());i++)
        m_listValue.push_back(0.);

    max = 250;
}

void drawingGraph::init()
{
}

void drawingGraph::bwdInit()
{
}

void drawingGraph::doTask()
{

    //--------------get Data
    data::SPtr<double> newValue = m_newValue.getData();
    data::SPtr<int> sizeWindow = m_sizeWindow.getData();
    data::SPtr<cv::Mat> graphSptr;

    try
    {
        graphSptr = m_frameIn.getData(false);
    }
    catch ( ivs::Exception e)
    {
        graphSptr = data::SPtr<cv::Mat>(new cv::Mat(500,1000,CV_8UC3));
    }


    m_listValue.push_back(*newValue);
    m_listValue.pop_front();

    if((*newValue)>max)
        max =(*newValue);

    double coef = max/250;


    cv::line(*graphSptr,cv::Point2d(0,250),cv::Point2d(1000,250),cv::Scalar( 200, 0, 0 ));

    for(unsigned int i=1;i< (*sizeWindow);i++)
    {
        cv::line(*graphSptr,cv::Point2d(1000-(i-1)*10,250-m_listValue[i-1]/coef),cv::Point2d((1000-i*10),250-m_listValue[i]/coef),cv::Scalar( 0, 200, 0 ));
    }



    m_frameOut.setData(graphSptr);
}

}
}
