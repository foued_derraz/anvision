#ifndef DRAWINGGRAPH_H
#define DRAWINGGRAPH_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"


namespace ivs
{

namespace component
{

class drawingGraph : public BaseComponent
{
public:
    IVS_DECL(drawingGraph);
    drawingGraph(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<double> m_newValue;
    data::Data<int> m_sizeWindow;
    std::deque<double> m_listValue;
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;

    double max;
};

}
}

#endif // DRAWINGGRAPH_H
