#include "drawinggraph2.h"


namespace ivs
{

namespace component
{

IVS_REGISTER(drawingGraph2);

drawingGraph2::drawingGraph2(std::string str):BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_vecSumFlowIn(initData("vecSumFlowIn","Vector of Sum of Flow",&m_vecSumFlowIn))
{
    max = 50;
}

void drawingGraph2::init()
{
}

void drawingGraph2::bwdInit()
{
}

void drawingGraph2::doTask()
{

    //--------------get Data
    //data::SPtr<int> sizeWindow = m_sizeWindow.getData();
    data::SPtr<std::vector<double>> vecSumFlowIn = m_vecSumFlowIn.getData();
    data::SPtr<cv::Mat> graphSptr;

    try
    {
        graphSptr = m_frameIn.getData(false);
    }
    catch ( ivs::Exception e)
    {
        graphSptr = data::SPtr<cv::Mat>(new cv::Mat(250,(*vecSumFlowIn).size(),CV_8UC3));
    }


    for(int i=0;i<(*vecSumFlowIn).size();i++)
    {
        if((*vecSumFlowIn)[i]>max)
            max =(*vecSumFlowIn)[i];

    }
    //std::cout << "max  " << max << std::endl;

    double coef = max/250;


    cv::line(*graphSptr,cv::Point2d(0,250),cv::Point2d(1000,250),cv::Scalar( 200, 0, 0 ));

    for(unsigned int i=1;i< (*vecSumFlowIn).size();i++)
    {
        cv::line(*graphSptr,cv::Point2d(1000-(i-1)*10,250-(*vecSumFlowIn)[i-1]/coef),cv::Point2d((1000-i*10),250-(*vecSumFlowIn)[i]/coef),cv::Scalar( 0, 200, 0 ));
    }



    m_frameOut.setData(graphSptr);
}

}
}
