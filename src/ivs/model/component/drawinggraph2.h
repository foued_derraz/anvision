#ifndef DRAWINGGRAPH2_H
#define DRAWINGGRAPH2_H


#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"


namespace ivs
{

namespace component
{

class drawingGraph2 : public BaseComponent
{
public:
    IVS_DECL(drawingGraph2);
    drawingGraph2(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<vector<double>> m_vecSumFlowIn;

    double max;
};

}
}

#endif // DRAWINGGRAPH2_H
