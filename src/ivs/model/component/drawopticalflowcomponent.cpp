#include "drawopticalflowcomponent.h"
#include "../factory/componentmaker.h"

namespace ivs
{
namespace component
{

using namespace std;
using namespace cv;

IVS_REGISTER(drawOpticalFlowComponent);


drawOpticalFlowComponent::drawOpticalFlowComponent(std::string str):  BaseComponent(),
    m_frameIn(initData("frameIn","Frame de la camera",&m_frameIn)),
    m_frameOut(initData("frameOut", "mainFrame coverted in Gray",&m_frameOut)),
    m_uflow(initData("Uflow","exit data of optical flow",&m_uflow)),
    m_flowTheta(initData("FlowTheta","le theta",cv::Mat(),&m_flowTheta))
{
}

void drawOpticalFlowComponent::init()
{
}

void drawOpticalFlowComponent::bwdInit()
{
    max = 0;
}

void drawOpticalFlowComponent::doTask()
{
    data::SPtr<cv::Mat> frameInSptr = m_frameIn.getData();
    data::SPtr<cv::Mat> UFlowSptr = m_uflow.getData();
    //data::SPtr<cv::Mat> FlowThetaSptr(new cv::Mat());;
    data::SPtr<cv::Mat> bgr(new cv::Mat(frameInSptr->cols,frameInSptr->rows,CV_8UC3));//CV_32FC3 matrix


    cv::Mat xy[2];
    cv::split(*UFlowSptr,xy);

    cv::Mat magnitude, angle;
    cv::cartToPolar(xy[0], xy[1], magnitude, angle, true);

    //print("test");

    //translate magnitude to range [0;1]
    double mag_max,mag_min;

    cv::minMaxLoc(magnitude, &mag_min, &mag_max);


    //build hsv image
    cv::Mat _hsv[3], hsv;
    _hsv[0] = angle;
    _hsv[1] = cv::Mat::ones(angle.size(), CV_32F);
    _hsv[2] = magnitude;
    cv::merge(_hsv, 3, hsv);


    //convert to BGR and show

    cv::cvtColor(hsv, *bgr, cv::COLOR_HSV2BGR);

    bgr->convertTo(*bgr,CV_8UC3,255.0);

    m_frameOut.setData(bgr);

}

}
}

//std::cout << mag_max  << std::endl;

//log(to_string(max));
//magnitude.convertTo(magnitude, -1, 1/7.);
//double sum = 0;


/*for ( unsigned int i = 0 ; i < magnitude.rows ; i++)
{
    float* row_ptr_angle = angle.ptr<float>(i);
    float* row_ptr_magnitude = magnitude.ptr<float>(i);
    for(unsigned int j = 0; j < magnitude.cols; j++)
    {
        if(row_ptr_magnitude[j]< 0.5 )
        {
            row_ptr_magnitude[j] = 0.;
            row_ptr_angle [j]= 0.;

            //(row_ptr_angle[j])

        }
        if(!(row_ptr_angle[j]> 60 &&  row_ptr_angle[j]< 120 ))
        {
            row_ptr_magnitude[j] = 0.;
            row_ptr_angle [j]= 0.;
        }
        else
        {
            //sum +=  row_ptr_magnitude / exp( (row_ptr_angle[j] - 90)%360  + );

             //cout << row_ptr_angle[j] << endl;
        }

        //cout << row_ptr_magnitude[j] << endl;
    }
}*/
//std::cout << sum << std::endl;*/


//Rect roi = Rect(0, 140, 400, 20);
//Mat image_roi = (*bgr)(roi);
//cv::imshow("test",*bgr);


/*const float* row_ptr_theta = (*frameInSptr).ptr<float>(i);
    const float* row_ptr_Magnitude = gradMagnitude.ptr<float>(i);
    float* row_ptr_res = res.ptr<float>(i);

    for(unsigned int j = 0; j < baseWidth; j++)
    {
       float tmp = cos(row_ptr_theta[j])*cos(angles[a])+sin(row_ptr_theta[j])*sin(angles[a]);
       pow(tmp,this->denseSiftAlpha);
       row_ptr_res[j] = row_ptr_Magnitude[j] * tmp;
    }*/

/*int step=8;
    for(int y = 0; y < (*frameInSptr).rows; y += step)
    {
        for(int x = 0; x < (*frameInSptr).cols; x += step)
        {
            const Point2f& fxy = UFlowSptr->at<Point2f>(y, x);
            line(*frameInSptr, Point(x,y), Point(cvRound(x+fxy.x), cvRound(y+fxy.y)),
                 Scalar(0, 0, 255));
        }
    }*/

/*UFlowSptr->copyTo(*FlowThetaSptr);

    float moy_mod=0;
    float moy_teta=0;
    float moy_u=0;
    float moy_v=0;
    int moy_x=0;
    int moy_y=0;
    int cpt=0;
    for(int y = 0; y < (*FlowThetaSptr).rows; y++)
    {
        for(int x = 0; x < (*FlowThetaSptr).cols; x++)
        {
            const Point2f& fxy = (*FlowThetaSptr).at<Point2f>(y, x);
            float mod = sqrt(pow(fxy.x,2)+pow(fxy.y,2));

            if (mod > 5 )
            {
                float teta = acos(fxy.x/mod);
                moy_u += fxy.x; //moyenne sur u modif suite a la remarque de Foued
                moy_v += fxy.y; //moyenne sur v
                moy_teta += teta;
                moy_x +=x;
                moy_y +=y;
                moy_mod += mod;
                cpt++;
            }
        }
    }

    if(cpt >= 100)
    {
        moy_u = moy_u/cpt;
        moy_v = moy_v/cpt;
        moy_mod=moy_mod/cpt;

        moy_teta=moy_teta/cpt;
        moy_x=round(moy_x/cpt);
        moy_y=round(moy_y/cpt);
    }
    else
    {
        (moy_x,moy_y)=(0,0);
        (moy_mod,moy_teta)=(0.0,0.0);
        (moy_u,moy_v)=(0.0,0.0);
    }

    cv::line(*frameInSptr, Point(moy_x,moy_y), Point(cvRound(moy_x+moy_u*5), cvRound(moy_y+moy_v*5)), Scalar(255,0,0),1,LINE_8, 0);*/


//setData
