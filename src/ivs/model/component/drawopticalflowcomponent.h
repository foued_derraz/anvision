#ifndef DRAWOPTICALFLOWCOMPONENT_H
#define DRAWOPTICALFLOWCOMPONENT_H
#include "basecomponent.h"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/videoio/videoio.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "math.h"


namespace ivs
{
namespace component
{

class drawOpticalFlowComponent: public BaseComponent
{
public:
    IVS_DECL(drawOpticalFlowComponent);

    /**
     * @brief drawOpticalFlowComponent: constructor of the draw optical flow component
     * @param str: name in the json of the component draw optical flow
     */
    drawOpticalFlowComponent(std::string str);
    drawOpticalFlowComponent();
    void doTask() ;
    void init() ;
    void bwdInit() ;


private:
    double max;
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<cv::Mat> m_uflow;
    data::Data<cv::Mat>  m_flowTheta;

    data::SPtr<cv::Mat> prev_frame;


};

}
}

#endif // DRAWOPTICALFLOWCOMPONENT_H
