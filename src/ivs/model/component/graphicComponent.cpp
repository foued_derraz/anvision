#include "graphicComponent.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(GraphicComponent);

GraphicComponent::GraphicComponent(std::string str): BaseComponent(),
    m_myInt(initData("myInt","unsimple integer",42,&m_myInt))
{
}

void GraphicComponent::init()
{

} 
void GraphicComponent::bwdInit()
{

} 
void GraphicComponent::doTask()
{
}

}
} 
