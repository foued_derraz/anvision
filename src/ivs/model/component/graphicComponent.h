#ifndef GRAPHICCOMPONENT_H
#define GRAPHICCOMPONENT¨_H
 
#include "basecomponent.h"
#include "../factory/componentmaker.h"
#include "../data/data.h"

namespace ivs
{

namespace component
{
class GraphicComponent : public BaseComponent {
public:
    IVS_DECL(GraphicComponent);
    GraphicComponent(std::string);
	void doTask();
	void init();
	void bwdInit();
private:
    data::Data<int> m_myInt;

};

}
}
#endif //graphicComponent¨_H 
