#include "graycomponent.h"
#include "../factory/componentmaker.h"


namespace ivs
{
namespace component
{

using namespace cv;

IVS_REGISTER(GrayComponent);


GrayComponent::GrayComponent(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut", "Mat gray_frame",&m_frameOut))
{
}

void GrayComponent::init()
{

}

void GrayComponent::bwdInit()
{

}

void GrayComponent::doTask()
{



    //getData
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();

    //do staff
    cv::cvtColor(*frameSptr, *frameSptr, COLOR_BGR2GRAY);

    cv::equalizeHist(*frameSptr,*frameSptr);


    //set Data
    m_frameOut.setData(frameSptr);
}

}
}
