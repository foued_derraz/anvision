#ifndef GRAYCOMPONENT_H
#define GRAYCOMPONENT_H
#include "basecomponent.h"
#include "opencv2/opencv.hpp"
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "../../helper/exception.h"

namespace ivs
{
namespace component
{

class GrayComponent : public BaseComponent
{
public:

    IVS_DECL(GrayComponent);
    GrayComponent();
    GrayComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:

   data::Data<cv::Mat> m_frameIn;
   data::Data<cv::Mat> m_frameOut;

};


}
}

#endif // GRAYCOMPONENT_H
