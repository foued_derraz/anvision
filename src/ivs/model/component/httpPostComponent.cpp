#include "httpPostComponent.h"

namespace ivs
{
namespace component
{

IVS_REGISTER(httpPostComponent);

httpPostComponent::httpPostComponent(std::string str) :
				BaseComponent(),
				m_serverAdress(initData("m_serverAdress", "serveur", (std::string) "10.0.0.1", &m_serverAdress)),
				m_counting(initData("m_counting", "serveur", 2, &m_counting))

{
	curl = NULL;
}

void httpPostComponent::init()
{
	curl_global_init(CURL_GLOBAL_DEFAULT);
}

void httpPostComponent::bwdInit()
{
}

void httpPostComponent::doTask()
{
	//getData
	data::SPtr<string> serverAdress = m_serverAdress.getData();
	data::SPtr<int> counting = m_counting.getData();
	struct WriteThis pooh;

	cout << "post to" << *serverAdress << endl;
	bool isok = sendMessage(curl, curlRes, *serverAdress, 0, *counting);
	if(!isok)
	{
		cout << "server error" << endl;
	}

	sleep(1);

}

httpPostComponent::~httpPostComponent()
{
}

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{ //callback must have this declaration
//buf is a pointer to the data that curl has for us
//size*nmemb is the size of the buffer
	returndata.clear();
	for (int c = 0; c < size * nmemb; c++)
	{
		returndata.push_back(buf[c]);
	}
	cout << "server returns : " << returndata << endl;
	return size * nmemb; //tell curl how many bytes we handled
}

// send http post message wuth curl ---
bool httpPostComponent::sendMessage(CURL *curl, CURLcode &res, string serveurAdress, int outCount, int inCount)
{
	curl = curl_easy_init();
	if (curl)
	{

		/* First set the URL that is about to receive our POST. This URL can
		 just as well be a https:// URL if that is what should receive the
		 data. */
		char mes[300];
		sprintf(mes, "in=%d&out=%d", inCount, outCount);
		//   cout << mes << endl;

		curl_easy_setopt(curl, CURLOPT_URL, serveurAdress.c_str());
		/* Now specify the POST data */
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, mes);
		// set the call back to retrieve server response
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if (res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
			return false;
		}
		/* always cleanup */
		curl_easy_cleanup(curl);
		return true;
	}
	else
	{
		return false;
	}

}

}
}
