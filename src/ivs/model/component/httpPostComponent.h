/**
 *
 */
#ifndef HTTPPOSTCOMPONENT_H
#define HTTPPOSTCOMPONENT_H

#include "httpPostComponent.h"
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include <curl/curl.h>

namespace ivs
{

namespace component
{

struct WriteThis
{
	const char *readptr;
	long sizeleft;
};

string returndata;

/**
 * @brief The MyComponent class
 */
class httpPostComponent: public BaseComponent
{
public:
	IVS_DECL(httpPostComponent);
	httpPostComponent(std::string);
	~httpPostComponent();
	void doTask();
	void init();
	void bwdInit();
private:
	data::Data<string> m_serverAdress;
	data::Data<int> m_counting;
	CURL *curl;
	CURLcode curlRes;
	bool sendMessage(CURL *curl, CURLcode &res, string serveurAdress, int outCount, int inCount);
};

}
}
#endif // httpPostComponent
