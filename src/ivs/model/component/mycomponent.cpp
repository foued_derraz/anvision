#include "mycomponent.h"
#include "../factory/componentmaker.h"
#include "../data/databuffer.h"
#include <string>

namespace ivs
{
namespace component
{

IVS_REGISTER(MyComponent);

//static factory::ComponentMaker<MyComponent> maker("MyComponent");

MyComponent::MyComponent(std::string str): BaseComponent(),
    m_myInt(initData("myInt","un simple integer",42,&m_myInt)),
    m_myValue(initData("myValue","une simple value en integer",42,&m_myValue)),
    m_myString(initData("myString","une simple string",(std::string)"string",&m_myString)),
    m_dataTest(initData("testData", "testData",&m_dataTest))
{

}

void MyComponent::init()
{

}

void MyComponent::bwdInit()
{
    log("MyComponent : bwdInit" + this->getName() + " myInt: ");
}

void MyComponent::doTask()
{
    log("MyComponent " + this->getName() + ": DoTask");
    cpt++;
    data::SPtr<dataTest> ptr(new dataTest("test"+std::to_string(cpt)));
    m_dataTest.setData(ptr);
}

}
}
