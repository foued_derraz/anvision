/**
  *
  */
#ifndef MYCOMPONENT_H
#define MYCOMPONENT_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "../data/datatest.h"

namespace ivs
{

namespace component
{

/**
 * @brief The MyComponent class
 */
class MyComponent : public BaseComponent{
public:
    IVS_DECL(MyComponent);
    MyComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<int> m_myInt;
    data::Data<int> m_myValue;
    data::Data<std::string> m_myString;
    data::Data<dataTest> m_dataTest;
    int cpt=0;


    //data::Data<MaStruct> m_mastruct;
};

}
}
#endif // MYCOMPONENT_H
