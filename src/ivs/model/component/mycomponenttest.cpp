#include "mycomponenttest.h"
#include "../factory/componentmaker.h"
#include <boost/algorithm/string.hpp>

namespace ivs
{
namespace component
{

IVS_REGISTER(MyComponentTest);

MyComponentTest::MyComponentTest(std::string str):BaseComponent(),
    m_dataTestIn(initData("dataIn","data test in",&m_dataTestIn)),
    m_dataTestOut(initData("dataOut","data test out",&m_dataTestOut)),
    m_monPoint(initData("point","un point",cv::Point2f(4.2,5.8),&m_monPoint))
{
}


void MyComponentTest::init()
{

}

void MyComponentTest::bwdInit()
{

}

void MyComponentTest::doTask()
{
    /*it++;

    if(m_dataTestIn.IsChanged())
    {
        data::SPtr<dataTest> dataIn = m_dataTestIn.getData();
        log("MyComponent "+this->getName() +": Do destruction with :"+ dataIn->getStr()  + " test" + std::to_string(dataIn.getCreationTime())  );
    }
    else
    {
        data::SPtr<dataTest> dataIn(new dataTest("start+"+std::to_string(it)));
        boost::this_thread::sleep_for(boost::chrono::milliseconds(20));

        log("MyComponent "+this->getName() +": Do creation with :"+ dataIn->getStr() + " " + std::to_string(dataIn.getCreationTime()) );
        m_dataTestOut.setData(dataIn);
    }
    */
    std::cout<< *m_monPoint.getData(false,false) << std::endl;
}

}
}
