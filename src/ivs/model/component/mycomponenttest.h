#ifndef MYCOMPONENTTEST_H
#define MYCOMPONENTTEST_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include <stdlib.h>

namespace ivs
{

namespace component
{

/**
 * @brief The MyComponentTest class
 */
class MyComponentTest : public BaseComponent{
public:
    IVS_DECL(MyComponentTest);
    MyComponentTest(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<dataTest> m_dataTestIn;
    data::Data<dataTest> m_dataTestOut;
    int it=0;
    data::Data<cv::Point2f> m_monPoint;
    /*data::Data<int> m_myValue;
    data::Data<std::string> m_myString;
    data::Data<std::vector<double>> m_myVec;
    data::Data<cv::Mat> m_myMatrix;
    data::Data<dataTest> m_dataTest;
*/
    //data::SPtr<dataTest> _dataTest;


    //data::Data<MaStruct> m_mastruct;
};

}
}

#endif // MYCOMPONENTTEST_H
