#include "opticalflowcomponent.h"
#include "../factory/componentmaker.h"

namespace ivs
{
namespace component


{

using namespace std;
using namespace cv;



IVS_REGISTER(opticalFlowComponent);


opticalFlowComponent::opticalFlowComponent(std::string str):  BaseComponent(),
    m_frameIn(initData("frameIn","Frame de la camera",&m_frameIn)),
    m_frameOut(initData("frameOut", "mainFrame coverted in Gray",&m_frameOut)),
    m_uflow(initData("uflow","exit data of optical flow",&m_uflow)),
    m_nbFrame(initData("nbFrame","nb Frame used",false,&m_nbFrame)),
    m_pyr_scale(initData("pyr_scale","pyr_scale of optical flow", 0.5,&m_pyr_scale)),
    m_levels(initData("levels","levels of optical flow",3,&m_levels)),
    m_winsize(initData("winsize","winsize of optical flow",15,&m_winsize)),
    m_iterations(initData("iterations","iterations of optical flow",3,&m_iterations)),
    m_poly_n(initData("poly_n","poly_n of optical flow",5,&m_poly_n)),
    m_poly_sigma(initData("poly_sigma","poly_sigma of optical flow",1.2,&m_poly_sigma)),
    m_flags(initData("flags","flags of optical flow",0,&m_flags))
{
}

void opticalFlowComponent::init()
{

}

void opticalFlowComponent::bwdInit()
{

}

void opticalFlowComponent::doTask()
{
    //getData and create Data
    data::SPtr<cv::Mat> frameInSptr = m_frameIn.getData();
    data::SPtr<bool> nbFrameSptr = m_nbFrame.getData();
    data::SPtr<cv::Mat> UFlowSptr(new cv::Mat());
    data::SPtr<double> pyrScaleSptr=m_pyr_scale.getData();
    data::SPtr<int> levelsSptr=m_levels.getData();
    data::SPtr<int> winsizeSptr=m_winsize.getData();
    data::SPtr<int> iterationsSptr=m_iterations.getData();
    data::SPtr<int>poly_nSptr=m_poly_n.getData();
    data::SPtr<double>poly_sigmaSptr=m_poly_sigma.getData();
    data::SPtr<int>flagsSptr=m_flags.getData();


    //do staff

    //apply Optical flow
    if (*nbFrameSptr==true)
    {
        //calcul le vecteur de mouvement de chaque pixel
        cv::calcOpticalFlowFarneback(m_prevGray,*frameInSptr,*UFlowSptr, *pyrScaleSptr, *levelsSptr, *winsizeSptr, *iterationsSptr, *poly_nSptr, *poly_sigmaSptr, *flagsSptr);

        //cv::imshow("test2",m_prevGray);
        //cv::imshow("test3",*frameInSptr);
        //setData
        m_prevGray = frameInSptr->clone();

        m_frameOut.setData(frameInSptr);
        m_uflow.setData(UFlowSptr);

    }
    else
    {
        //std::cout << "test" << endl;
        m_prevGray = frameInSptr->clone();

        *UFlowSptr = cv::Mat::zeros((*frameInSptr).size(),CV_32FC2);
        m_frameOut.setData(frameInSptr);
        m_uflow.setData(UFlowSptr);
        *nbFrameSptr=true;
    }






}


}
}


