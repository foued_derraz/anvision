#ifndef OPTICALFLOWCOMPONENT_H
#define OPTICALFLOWCOMPONENT_H
#include "basecomponent.h"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/videoio/videoio.hpp"
#include "opencv2/highgui/highgui.hpp"




namespace ivs
{
namespace component
{


class opticalFlowComponent : public BaseComponent
{
public:
    IVS_DECL(opticalFlowComponent);
    /**
     * @brief opticalFlowComponent: the constructor of optical flow component
     * @param str: the name of the component in the json
     */
    opticalFlowComponent(std::string str);
    void doTask() ;
    void init() ;
    void bwdInit() ;


private:

    /**
     * @brief m_frameIn: the frame in of the optical flow component
     */
    data::Data<cv::Mat> m_frameIn;
    /**
     * @brief m_frameOut: the frame out of the optical flow component
     */
    data::Data<cv::Mat> m_frameOut;
    /**
     * @brief m_prevGray: the previous gray frame
     */
    cv::Mat m_prevGray;
    /**
     * @brief m_uflow: the matrix of the coefficient of the optical flow component
     */
    data::Data<cv::Mat> m_uflow;
    /**
     * @brief m_nbFrame: the boolean used to test the existence of two frame to compute the optical flow
     */
    data::Data<bool> m_nbFrame;
    /**
     * @brief m_pyr_scale: the scale used to compute the optical flow
     */
    data::Data<double>m_pyr_scale;
    /**
     * @brief m_levels: number of levels used to compute the optical flow
     */
    data::Data<int>m_levels;
    /**
     * @brief m_winsize: the size of the window used to compute the optical flow
     */
    data::Data<int>m_winsize;
    /**
     * @brief m_iterations: number of iteration used to compute the optical flow
     */
    data::Data<int>m_iterations;
    /**
     * @brief m_poly_n
     */
    data::Data<int>m_poly_n;
    /**
     * @brief m_poly_sigma: the variance used to compute the optical flow
     */
    data::Data<double>m_poly_sigma;
    /**
     * @brief m_flags
     */
    data::Data<int>m_flags;

};

}

}

#endif // OPTICALFLOWCOMPONENT_H
