#include "printcamera.h"



namespace ivs
{
namespace component
{
using namespace cv;

IVS_REGISTER(PrintCamera);

PrintCamera::PrintCamera(std::string str):BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_displayInterface(initData("frameIn","Mat main_frame",false, &m_displayInterface))
{
}

void PrintCamera::init()
{

}

void PrintCamera::bwdInit()
{
    view::SBaseView::Instance()->addDisplay(this->getName());
}

void PrintCamera::doTask()
{

    //getData
    data::SPtr<cv::Mat> frameInSptr = m_frameIn.getData();


    helper::AdvancedTimer atimer = helper::AdvancedTimer::Instance();

    helper::ctime_t stime = frameInSptr.getCreationTime();
    helper::ctime_t etime = atimer.getFastTime();

    double time = atimer.convertTimeInMS(etime -stime);

    log("time : "+std::to_string(time));


    //  cv::imshow("video",*frameInSptr);

        //  cv::imshow("video",*frameInSptr);


    view::SBaseView::Instance()->showDisplay(this->getName(),frameInSptr);
    //cv::imshow("video"+this->getName(),*frameInSptr);

    //waitKey(1);
}


}
}

