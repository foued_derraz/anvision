#ifndef PRINTCAMERA_H
#define PRINTCAMERA_H

#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "basecomponent.h"
#include "../view/baseView.h"

namespace ivs
{

namespace component
{


class PrintCamera : public BaseComponent
{
public:
    IVS_DECL(PrintCamera);
    PrintCamera();
    PrintCamera(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<bool> m_displayInterface;
    //data::Data<cv::Mat> m_gray_frame;

};


}
}
#endif // PRINTCAMERA_H
