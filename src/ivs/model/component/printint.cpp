#include "printint.h"
#include "../factory/componentmaker.h"


namespace ivs
{
namespace component
{

IVS_REGISTER(PrintInt);

PrintInt::PrintInt(std::string str):
    BaseComponent(),
    m_int(initData("int","int to print",42,&m_int))
{
}

void PrintInt::init()
{
    log("PrintInt : init " + this->getName());
}

void PrintInt::bwdInit()
{
    log("PrintInt : bwdInit" + this->getName() + " myInt: ");
}

void PrintInt::doTask()
{

    log("PrintInt : " + std::to_string(*m_int.getData()));
}

}
}
