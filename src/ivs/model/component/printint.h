#ifndef PRINTINT_H
#define PRINTINT_H

#include "basecomponent.h"

namespace ivs
{
namespace component
{


class PrintInt : public BaseComponent
{
public:

    IVS_DECL(PrintInt);
    PrintInt(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;
private:
    data::Data<int> m_int;
    PrintInt();
};


}
}
#endif // PRINTINT_H
