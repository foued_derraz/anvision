#include "recordvideocomponent.h"
//#include "../factory/componentmaker.h"


namespace ivs
{
namespace component
{

IVS_REGISTER(RecordVideoComponent);

RecordVideoComponent::RecordVideoComponent(std::string): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_fileName(initData("fileName","the video's filename",(std::string)"videoTest.avi",&m_fileName)),
    m_fps(initData("fps","the fps of my video",30.0,&m_fps)),
    m_Width(initData("Width","the width of video",480,&m_Width)),
    m_isColor(initData("isColor","the video is in color or not?",true,&m_isColor)),
    m_Height(initData("Height","the height of video",640,&m_Height)),
    m_CV_FOURCC(initData("fourCC","CV_FOURCC of frame",std::vector<char>(), &m_CV_FOURCC))
{
}


void RecordVideoComponent::init()
{

}

void RecordVideoComponent::bwdInit()
{
    data::SPtr<std::string> fileNameSptr = m_fileName.getData();
    data::SPtr<double> fpsSptr = m_fps.getData();
    data::SPtr<int> WidthSptr = m_Width.getData();
    data::SPtr<int> HeightSptr = m_Height.getData();
    data::SPtr<bool> isColorSptr = m_isColor.getData();

    m_video.open(*fileNameSptr, CV_FOURCC((*m_CV_FOURCC.getData())[0],(*m_CV_FOURCC.getData())[1],(*m_CV_FOURCC.getData())[2],(*m_CV_FOURCC.getData())[3]), *fpsSptr, cv::Size(*WidthSptr,*HeightSptr), *isColorSptr);
}

void RecordVideoComponent::doTask()
{
    //getData and create Data
    data::SPtr<cv::Mat> frameInSptr = m_frameIn.getData();

    //do staff
    if(m_video.isOpened())
    {
        m_video.write(*frameInSptr);
    }
    else
        throw ivs::Exception("the video doesn't open");

    //set Data
    m_frameOut.setData(frameInSptr);
}


}
}
