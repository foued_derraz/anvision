#ifndef RECORDVIDEOCOMPONENT_H
#define RECORDVIDEOCOMPONENT_H

#include "basecomponent.h"
#include <iostream>
#include "../factory/componentmaker.h"
#include "../data/data.h"

namespace ivs
{

namespace component
{

class RecordVideoComponent : public BaseComponent
{
public:
    IVS_DECL(RecordVideoComponent);
    RecordVideoComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<std::string> m_fileName;
    data::Data<bool> m_isColor;
    data::Data<double> m_fps;
    data::Data<int> m_Height;
    data::Data<int> m_Width;
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<std::vector<char>> m_CV_FOURCC;
    cv::VideoWriter m_video;
    cv::Size m_tailleImg;

    //data::Data<std::string>m_CV_FOURCC;
};

}
}

#endif // RECORDVIDEOCOMPONENT_H
