#include "resizecomponent.h"


#include "../factory/componentmaker.h"


namespace ivs
{
namespace component
{
using namespace cv;


IVS_REGISTER(ResizeComponent);


ResizeComponent::ResizeComponent(std::string str): BaseComponent(),
    m_frameIn(initData("frameIn","Mat main_frame", &m_frameIn)),
    m_frameOut(initData("frameOut","Mat main_frame", &m_frameOut)),
    m_Height(initData("Height","Height of the frame",800,&m_Height)),
    m_Width(initData("Width","Width of the frame",600,&m_Width))
{
}

void ResizeComponent::init()
{

}

void ResizeComponent::bwdInit()
{

}

void ResizeComponent::doTask()
{
    data::SPtr<cv::Mat> frameSptr = m_frameIn.getData();
    data::SPtr<int> height = m_Height.getData();
    data::SPtr<int> width = m_Width.getData();

    //do staff
    cv::resize(*frameSptr,*frameSptr,cv::Size (*width,*height));

    //set Data
    m_frameOut.setData(frameSptr);

}

}
}
