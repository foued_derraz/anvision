#ifndef RESIZECOMPONENT_H
#define RESIZECOMPONENT_H
#include "../factory/componentmaker.h"
#include "../data/data.h"
#include "basecomponent.h"
namespace ivs
{

namespace component
{


class ResizeComponent: public BaseComponent
{
public:
    IVS_DECL(ResizeComponent);
    ResizeComponent();
    ResizeComponent(std::string);
    void doTask() ;
    void init() ;
    void bwdInit() ;

private:
    data::Data<cv::Mat> m_frameIn;
    data::Data<cv::Mat> m_frameOut;
    data::Data<int> m_Height;
    data::Data<int> m_Width;

};

}
}

#endif // RESIZECOMPONENT_H
