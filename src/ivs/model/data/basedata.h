#ifndef BASEDATA_H
#define BASEDATA_H

//#include "../component/baseobject.h"
#include  <iostream>
namespace ivs
{
namespace data {

class BaseData
{
public:
    const char* getName(){return this->name;}
    virtual void setData(std::string) = 0;
    //template<class T>
    virtual void* getDataVoid() = 0;
    virtual void initialyse(int bufferType,int bufferSize)=0;
    virtual void initData(void* defautlValue,void* owner) = 0;
    inline void setIsChanged(bool b){ this->isChanged = b;}
    inline bool IsChanged(){ return this->isChanged;}

protected:
    const char* name;
    const char* ownerClass;
    const char* description;
    bool isChanged = false;

    //component::BaseComponent* owner;
};

}
}

#endif // BASEDATA_H
