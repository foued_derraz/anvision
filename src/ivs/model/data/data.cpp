#include "data.h"

namespace ivs
{
namespace data
{

template <>
void Data<int>::setData(std::string valueDescription)
{
    SPtr<int> ptr(new int(std::stoi(valueDescription.c_str())));
    this->bufferData->setData(ptr,false,true);
}

template <>
void Data<std::string>::setData(std::string valueDescription)
{
    SPtr<std::string> ptr(new std::string(valueDescription));
    this->bufferData->setData(ptr,false,true);
}

template <>
void Data<double>::setData(std::string valueDescription)
{
    SPtr<double> ptr(new double(std::stod(valueDescription.c_str())));
    this->bufferData->setData(ptr,false,true);
}

template <>
void Data<bool>::setData(std::string valueDescription)
{
    SPtr<bool> ptr(new bool(helper::strToBool(valueDescription)));
    this->bufferData->setData(ptr,false,true);
}


template <>
void Data<float>::setData(std::string valueDescription)
{
    SPtr<float> ptr(new float(std::stof(valueDescription.c_str())));
    this->bufferData->setData(ptr,false,true);
}


template <>
void Data<long int>::setData(std::string valueDescription)
{
    SPtr<long int> ptr(new long int(std::stol(valueDescription.c_str())));
    this->bufferData->setData(ptr,false,true);
}

template<>
void Data<dataTest>::setData(std::string valueDescription)
{
}

template<>
void Data<cv::Mat>::setData(std::string valueDescription)
{
    std::string matChaine= valueDescription.c_str();
    matChaine = matChaine.substr(matChaine.find("(")+1);
    std::string typeMat = matChaine.substr(0, matChaine.find(")"));
    matChaine = valueDescription.substr(valueDescription.find(")")+1);

    SPtr<cv::Mat> laMatPtr(new cv::Mat()) ;
    if(typeMat=="int")
    {
        helper::strToMat<int>(matChaine, *laMatPtr);
    }
    else if(typeMat=="double")
    {
        helper::strToMat<double>(matChaine, *laMatPtr);
    }
    else if(typeMat=="float")
    {
        helper::strToMat<float>(matChaine, *laMatPtr);
    }

    this->bufferData->setData(laMatPtr,false,true);
}

template<>
void Data<std::vector<int> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}

template<>
void Data<std::vector<double> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}

template<>
void Data<std::vector<float> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}

template<>
void Data<std::vector<std::string> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}

template<>
void Data<std::vector<char> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}


template<>
void Data<std::vector<cv::Point2d> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}

template<>
void Data<std::vector<cv::Point2i> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}

template<>
void Data<std::vector<cv::Point2f> >::setData(std::string valueDescription)
{
    data::setData(this,valueDescription);
}


template<>
void Data<cv::Point2i>::setData(std::string valueDescription)
{
    std::string maChaine= valueDescription.c_str();

    cv::Point2i* point = new cv::Point2i();
    *point = helper::stoType<cv::Point2i> (maChaine);
    SPtr<cv::Point2i> ptr (point);
    this->bufferData->setData(ptr,false,true);
}

template<>
void Data<cv::Point2d>::setData(std::string valueDescription)
{
    std::cout << "valueDesc : " << valueDescription << std::endl;
    std::string maChaine= valueDescription.c_str();

    cv::Point2d* point = new cv::Point2d();
    *point = helper::stoType<cv::Point2d> (maChaine);
    SPtr<cv::Point2d> ptr (point);
    this->bufferData->setData(ptr,false,true);
}

template<>
void Data<cv::Point2f>::setData(std::string valueDescription)
{
    std::string maChaine= valueDescription.c_str();

    cv::Point2f* point = new cv::Point2f();
    *point = helper::stoType<cv::Point2f> (maChaine);
    SPtr<cv::Point2f> ptr (point);
    this->bufferData->setData(ptr,false,true);
}

/*template <>
void Data<int>::initData(char* defautlValue,component::BaseObject* owner)
{
    this->data = (int*)defautlValue;
}

template <>
void Data<std::string>::initData(char* defautlValue,component::BaseObject* owner)
{
    this->data = (std::string*)defautlValue;
}

template <>
void Data<bool>::initData(char* defautlValue,component::BaseObject* owner)
{
    this->data = (bool*)defautlValue;
}*/


/*template <>
void Data<std::string>::initData(char* defaultValue, graph::BaseGraph *ownerG)
{
    this->data = (std::string*) defaultValue;
}*/

/*template <class T>
void Data<T>::initData(char* name, char* description, component::BaseComponent* owner)
{
    this->name = name;
    this->description = description;
    this->data = new T();
    this->owner = owner;
}

template <class T>
void Data<T>::initData(char* name, T defaultValue, char* description, component::BaseComponent* owner)
{
    this->name = name;
    this->description = description;
    this->data = &defaultValue;
    this->owner = owner;
}

template  <>
void Data<int>::initData(char* name, int defaultValue, char* description, component::BaseComponent* owner)
{
    this->name = name;
    this->description = description;
    *this->data = defaultValue;
    this->owner = owner;
}*/

}
}
