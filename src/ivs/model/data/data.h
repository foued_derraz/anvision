#ifndef DATA_H
#define DATA_H

#include "basedata.h"
#include "../component/baseobject.h"
#include "../../helper/stringprocess.h"
#include "databuffer.h"
#include "../data/datatest.h"

#include <stdlib.h>
#include <string>
#include <iostream>



namespace ivs
{
namespace data
{


template <typename T>
class Data : public BaseData
{
public:

    Data(int bufferType,int bufferSize) { initialyse(bufferType,bufferSize); }
    Data(BaseDataBuffer<T>* buffer) {
        this->bufferData = buffer;
    }

    void initialyse(int bufferType,int bufferSize)
    {
        this->bufferSize = bufferSize;
        switch (bufferType) {
        case FIFO_BUFFER:
            this->bufferType = FIFO_BUFFER;
            bufferData = new DataBuffer<T,FIFO_BUFFER>(bufferSize);
            break;
        case LIFO_BUFFER:
            this->bufferType = LIFO_BUFFER;
            bufferData = new DataBuffer<T,LIFO_BUFFER>(bufferSize);
            break;
        case CONSTANT_BUFFER:
            this->bufferType = CONSTANT_BUFFER;
            bufferData = new DataBuffer<T,CONSTANT_BUFFER>(bufferSize);
            break;
        default:
            break;
        }
        //bufferData = new DataBuffer<T,bufferType>(bufferSize);
        //bufferData->setData(SPtr<T>(new T()),false,true);
    }

    void initData(const char* name,const char* description, component::BaseObject* owner)
    {
        this->name = name;
        this->description = description;
        //this->bufferData = (BaseDataBuffer<T>*) (new DataBuffer<T,FIFO_BUFFER>(1));
        this->owner = owner;
    }

    void initData(const char* name, T defaultValue, const char* description, component::BaseObject* owner)
    {
        this->name = name;
        this->description = description;
        this->bufferData->setData( SPtr<T>(new T(defaultValue)),false,true);
        this->owner = owner;
    }
    void initData(const char* name, SPtr<T> defaultValue, const char* description, component::BaseObject* owner)
    {
        this->name = name;
        this->description = description;
        this->bufferData->setData(defaultValue,false,true);
        this->owner = owner;
    }

    void initData(void* defaultValue,void * owner)
    {
        this->initData(defaultValue,(component::BaseObject*)owner);
    }



    void initData(void* defautlValue,component::BaseObject* owner)
    {
        //SPtr<T> ptr((T*)defautlValue);
        this->bufferData = (BaseDataBuffer<T>*) defautlValue;
    }

    //void initData(char* defaultValue, graph::BaseGraph *ownerG);

    void setData(std::string valueDescription){}

    void* getDataVoid() {return (void*)bufferData;}
    SPtr<T> getData(bool isBlockingStrat=true,bool isInit = false) {return bufferData->getData(isBlockingStrat,isInit);}
    void setData(SPtr<T> TData,bool isBlockingStrat=true,bool isInit = false) { bufferData->setData(TData,isBlockingStrat,isInit);}
    inline int getBufferType() {return bufferType;}
    inline int getBufferSize() {return bufferSize;}
    component::BaseObject *owner;


    //graph::BaseGraph* ownerG;
private:
    BaseDataBuffer<T>* bufferData;
    int bufferType;
    int bufferSize;

};


template< typename vecType >
void setData(Data<std::vector<vecType>>* data,std::string valueDescription)
{
    std::string vecChaine(valueDescription.c_str());
    vecChaine = vecChaine.substr(vecChaine.find("(")+1);
    vecChaine = vecChaine.substr(0,vecChaine.find(")"));
    std::string leBuffer=vecChaine;

    int nbElements = 0;

    for(int i=0; i<vecChaine.size(); i++)
    {
        if(leBuffer.substr(0,1)==",")
            nbElements++;
        leBuffer=leBuffer.substr(1,leBuffer.find(")"));
    }

    nbElements++;

    SPtr< std::vector<vecType> > ptr (new std::vector<vecType>(nbElements));

    helper::strToVector<vecType>(vecChaine,*ptr,nbElements);
    data->setData(ptr,false,true);
}





}
}



#endif // DATA_H
