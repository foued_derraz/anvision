#ifndef DATABUFFER_H
#define DATABUFFER_H

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_mutex.hpp>
#include "../../helper/exception.h"
#include "sptr.h"

namespace ivs
{
namespace data
{

#define SLEEP_TIME_MICROSECONDS 10


/*
template<typename T>
using SPtr = boost::shared_ptr<T>;
*/

enum bufferType {FIFO_BUFFER = 0 , LIFO_BUFFER = 1, CONSTANT_BUFFER = 2 } ;

template <typename T>
class BaseDataBuffer
{
public:
    virtual SPtr<T> getData(bool blockingStrat = true,bool isInit =false) = 0;
    virtual void setData(SPtr<T> sptr,bool blockingStrat = true,bool isInit = false) = 0;
};

template <typename T,int typeBuffer>
class DataBuffer;

/**
 *  FIFO Buffer (First In, First out)
 *  example:
 *  1) insert A  |   2) insert B  |   3) insert C   |  4) insert D  |   5) get  |   4*)       |  6) get
 *      _        |       _        |       C         |       wait    |       _   |       D     |     _
 *      _        |       B        |       B         |       until   |       C   |       C     |     D
 *      A        |       A        |       A         |       Space*  |       B   |       B     |     C
 *
 */
template <typename T>
class DataBuffer<T,FIFO_BUFFER>  : public BaseDataBuffer<T>
{
public:
    DataBuffer(int bufferSize)
    {
        this->bufferSize = bufferSize;
        this->buffer = new SPtr<T>[bufferSize];
        for(int i =0;i<this->bufferSize;i++)
        {
            this->buffer[i] = NULL;
        }
    }

    /**
     * @brief getData on buffer
     * @param blockingStrat: if true block execution until there are a data available, else throw an Exeption if it's not possible
     * @return data available (depends on strats of the buffer)
     */
    SPtr<T> getData(bool blockingStrat = true,bool isInit = false)
    {
        //object boost use for lock exclusive mutex on constructor and free him in destructor on the end of this function
        boost::unique_lock<boost::shared_mutex> lock(this->_access);
        SPtr<T> firstData = this->buffer[this->readIndex];
        lock.unlock();
        if(firstData == NULL)
        {

            if(blockingStrat)
            {
                while(firstData == NULL)
                {
                    boost::this_thread::sleep_for(boost::chrono::microseconds(SLEEP_TIME_MICROSECONDS));
                    lock.lock();
                    firstData = this->buffer[this->readIndex];
                    lock.unlock();
                }
            }
            else
            {
                throw ivs::Exception("Data is not available");
            }
        }
        lock.lock();
        this->buffer[this->readIndex] = NULL;

        this->readIndex = (this->readIndex+1)%this->bufferSize;
        return firstData;
    }

    /**
     * @brief setData on buffer
     * @param sptr blockingStrat: if true block execution until there are a data available, else throw an Exeption if it's not possible (not important for FIFO buffer)
     * @param blockingStrat
     */
    void setData(SPtr<T> sptr,bool blockingStrat = true,bool isInit = false)
    {
        if(isInit)
        {
            this->buffer[this->writeIndex] = sptr;
            this->writeIndex = (this->writeIndex+1)%this->bufferSize;
        }
        else
        {
            //object boost use for lock exclusive mutex on constructor and free him in destructor on the end of this function
            boost::unique_lock<boost::shared_mutex> lock(this->_access);
            if(this->buffer[this->writeIndex] == NULL)
            {
                this->buffer[this->writeIndex] = sptr;
                this->writeIndex = (this->writeIndex+1)%this->bufferSize;
            }
            else
            {
                lock.unlock();
                //if data space is not available, sleep execution and wait to write
                while(this->buffer[this->writeIndex] != NULL)
                {
                    boost::this_thread::sleep_for(boost::chrono::microseconds(SLEEP_TIME_MICROSECONDS));
                }
                lock.lock();
                this->buffer[this->writeIndex] = sptr;
                this->writeIndex = (this->writeIndex+1)%this->bufferSize;
                lock.unlock();
            }
        }
    }
private:
    int readIndex = 0;
    int writeIndex = 0;
    int bufferSize;
    SPtr<T>* buffer;
    boost::shared_mutex _access;


};


template <typename T>
class DataBuffer<T,LIFO_BUFFER>  : public BaseDataBuffer<T>
{
public:

    DataBuffer(int bufferSize)
    {
        this->bufferSize = bufferSize;
        this->buffer = new SPtr<T>[bufferSize];
        for(int i =0;i<this->bufferSize;i++)
        {
            this->buffer[i] = NULL;
        }
    }
    /**
     * @brief getData on buffer
     * @param blockingStrat: if true block execution until there are a data available, else throw an Exeption if it's not possible
     * @return data available (depends on strats of the buffer)
     */
    SPtr<T> getData(bool blockingStrat = true,bool isInit = false)
    {
        //object boost use for lock exclusive mutex on constructor and free him in destructor on the end of this function

        boost::unique_lock<boost::shared_mutex> lock(this->_access);
        SPtr<T> firstData = this->buffer[this->readIndex];
        lock.unlock();

        if(firstData == NULL)
        {
            if(blockingStrat)
            {
                while(firstData == NULL)
                {
                    boost::this_thread::sleep_for(boost::chrono::microseconds(SLEEP_TIME_MICROSECONDS));
                    lock.lock();
                    firstData = this->buffer[this->readIndex];
                    lock.unlock();
                }
            }
            else
            {
                throw ivs::Exception("Data is not available");
            }
        }

        lock.lock();
        this->buffer[this->readIndex] = NULL;
        this->readIndex = (this->readIndex+1)%this->bufferSize;
        return firstData;

    }

    /**
     * @brief setData on buffer
     * @param sptr blockingStrat: if true block execution until there are a data available, else throw an Exeption if it's not possible (not important for FIFO buffer)
     * @param blockingStrat
     */
    void setData(SPtr<T> sptr,bool blockingStrat = true,bool isInit = false)
    {
        if(isInit)
        {
            this->buffer[this->writeIndex] = sptr;
            this->writeIndex = (this->writeIndex+1)%this->bufferSize;
        }
        else
        {
            //object boost use for lock exclusive mutex on constructor and free him in destructor on the end of this function
            boost::unique_lock<boost::shared_mutex> lock(this->_access);

            this->buffer[this->writeIndex] = sptr;
            this->readIndex = this->writeIndex;
            this->writeIndex = (this->writeIndex+1)%this->bufferSize;

        }

    }

private:
    int readIndex = 0;
    int writeIndex = 0;
    int bufferSize;
    SPtr<T>* buffer;
    boost::shared_mutex _access;

};

template <typename T>
class DataBuffer<T,CONSTANT_BUFFER>  : public BaseDataBuffer<T>
{
public:

    DataBuffer(int bufferSize)
    {
        this->bufferSize = bufferSize;
        this->buffer = new SPtr<T>[bufferSize];
        for(int i =0;i<this->bufferSize;i++)
        {
            this->buffer[i] = NULL;
        }
    }

    /**
     * @brief getData on buffer
     * @param blockingStrat: if true block execution until there are a data available, else throw an Exeption if it's not possible
     * @return data available (depends on strats of the buffer)
     */
    SPtr<T> getData(bool blockingStrat = true,bool isInit = false)
    {
        if(isInit)
        {
            return this->buffer[this->readIndex];
        }
        else
        {
            //object boost use for lock shared mutex on constructor and free him in destructor on the end of this function
            boost::shared_lock<boost::shared_mutex> lock(this->_access);
            SPtr<T> firstData = this->buffer[this->readIndex];
            if(firstData == NULL)
            {
                this->_access.unlock();
                if(blockingStrat)
                {
                    while(firstData == NULL)
                    {
                        boost::this_thread::sleep_for(boost::chrono::microseconds(SLEEP_TIME_MICROSECONDS));
                        firstData = this->buffer[this->readIndex];
                    }
                }
                else
                {
                    throw ivs::Exception("Data is not available");
                }
            }
            return firstData;
        }

    }

    /**
     * @brief setData on buffer
     * @param sptr blockingStrat: if true block execution until there are a data available, else throw an Exeption if it's not possible (not important for FIFO buffer)
     * @param blockingStrat
     */
    void setData(SPtr<T> sptr,bool blockingStrat = true,bool isInit = false)
    {
        if(isInit)
        {
            this->buffer[this->writeIndex] = sptr;
            this->writeIndex = (this->writeIndex+1)%this->bufferSize;
        }
        else
        {
            //object boost use for lock exclusive mutex on constructor and free him in destructor on the end of this function
            boost::unique_lock<boost::shared_mutex> lock(this->_access);
            this->buffer[this->writeIndex] = sptr;
            this->writeIndex = (this->writeIndex+1)%this->bufferSize;
        }

    }
private:
    int readIndex = 0;
    int writeIndex = 0;
    int bufferSize;
    SPtr<T>* buffer;
    boost::shared_mutex _access;


};


}

}

#endif // DATABUFFER_H
