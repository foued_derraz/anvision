#include "datatest.h"

dataTest::dataTest(std::string str)
{
    _str = str;

    helper::log("Creation dataTest " + _str);
}

dataTest::~dataTest()
{
    helper::log("Destruction dataTest " + _str);
}
