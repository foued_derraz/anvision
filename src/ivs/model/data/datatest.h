#ifndef DATATEST_H
#define DATATEST_H

#include <iostream>
#include "../../helper/log.h"

class dataTest
{
public:
    dataTest() : dataTest(""){}
    dataTest(std::string str);
    ~dataTest();
    std::string getStr(){return _str;}
private:
    std::string _str;
};

#endif // DATATEST_H
