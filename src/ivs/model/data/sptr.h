#ifndef SPTR_H
#define SPTR_H

#include <boost/shared_ptr.hpp>
#include "../../helper/AdvancedTimer.h"

namespace ivs
{
namespace data
{

typedef helper::ctime_t ctime_t;

template <typename T>
class SPtr : public boost::shared_ptr<T>
{
public:
    SPtr() : boost::shared_ptr<T>()
    {
        helper::AdvancedTimer& atimer = helper::AdvancedTimer::Instance();
        m_CreationTime = atimer.getFastTime();
    }

    SPtr(T* ptr) : boost::shared_ptr<T>(ptr)
    {
        helper::AdvancedTimer& atimer = helper::AdvancedTimer::Instance();
        m_CreationTime = atimer.getFastTime();
    }

    SPtr& operator=(SPtr sptr)
    {
        this->swap(sptr);
        return *this;
    }

    SPtr & operator=(boost::detail::sp_nullptr_t)
    {
        SPtr().swap(*this);
        return *this;
    }

    inline ctime_t getCreationTime()
    {
        return m_CreationTime;
    }

    void swap( SPtr & other )
    {
        ctime_t time = this->m_CreationTime;
        this->m_CreationTime = other.m_CreationTime;
        other.m_CreationTime = time;

        ((boost::shared_ptr<T>*)this)->swap((boost::shared_ptr<T>&)other);
    }

    ~SPtr()
    {
    }

private:
    ctime_t m_CreationTime;
};

template<class T>
inline bool operator==( boost::detail::sp_nullptr_t, SPtr<T> const & p )
{
    return p.get() == 0;
}

template<class T>
inline bool operator==( SPtr<T> const & p, boost::detail::sp_nullptr_t )
{
    return p.get() == 0;
}

}
}

#endif // SPTR_H
