#ifndef ICOMPONENTMAKER_H
#define ICOMPONENTMAKER_H

#include "../component/baseobject.h"

namespace ivs
{
namespace factory
{

class BaseComponentMaker
{
public:
    virtual component::BaseObject* Create(std::string) const = 0;
    ~BaseComponentMaker() {}
};

}
}
#endif // ICOMPONENTMAKER_H
