#include "componentfactory.h"
#include <iostream>
#include <stdexcept>


namespace ivs
{
namespace factory
{

ComponentFactory::ComponentFactory(){
}

void ComponentFactory::RegisterMaker(std::string key, BaseComponentMaker* maker)
{

    //log("Register " + std::to_string(key) + " in factory");
	std::cout  << "Register " << key << " in factory" <<std::endl;    m_makers.find(key);

    if (m_makers.find(key) != m_makers.end())
    {
        throw ivs::Exception("Factory => Multiple makers for given key!");
    }
    m_makers.insert( std::pair<std::string, BaseComponentMaker*>(key, maker));
}

component::BaseObject* ComponentFactory::Create(std::string str) const
{
    std::map<std::string, BaseComponentMaker*>::const_iterator i = m_makers.find(str);
    if (i == m_makers.end())
    {
        throw ivs::Exception("Factory => Unrecognized object type : "+str);
    }
    BaseComponentMaker* maker = i->second;
    return maker->Create(str);
}

ComponentFactory& ComponentFactory::Instance()
{
    return m_instance;
}

factory::ComponentFactory factory::ComponentFactory::m_instance=factory::ComponentFactory();

}
}
