#ifndef COMPONENTFACTORY_H
#define COMPONENTFACTORY_H

#include "basefactory.h"
#include "basecomponentmaker.h"
#include "../component/basecomponent.h"
#include "../../helper/exception.h"
#include "../../helper/log.h"
#include <map>

namespace ivs
{
namespace factory
{

class ComponentFactory : public BaseFactory
{
public:

    void RegisterMaker(std::string str, BaseComponentMaker * maker);
    static ComponentFactory& Instance();
    component::BaseObject * Create(std::string str) const;
private:
    ComponentFactory();
    static ComponentFactory m_instance;
    std::map<std::string, BaseComponentMaker*> m_makers;
};

}
}
#endif // COMPONENTFACTORY_H
