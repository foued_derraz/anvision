#ifndef COMPONENTMAKER_H
#define COMPONENTMAKER_H

#include <iostream>
#include "componentfactory.h"
#include "basecomponentmaker.h"


namespace ivs
{
namespace factory
{

template <typename T>
class ComponentMaker : public BaseComponentMaker
{
public:
    virtual component::BaseObject * Create(std::string str) const
    {
        return new T(str);
    }

    ComponentMaker(std::string str)
    {
        ComponentFactory::Instance().RegisterMaker(str, this);
    }

};

}
}

#endif // COMPONENTMAKER_H
