#ifndef BASEGRAPH_H
#define BASEGRAPH_H

#include <iostream>
//#include <map>
//#include "../data/basedata.h"
#include "../component/baseobject.h"
#include "../data/data.h"

namespace ivs
{
namespace module
{

class BaseModule: public component::BaseObject
{
public:
    IVS_DECL(BaseModule)
    BaseModule();
    void bwdInitComponents();
    void bwdInitModules();
    //inline std::string getName(){return m_name;}
    //inline void setName(std::string name){ m_name=name;}
    std::string getName() {return *m_name.getData();}




    //data::BaseData* getData(std::string name){ return dataList[name]; }

protected:

    data::Data<std::string> m_name;
    //std::map<std::string,data::BaseData*> dataList;

};

}
}
#endif // BASEGRAPH_H
