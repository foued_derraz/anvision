#include "module.h"

namespace ivs
{
namespace module
{

using namespace std;

Module::Module(std::string) :
				BaseModule(),
				m_frequency(initData("frequency", "frequence du graph", 41, &m_frequency)) //,
//m_verbose(initData("verbose","boolean print debug message",true,&m_verbose))
{
}

Module::Module() :
				BaseModule(),
				m_frequency(initData("frequency", "frequence du graph", 41, &m_frequency))    //,
//m_verbose(initData("verbose","boolean print debug message",true,&m_verbose))
{
}

void Module::loop()
{
	helper::createStream(this->getName());
	while (true)
	{
		this->doTask();
	}
}

#define errExitEN(en, msg) \
            do { errno = en; perror(msg); exit(EXIT_FAILURE); \
        } while (0)

void Module::start()
{
	//cout << "start " << *this->m_name.getData() << endl;
	m_Thread = boost::thread(&Module::loop, this);
//#ifdef __unix
	string threadName = "IVS" + *this->m_name.getData();
	string subName = threadName.substr(0, 15); // cut to 15 length
	int rc = pthread_setname_np((pthread_t) m_Thread.native_handle(), subName.c_str());
	if (rc != 0)
		errExitEN(rc, "pthread_setname_np");
//#endif

	for (std::vector<Module *>::iterator itGraph = m_modules.begin(); itGraph < m_modules.end(); ++itGraph)
	{
		((Module *) *itGraph)->start();
	}
}

void Module::join()
{
	for (std::vector<Module *>::iterator itGraph = m_modules.begin(); itGraph < m_modules.end(); ++itGraph)
	{
		((Module *) *itGraph)->join();
	}
	m_Thread.join();
}

void Module::doTask()
{

	if (m_components.size() > 0)
	{
		helper::AdvancedTimer& atimer = helper::AdvancedTimer::Instance();
		atimer.startTimer("Module " + this->getName() + " : ComputationTime");
		for (std::vector<component::BaseObject *>::iterator itComp = m_components.begin(); itComp < m_components.end(); ++itComp)
		{
			((component::BaseObject *) *itComp)->doTask();
		}
		atimer.stopTimer("Module " + this->getName() + " : ComputationTime");
	}
	else
	{
		 boost::this_thread::sleep_for(boost::chrono::microseconds(EMPTY_MODULE_SLEEP_TIME_MICROSECONDS));
	}
}

void Module::addModule(Module * module)
{
	m_modules.push_back(module);
}

void Module::removeModule(Module * module)
{
	//à vérifier , non testé
	for (std::vector<Module *>::iterator i = m_modules.begin(); i < m_modules.end(); ++i)
	{
		if (module == *i)
		{
			m_modules.erase(i);
			break;
		}
	}
}

std::vector<Module *> *Module::getModules()
{
	return &m_modules;
}

void Module::addComponent(component::BaseObject * component)
{
	m_components.push_back(component);
}

void Module::removeComponent(component::BaseObject * component)
{
	//à vérifier , non testé
	for (std::vector<component::BaseObject *>::iterator i = m_components.begin(); i < m_components.end(); ++i)
	{
		if (component == *i)
		{
			m_components.erase(i);
			break;
		}
	}
}

std::vector<component::BaseObject *> *Module::getComponents()
{
	return &m_components;
}

void Module::bwdInitComponents()
{
	// bwdinit les composants du graph
	std::vector<component::BaseObject*>::iterator sit(this->getComponents()->begin()), send(this->getComponents()->end());
	for (; sit != send; sit++)
	{
		((component::BaseComponent*) *sit)->bwdInit();
	}
}

void Module::bwdInitModules()
{
	// bwdinit les graphs
	this->bwdInit();
	for (std::vector<Module*>::iterator itgraph = this->getModules()->begin(); itgraph != this->getModules()->end(); itgraph++)
	{
		module::Module* lowgraph = (*itgraph);
		lowgraph->bwdInitComponents();
		lowgraph->bwdInitModules();
	}
}

Module * Module::findModule(std::string nameGraph)
{
	for (std::vector<Module*>::iterator itgraph = this->getModules()->begin(); itgraph != this->getModules()->end(); itgraph++)
	{
		Module* lowgraph = (*itgraph);

		if (strcmp(lowgraph->getName().c_str(), nameGraph.c_str()) == 0)
		{
			return lowgraph;
		}
	}
	return 0;
}

component::BaseObject * Module::findComponent(std::string name)
{
	for (std::vector<component::BaseObject*>::iterator itcomp = this->getComponents()->begin(); itcomp != this->getComponents()->end(); itcomp++)
	{
		component::BaseObject* comp = (*itcomp);

		if (strcmp(comp->getName().c_str(), name.c_str()) == 0)
		{
			return comp;
		}
	}
	return 0;
}

}
}
