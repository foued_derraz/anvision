#ifndef GRAPH_H
#define GRAPH_H

#include "basemodule.h"
#include "../component/basecomponent.h"
#include <vector>
#include <cstring>
#include "../data/data.h"
#include "../../helper/AdvancedTimer.h"
#include <boost/thread/thread.hpp>


namespace ivs
{
namespace module
{


#define EMPTY_MODULE_SLEEP_TIME_MICROSECONDS (10)

class Module : public BaseModule
{
public:

    IVS_DECL(Module);

    Module();
    Module(std::string);

    void addModule(Module *module);
    void removeModule(Module *module);
    std::vector<Module*>* getModules();

    void addComponent(component::BaseObject *);
    void removeComponent(component::BaseObject *);
    std::vector<component::BaseObject*>* getComponents();

    void bwdInitComponents();
    void bwdInitModules();
    Module * findModule(std::string nameGraph);
    component::BaseObject * findComponent(std::string name);

    void doTask();

    void init() {}
    void bwdInit() {}

    void loop();

    void start();
    void join();
private:
    boost::thread m_Thread;

    std::vector<Module*> m_modules;
    std::vector<component::BaseObject*> m_components;

    data::Data<int> m_frequency;
    //data::Data<bool> m_verbose;

};


}
}

#endif // GRAPH_H
