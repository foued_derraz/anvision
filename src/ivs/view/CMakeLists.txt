Set(IVS_VICKY_HEADER
        baseView.cpp
        OpencvView.cpp
        QtView.cpp

        cqtopencvviewergl.cpp

) 


Set(IVS_VICKY_SOURCES
        baseView.h
        OpencvView.h
        QtView.h

        cqtopencvviewergl.h
)

QT4_WRAP_UI(IVS_VICKY_HDRS
        QtView.ui
)

qt5_add_resources(IVS_VICKY_RESOURCES_RCC
        qtStyle/style.qrc
)

ADD_LIBRARY(view SHARED ${IVS_VICKY_SOURCES} ${IVS_VICKY_HEADER} ${IVS_VICKY_HDRS} ${IVS_VICKY_RESOURCES_RCC} )
QT5_USE_MODULES(view Widgets OpenGL)


