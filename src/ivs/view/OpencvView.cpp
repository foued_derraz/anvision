#include "OpencvView.h"

namespace ivs
{
namespace view
{

OpencvView::OpencvView()
{

    this->DisplayList = std::map<std::string ,data::SPtr<cv::Mat> >();
}

void OpencvView::update()
{
    std::vector<std::pair<std::string ,data::SPtr<cv::Mat> > > ImList;
    boost::unique_lock<boost::shared_mutex> lock(this->_access);
    for (auto kv : this->DisplayList) {
        ImList.push_back(kv);
    }
    lock.unlock();

    for(auto im : ImList)
    {
        if (im.second != NULL)
        cv::imshow(im.first,*(im.second));
    }
}


void OpencvView::addDisplay(std::string str)
{

    boost::unique_lock<boost::shared_mutex> lock(this->_access);
    this->DisplayList.insert(std::pair<std::string,data::SPtr<cv::Mat> > (str,NULL));
}

void OpencvView::showDisplay(std::string key ,data::SPtr<cv::Mat> mat)
{


    boost::unique_lock<boost::shared_mutex> lock(this->_access);
    this->DisplayList[key] = mat;
}

void OpencvView::loop()
{
    while(true)
    {
        this->update();
        cv::waitKey(33);
    }
}

void OpencvView::startDisplay()
{
    //cout << "start " << *this->m_name.getData() << endl;
    m_Thread = boost::thread(&OpencvView::loop, this);
//#ifdef __unix
    string threadName = "IVS-display";
    int rc = pthread_setname_np((pthread_t) m_Thread.native_handle(), threadName.c_str());
    if (rc != 0)
        errExitEN(rc, "pthread_setname_np");
//#endif
}



}
}

