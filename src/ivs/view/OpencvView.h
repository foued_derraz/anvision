#ifndef OPENCVVIEW_H
#define OPENCVVIEW_H

#include "baseView.h"

namespace ivs
{

namespace view
{

class OpencvView : public BaseView
{
public:
    OpencvView();

    //more virtual function here
    void update() ;
    void addDisplay(std::string);
    void showDisplay(std::string ,data::SPtr<cv::Mat>);

    void startDisplay();
    void loop();


private:
    boost::shared_mutex _access;
};


}

}

#endif //BASEVIEW_H
