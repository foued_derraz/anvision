#include "QtView.h"
#include "ui_QtView.h"
#include "../controller/maincontroller.h"

namespace ivs
{
namespace view
{

QtView::QtView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QtView)
{
    this->DisplayList = std::map<std::string ,data::SPtr<cv::Mat> >();

    ui->setupUi(this);
    QString x, y;
    x = "test";
    y= "testEditable";

    QStandardItemModel * model = new QStandardItemModel;

    QList<QStandardItem*> newIt;

    QStandardItem * item = new QStandardItem(x);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    newIt.append(item);
    QStandardItem * item2 = new QStandardItem(y);
    item2->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsEditable);
    newIt.append(item2);
    model->appendRow(newIt);

    QList<QStandardItem*> newIt2;
    QStandardItem * item3 = new QStandardItem(x);
    item3->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    item3->setIcon( QIcon("/qss_icons/rc/checkbox_checked_focus.png"));
    newIt2.append(item3);
    item3 = new QStandardItem(y);
    item3->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsEditable);
    newIt2.append(item3);

    item->appendRow(newIt2);
    ui->TreeViewTest->setModel(model);
    ui->TreeViewTest->setHeaderHidden(true);
    ui->TreeViewTest->expandAll();
    ui->TreeViewTest->resizeColumnToContents(0);


    mtimer = new QTimer(this);
    connect(mtimer, SIGNAL(timeout()), this, SLOT(refresh()));
}

QtView::~QtView()
{
    delete ui;
}

void QtView::update()
{

    std::vector<std::pair<std::string ,data::SPtr<cv::Mat> > > ImList;
    boost::unique_lock<boost::shared_mutex> lock(this->_access);
    for (auto kv : this->DisplayList) {
        ImList.push_back(kv);
    }
    lock.unlock();

    for(auto im : ImList)
    {
        if (im.second != NULL)
        {
            this->showImage(im.first,im.second);
        }
    }


}


void QtView::addDisplay(std::string str)
{
    boost::unique_lock<boost::shared_mutex> lock(this->_access);
    this->DisplayList.insert(std::pair<std::string,data::SPtr<cv::Mat> > (str,NULL));
}

void QtView::showDisplay(std::string key ,data::SPtr<cv::Mat> mat)
{
    boost::unique_lock<boost::shared_mutex> lock(this->_access);
    this->DisplayList[key] = mat;
}

void QtView::loop() {}

void QtView::showImage( std::string str, ivs::data::SPtr<cv::Mat> mat)
{
    QString qstr = QString(str.c_str());
    if(ui->ComboBoxDisplay1->currentText()==qstr)
    {
        ui->openGLViewer1->showImage(mat);
    }
    if(ui->ComboBoxDisplay2->currentText()==qstr)
        ui->openGLViewer2->showImage(mat);
}



void QtView::startDisplay()
{
    this->mtimer->start(100);
}

void QtView::on_toolButton_released()
{


    ivs::MainController::Instance()->init();
    ivs::MainController::Instance()->loop();

}

void QtView::on_toolButton_2_released()
{
    this->mtimer->stop();
}

void QtView::refresh()
{
    update();
}

}
}


void ivs::view::QtView::on_ComboBoxDisplay1_currentTextChanged(const QString &arg1)
{

    ui->openGLViewer1->DisplayChange();
}
