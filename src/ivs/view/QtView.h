#ifndef QTVIEW_H
#define QTVIEW_H

#include "baseView.h"
#include <QMainWindow>
#include <QStandardItem>
#include <QTimer>
#include <QDebug>

namespace Ui {
class QtView;
}

namespace ivs
{

namespace view
{

class QtView : public QMainWindow, public BaseView
{
    Q_OBJECT

public:
    explicit QtView(QWidget *parent = 0);


    //more virtual function here
    void update() ;
    void addDisplay(std::string);
    void showDisplay(std::string ,data::SPtr<cv::Mat>);
    void showImage( std::string str, ivs::data::SPtr<cv::Mat> mat);

    void startDisplay();
    void loop();
    ~QtView();



private slots:

    void on_toolButton_released();
    void on_toolButton_2_released();
    void refresh();

    void on_ComboBoxDisplay1_currentTextChanged(const QString &arg1);

private:
    boost::shared_mutex _access;
    Ui::QtView *ui;
    cv::VideoCapture mCapture;
    QTimer *mtimer;
    std::map<std::string,ivs::data::SPtr<cv::Mat> > DisplayList;

protected:



};


}

}

#endif //QtView_H
