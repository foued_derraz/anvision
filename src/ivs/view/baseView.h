#ifndef BASEVIEW_H
#define BASEVIEW_H

#include "../model/data/data.h"

namespace ivs
{

namespace view
{

#define errExitEN(en, msg) \
            do { errno = en; perror(msg); exit(EXIT_FAILURE); \
        } while (0)

template <typename IfaceT >
class Singleton
{
public:
    static IfaceT* Instance() {
        return m_instance;
    }

    Singleton () {}
    // Only used for unit tests
    // Takes ownership of instance
    template< typename ImplT>
    static void declareInstance(ImplT* instance) {
        if (m_instance == NULL)
        {

            if( IfaceT* test = dynamic_cast< IfaceT* >( instance ) )
            {
                m_instance = instance;
            }
        }
    }

private:
static IfaceT * m_instance;
};



class BaseView
{
public:

    //more virtual function here
    virtual void update()  = 0;
    virtual void addDisplay(std::string)  = 0;
    virtual void showDisplay(std::string ,data::SPtr<cv::Mat>)  = 0;

    virtual void startDisplay() = 0;
    virtual void loop() =0;



protected:
    std::map<std::string,data::SPtr<cv::Mat> > DisplayList;
    boost::thread m_Thread;

};


typedef Singleton<BaseView> SBaseView;




}

}

#endif //BASEVIEW_H
