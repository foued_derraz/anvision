#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"
#include "../controller/maincontroller.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    QString x, y;
    x = "test";
    y= "testEditable";

    QStandardItemModel * model = new QStandardItemModel;

    QList<QStandardItem*> newIt;

    QStandardItem * item = new QStandardItem(x);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    newIt.append(item);
    QStandardItem * item2 = new QStandardItem(y);
    item2->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsEditable);
    newIt.append(item2);
    model->appendRow(newIt);

    QList<QStandardItem*> newIt2;
    QStandardItem * item3 = new QStandardItem(x);
    item3->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    item3->setIcon( QIcon("/qss_icons/rc/checkbox_checked_focus.png"));
    newIt2.append(item3);
    item3 = new QStandardItem(y);
    item3->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsEditable);
    newIt2.append(item3);

    item->appendRow(newIt2);
    ui->TreeViewTest->setModel(model);
    ui->TreeViewTest->setHeaderHidden(true);
    ui->TreeViewTest->expandAll();
    ui->TreeViewTest->resizeColumnToContents(0);


    mtimer = new QTimer(this);
    connect(mtimer, SIGNAL(timeout()), this, SLOT(refreshCam()));

    /*ui->treeWidget->expandAll();
    ui->treeWidget->resizeColumnToContents(0);*/
    //ui->TreeViewTest = new QTreeView(this);
    //treeViewsetModel(model);
    //ui->TreeViewTest->model()->insertColumn(0);
    //ui->TreeViewTest->model()->insertColumn(1);


}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showImage( std::string str, ivs::data::SPtr<cv::Mat> mat)
{
    std::cout << "test image " << str << std::endl;
    QString qstr = QString(str.c_str());
    qDebug().nospace() << "abc" << qPrintable( ui->ComboBoxDisplay1->currentText() ) << "def";
    if(ui->ComboBoxDisplay1->currentText()==qstr)
    {
        std::cout << "test if " << str << std::endl;
        ui->openGLViewer1->showImage(*mat);
    }
    if(ui->ComboBoxDisplay2->currentText()==qstr)
        ui->openGLViewer2->showImage(*mat);
}

void MainWindow::startDisplay()
{
    this->mtimer->start(0);
}

void MainWindow::updateView()
{
    std::vector<std::pair<std::string ,ivs::data::SPtr<cv::Mat> > > ImList;
    //boost::unique_lock<boost::shared_mutex> lock(this->_access);
    for (auto kv : this->DisplayList) {
        ImList.push_back(kv);
    }
    lock.unlock();

    for(auto im : ImList)
    {
        if (im.second != NULL)
        {
            this->showImage(im.first,im.second);
        }
    }
}

void MainWindow::refreshCam()
{
    /*cv::Mat image;
    mCapture >> image;

    // Do what you want with the image :-)

    // Show the image
    ui->openGLViewer1->showImage( image );*/
}

void MainWindow::timerEvent(QTimerEvent *event)
{


    // Do what you want with the image :-)

    // Show the image
    updateView();
}


void MainWindow::on_actionStart_camera_triggered()
{

}

void MainWindow::on_toolButton_released()
{
    /*if( !mCapture.isOpened() )
        if( !mCapture.open( 0 ) )
            return;

    std::cout << "test" << std::endl;
    ;
*/

    ivs::MainController::Instance()->init();
    ivs::MainController::Instance()->loop();
    //this->mtimer->start(0);
}

void MainWindow::on_toolButton_2_released()
{
    this->mtimer->stop();
}
