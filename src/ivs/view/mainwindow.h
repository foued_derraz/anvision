#ifndef MAINWINDOW_H
#define MAINWINDOW_H



#include "../model/data/sptr.h"
#include <QMainWindow>
#include <QStandardItem>
#include <QTimer>
#include <QDebug>
#include <opencv2/highgui/highgui.hpp>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void showImage( std::string str, ivs::data::SPtr<cv::Mat> mat );
    void startDisplay();
    void updateView();
    ~MainWindow();


private slots:

    void on_actionStart_camera_triggered();

    void refreshCam();

    void on_toolButton_released();

    void on_toolButton_2_released();

private:
    Ui::MainWindow *ui;
    cv::VideoCapture mCapture;
    QTimer *mtimer;
    std::map<std::string,ivs::data::SPtr<cv::Mat> > DisplayList;

protected:
    void timerEvent(QTimerEvent *event);
};

#endif // MAINWINDOW_H
